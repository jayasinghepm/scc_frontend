import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './delete-confirm.component.html',
  styleUrls: ['./delete-confirm.component.scss']
})
export class DeleteConfirmComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DeleteConfirmComponent>,
  ) {

  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(false);

  }
  public onClickYes() {
    this.dialogRef.close(true);
  }
  public onClickNo() {
    this.dialogRef.close(false);
  }
}
