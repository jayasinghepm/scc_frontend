import {Component, Inject, OnInit, Optional} from '@angular/core';
import {BackendService} from "../../../../@core/rest/bo_service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {validateEmail, validateMobile} from "../../../../@core/utils/validator";

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.scss']
})
export class CreateAdminComponent implements OnInit {

  public savedLogin = false;
  public disableClose = false;

  private inValidFieldMessage = undefined;

  public jsonBodyAdmin = {
    id: undefined,
    firstName: undefined,
    lastName: undefined,
    title: undefined,
    email: undefined,
    telephoneNo: undefined,
    mobileNo: undefined,
    position: undefined,
    userId: undefined,
    role: undefined,
  }


  public jsonBodyLogin = {
      userId: -1,
      userType: 3,
      loginStatus: 1,
      userName: undefined,
      password: undefined,
      isFirstTime: 2,
  };

  public roles = [];
  public titles = [];

  constructor(
    private boService:BackendService,
    public dialogRef: MatDialogRef<CreateAdminComponent>,
    private  toastSerivce:  NbToastrService,
    private masterData: MassterDataService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,
  ) {
      this.init();
  }

  init() {
    this.masterData.getTitles().subscribe(d => {
      this.titles = d;
    })
    this.masterData.getRolesFull().subscribe(d => {
      this.roles = d;
    })
    console.log(this.popupData)
    if (this.popupData.isEdit) {
      this.savedLogin = true;
      this.jsonBodyAdmin.id = this.popupData.data.id;
      this.jsonBodyAdmin.firstName = this.popupData.data.firstName;
      this.jsonBodyAdmin.lastName = this.popupData.data.lastName;
      this.jsonBodyAdmin.title = this.popupData.data.title;
      this.jsonBodyAdmin.email = this.popupData.data.email;
      this.jsonBodyAdmin.mobileNo = this.popupData.data.mobileNo;
      this.jsonBodyAdmin.role = this.popupData.data.idRole;
      this.jsonBodyAdmin.position = this.popupData.data.position;
      console.log(this.jsonBodyAdmin)
    } else {
      
    }

  }


  public createLogin() {
    if (this.jsonBodyLogin.userName === undefined ||
      this.jsonBodyLogin.userName === '' ||
      this.jsonBodyLogin.password === undefined ||
      this.jsonBodyLogin.password === '' ) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    } else {
      this.boService.sendRequestToBackend(
        RequestGroup.LoginUser,
        RequestType.ManageUserLogin,
        {
          DATA: this.jsonBodyLogin,
        }
      ).then(data  => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
            this.disableClose = true;
            this.savedLogin = true;

            console.log(data.DAT);
            this.jsonBodyLogin.userId = data.DAT.dto.userId;
            this.jsonBodyAdmin.userId =  data.DAT.dto.userId;
          } else {
            this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      });
    }

  }
  public updateAdmin() {
    if (!this.popupData.isEdit) this.jsonBodyAdmin.id = -1;
    if (this.validateAdmin()) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ManageAdmin,
        {
          DATA: this.jsonBodyAdmin,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });

            this.disableClose = false;
            // this.userId = data.DAT.dto.userId;
            this.dialogRef.close(true);
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      })
    }else  {
      if (this.inValidFieldMessage !== undefined) {
        this.toastSerivce.show('', this.inValidFieldMessage, {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
  }

  private validateAdmin():boolean {
    this.inValidFieldMessage = undefined;

    if ((this.popupData.isEdit) && this.jsonBodyAdmin.id === undefined || this.jsonBodyAdmin.id === 0 || this.jsonBodyAdmin.id === '') {
      return false;
    }
    if (this.jsonBodyAdmin.firstName === undefined || this.jsonBodyAdmin.firstName === '') {
      return false;
    }
    if (this.jsonBodyAdmin.lastName === undefined || this.jsonBodyAdmin.lastName === ''){
      return false;
    }
    if (this.jsonBodyAdmin.title === undefined || this.jsonBodyAdmin.title === '' ) {
      return false;
    }
    if (this.jsonBodyAdmin.email === undefined || this.jsonBodyAdmin.email === ''  ) {
      return false;
    } else if (!validateEmail(this.jsonBodyAdmin.email)) {
      this.inValidFieldMessage = 'Enter Valid Email'
      return false;
    }
    if (this.jsonBodyAdmin.mobileNo === undefined || this.jsonBodyAdmin.mobileNo === '') {
      return false;
    } else if (!validateMobile(this.jsonBodyAdmin.mobileNo)) {
      this.inValidFieldMessage = 'Enter Valid Mobile Number'
      return false;
    }
    if (this.jsonBodyAdmin.position === undefined || this.jsonBodyAdmin.position === '') {
      return false;
    }
    if (this.jsonBodyAdmin.role === undefined || this.jsonBodyAdmin.role === 0 || this.jsonBodyAdmin.role === '' ) {
      return false;
    }

    return true;

  }


  ngOnInit() {
  }

  onClickSave() {
    if (this.savedLogin) {
      this.updateAdmin();
    } else {
      this.createLogin();
    }
  }

  closeDialog() {
    if (!this.disableClose) {
      this.dialogRef.close();
    }
  }


}
