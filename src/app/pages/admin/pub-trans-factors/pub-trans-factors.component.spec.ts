import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubTransFactorsComponent } from './pub-trans-factors.component';

describe('PubTransFactorsComponent', () => {
  let component: PubTransFactorsComponent;
  let fixture: ComponentFixture<PubTransFactorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubTransFactorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubTransFactorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
