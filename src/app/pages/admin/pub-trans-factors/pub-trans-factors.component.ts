import { Component, OnInit } from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";

@Component({
  selector: 'app-pub-trans-factors',
  templateUrl: './pub-trans-factors.component.html',
  styleUrls: ['./pub-trans-factors.component.scss']
})
export class PubTransFactorsComponent implements OnInit {

  settings = {
    mode: 'inline',
    pager: {
      display: false,
    },
    actions: {
      delete: false,
      add: false,
    },
    // add: {
    //   // addButtonContent: '<i class="nb-plus"></i>',
    //   createButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    //   confirmCreate: true,
    // },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {

      vehicleName: {
        title: 'Vehicle Name',
        type: 'number',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      g_per_km: {
        title: 'g/km',
        type: 'number',
        filter: false,
        sort: false,

      },
      co2_per_km: {
        title: 'kg Co2e/passenger km',
        type: 'number',
        filter: false,
        sort: false,
      },
      assumption: {
        title: 'Assumption',
        type: 'string',
        filter: false,
        sort: false,
      },
    }
  };
  source: LocalDataSource = new LocalDataSource();

  constructor(private boService: BackendService, private toastSerivce: NbToastrService) {
    this.loadData()
  }


  onUpdate($event: any) {
    console.log($event)
    let jsonBody = {
      vehicleType: $event.newData.vehicleType,
      vehicleName: $event.newData.vehicleName,
      g_per_km: $event.newData.g_per_km,
      co2_per_km: $event.newData.co2_per_km,
      assumption: $event.newData.assumption,
    }


    if ((jsonBody.vehicleType === undefined || jsonBody.vehicleType === '' || jsonBody.vehicleType < 1) ||
      (jsonBody.vehicleName === undefined || jsonBody.vehicleName === '')
      || (jsonBody.assumption === undefined || jsonBody.assumption === '')
      || (jsonBody.g_per_km === undefined || jsonBody.g_per_km === '' || jsonBody.g_per_km < 0)
      || (jsonBody.co2_per_km === undefined || jsonBody.co2_per_km === '' || jsonBody.co2_per_km < 0)

    ) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      })
      return;
    }
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ManagePubTransFactors,
      { DATA: jsonBody}
    ).then(data => {

        if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
          this.toastSerivce.show('', 'Saved data successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
          $event.confirm.resolve($event.newData);
          this.loadData();
        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }

    )
  }



  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ListPubTransFactors,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          console.log(data.DAT.LIST)
          data.DAT.LIST.forEach(dto => {
            list.push(this.fromListRequest(dto));
          })
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private fromListRequest(dto: any) :any {
    return {
      vehicleType: dto.vehicleType,
      vehicleName: dto.vehicleName,
      g_per_km: dto.g_per_km,
      co2_per_km: dto.co2_per_km,
      assumption: dto.assumption,
    }
  }
  ngOnInit() {
  }

}
