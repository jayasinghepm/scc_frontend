import {Component, OnInit} from '@angular/core';
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {BackendService} from "../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-emission-history',
  templateUrl: './emission-history.component.html',
  styleUrls: ['./emission-history.component.scss']
})
export class EmissionHistoryComponent implements OnInit {

  constructor(
    private masterData: MassterDataService,
    private boService : BackendService,
    private toastSerivce: NbToastrService)
  { }
  
  public compaines = [];
  public selectedCompany = -1;
  private fy = '';

  private deletedYears = [];

  public emissionHistory = {
    company_owned: [],
    emp_comm_not_paid: [],
    mun_water: [],
    t_d: [],
    emp_comm_paid: [],
    diesel_generators: [],
    refri_leakage: [],
    transport_hired_paid: [],
    waste_transport: [],
    electricity: [],
    air_travel: [],
    transport_hired_not_paid: [],
    fire_ext: [],
    transport_loc_pur: [],
    transport_rented: [],
    offroad: [],
    waste_disposal: [],
    direct: [],
    total: [],
    indirect: [],
    scope_3: [],
    scope_2: [],
    scope_1: [],
    intensity: [],
    per_capita: [],
    per_prod: [],
    ef_grid_elec: [],
    ef_td: [],
    ef_mw: []
  }

  public emissionHistoryYears = [];
  public previousYear: string;

  public disableEnterData = false;
  public isFy  = false;


  ngOnInit(): void {
    this.masterData.getCompaninesFull().subscribe(d => {
      if (d !== undefined && d.length !== 0) {
        d.forEach(c => {
          this.compaines.push({
            id: c.id,
            name: c.name
          });
        })
      }
    })

  }

  onChangeCompany(value: any) {
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL:{
          id: {
            type: 1,
            col: 1,
            value: value,
          },
        },
        SORT_MODEL: "",
      }
    ).then(data => {

      if (data.HED !== undefined && data.HED.RES_STS == 1) {
        if (data.DAT !== undefined && data.DAT.list !== undefined) {
            const company = data.DAT.list[0];
            if (company.fyCurrentStart == 0 || company.fyCurrentEnd == 0) {
              this.disableEnterData = true;
            }else if (company.fyCurrentStart !== company.fyCurrentEnd){
              this.fy = company.fyCurrentStart + "/" + (company.fyCurrentEnd % 100);
              this.isFy = true;
              this.disableEnterData = false;
            }else {
              this.fy =company.fyCurrentStart;
              this.isFy = false;
              this.disableEnterData = false;
            }

          //load emission history
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.GHGEmissionSummary,
            {
              companyId: this.selectedCompany,
              branchId: -1,
              yearsBack: 5
            }
          ).then(data => {
            if (data.DAT !== undefined && data.DAT.list !== undefined) {
              let emissionYearList = data.DAT.list;
              console.log(emissionYearList)
              emissionYearList.splice(0, 1);
              this.emissionHistoryYears = [];
              for(const dto of emissionYearList) {
                if (dto.fy !== undefined && dto.fy !== this.fy) {
                  this.emissionHistoryYears.push(dto.fy);

                }else {
                  continue;
                }

                for(const key of Object.keys(dto.emissionInfo)) {
                  try{
                    this.emissionHistory[key].push(dto.emissionInfo[key])
                  }catch(error) {
                    console.log(dto, key, dto.emissionInfo)
                    console.log(error)
                  }

                }
              }


            }
          });

        }else {
          this.disableEnterData = true;
        }
      }else {
        this.disableEnterData = true;
      }
    });



    console.log(value);
    //todo: load emission of years
  }

  onChangeEm($event, index, key) {
     //@ts-ignore
    this.emissionHistory[key][index] = $event.target.value;
  }

  public onClickAddPrevYear($event): void {
    if (this.previousYear !== undefined) {
      if (this.emissionHistoryYears.includes(this.previousYear)) {
        this.toastSerivce.show('', 'The Year Already Exists.', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        return;
      }else if (this.previousYear === this.fy) {
        this.toastSerivce.show('', "Can't add current year to emission history.", {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        return;
      }
      if (this.validateYear(this.previousYear)) {

        this.emissionHistoryYears.push(this.previousYear);
        this.emissionHistoryYears.sort();
        const indexOfPrevYear =  this.emissionHistoryYears.indexOf(this.previousYear);
        console.log(indexOfPrevYear)

        this.emissionHistory.company_owned.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.emp_comm_not_paid.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.mun_water.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.t_d.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.emp_comm_paid.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.diesel_generators.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.refri_leakage.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.transport_hired_paid.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.waste_transport.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.electricity.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.air_travel.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.transport_hired_not_paid.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.fire_ext.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.transport_loc_pur.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.transport_rented.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.waste_disposal.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.offroad.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.total.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.direct.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.indirect.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.scope_1.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.scope_2.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.scope_3.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.per_capita.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.per_prod.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.intensity.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.ef_mw.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.ef_grid_elec.splice(indexOfPrevYear, 0, 0.0);
        this.emissionHistory.ef_td.splice(indexOfPrevYear, 0, 0.0);


        this.previousYear = '';


      }else {
        this.toastSerivce.show('', 'Enter Valid Year', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        return;
      }
    }
  }

  public deleteYearFromEmissionHistory($event: MouseEvent, year: string, index: number) {
    if (!this.deletedYears.includes(year)) {
      this.deletedYears.push(year);
    }
    this.emissionHistoryYears.splice(index, 1);

    this.emissionHistory.company_owned.splice(index, 1);
    this.emissionHistory.emp_comm_not_paid.splice(index, 1);
    this.emissionHistory.mun_water.splice(index, 1);
    this.emissionHistory.t_d.splice(index, 1);
    this.emissionHistory.emp_comm_paid.splice(index, 1);
    this.emissionHistory.diesel_generators.splice(index, 1);
    this.emissionHistory.refri_leakage.splice(index, 1);
    this.emissionHistory.transport_hired_paid.splice(index, 1);
    this.emissionHistory.waste_transport.splice(index, 1);
    this.emissionHistory.electricity.splice(index, 1);
    this.emissionHistory.air_travel.splice(index, 1);
    this.emissionHistory.transport_hired_not_paid.splice(index, 1);
    this.emissionHistory.fire_ext.splice(index, 1);
    this.emissionHistory.transport_loc_pur.splice(index, 1);
    this.emissionHistory.transport_rented.splice(index, 1);
    this.emissionHistory.offroad.splice(index, 1);
    this.emissionHistory.waste_disposal.splice(index, 1);
    this.emissionHistory.total.splice(index, 1);
    this.emissionHistory.direct.splice(index, 1);
    this.emissionHistory.indirect.splice(index, 1);
    this.emissionHistory.scope_1.splice(index, 1);
    this.emissionHistory.scope_2.splice(index, 1);
    this.emissionHistory.scope_3.splice(index, 1);
    this.emissionHistory.per_capita.splice(index, 1);
    this.emissionHistory.per_prod.splice(index, 1);
    this.emissionHistory.intensity.splice(index, 1);
    this.emissionHistory.ef_td.splice(index, 1);
    this.emissionHistory.ef_mw.splice(index, 1);
    this.emissionHistory.ef_grid_elec.splice(index, 1);


    const dto = {
      isDeleted: 1,
      companyId: this.selectedCompany,
      fy: year,
      mode: 1,
      emissionInfo: {}
    }

    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      26,
      {
        DATA: dto,
      }
    ).then(data=> {
      if (data !== undefined && data.DAT !== undefined && data.DAT.DATA !== undefined) {
        this.toastSerivce.show('', 'The Emission History was deleted successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })

  }
  public saveEmissionHistory($event : MouseEvent) {
    let savedSuccess = 0;
    const dtos: any[] = this.toDtos();
    for(const dto of dtos) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        26,
        {
          DATA: dto,
        }
      ).then(data => {
          console.log(data);

          if(data !== undefined && data.DAT !== undefined && data.DAT.DATA !== undefined) {
            //if success
            savedSuccess++;
            console.log(savedSuccess, (this.emissionHistoryYears.length - this.deletedYears.length))
            if (savedSuccess == (this.emissionHistoryYears.length - this.deletedYears.length)) {
              this.toastSerivce.show('', 'The Emission History was saved successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('', "Error in saving previous year's emission.", {
              status: 'warning',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }



      })
    }
  }

  private toDtos(): any  {
    let dtos = []
    for (const year of this.emissionHistoryYears) {
      if (this.deletedYears.includes(year)) {
        // const dto = {
        //   isDeleted: 1,
        //   companyId: this.selectedCompany,
        //   fy: year,
        //   mode: 1,
        //   emissionInfo: {}
        // }
        // dtos.push(dto);
        continue;
      } else {
        let yearIndex = this.emissionHistoryYears.indexOf(year);
        let emissionInfo = {};
        for(const key of Object.keys(this.emissionHistory)) {
          emissionInfo[key] = this.emissionHistory[key][yearIndex];
        }
        const dto = {
          isDeleted: 0,
          companyId: this.selectedCompany,
          fy: year,
          mode: 1,
          emissionInfo: emissionInfo
        }
        dtos.push(dto);
      }
    }
    return dtos;
  }





  public validateYear(year : string) : boolean {
    if (this.isFy) {
      return year.match(/^20\d\d\/\d\d$/) != null;
    }else {
      return year.match(/^20\d\d$/) != null;
    }
  }

  exportExcelSummary($event: MouseEvent) {

    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ExportGHGEmissionSummary,
      {
          comId: this.selectedCompany
      }
    )
      .then(data => {
        console.log(data)
         if(data !== undefined && data.DAT !== undefined  && data.DAT.ENTITY_ACTION_STATUS === 1) {
           // window.open(data.DAT.fileUrl, "_blank");
           var a = document.createElement('a');
           a.href = data.DAT.fileUrl;
           // a.download = "";
           a.click();
           a.remove()
         }else {
           this.toastSerivce.show('', "Error in exporting.", {
             status: 'warning',
             destroyByClick: true,
             duration: 2000,
             hasIcon: false,
             position: NbGlobalPhysicalPosition.TOP_RIGHT,
             preventDuplicates: true,
           })
         }


      })

  }
}
