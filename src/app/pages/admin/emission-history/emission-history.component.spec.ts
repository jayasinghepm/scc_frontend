import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmissionHistoryComponent } from './emission-history.component';

describe('EmissionHistoryComponent', () => {
  let component: EmissionHistoryComponent;
  let fixture: ComponentFixture<EmissionHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmissionHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmissionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
