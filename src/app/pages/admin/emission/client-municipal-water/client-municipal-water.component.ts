import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {SmartTableService} from "../../../../@core/service/smart-table.service";
import {BackendService} from "../../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {MatDialog} from "@angular/material";
import {FireExtFormulaComponent} from "../../../formulas/fire-ext-formula/fire-ext-formula.component";
import {MunWaterFormulaComponent} from "../../../formulas/mun-water-formula/mun-water-formula.component";
import {UpdateGeneratorsComponent} from "../client-generators/update-generators/update-generators.component";
import {UpdateMunWaterComponent} from "./update-mun-water/update-mun-water.component";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";

@Component({
  selector: 'app-client-municipal-water',
  templateUrl: './client-municipal-water.component.html',
  styleUrls: ['./client-municipal-water.component.scss']
})
export class ClientMunicipalWaterComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      // delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      // },
      company: {
        title: 'Company',
        type: 'string',
        filter: true,
        sort: true,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,
      },
      // numOfMeters: {
      //   title: 'No of Meters',
      //   type: 'number',
      //   filter: false,
      //   sort: false,
      // },
      meterNo: {
        title: 'Meter No',
        type: 'string',
        filter: true,
        sort: true,
      },
      units: {
        title: 'Unit',
        type: 'string',
        filter: false,
        sort: false,
      },
      consumption: {
        title: 'Consumption',
        type: 'number',
        filter: true,
        sort: true,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'string',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCo2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };

  mySetting = JSON.parse(JSON.stringify(this.settings))
  source: LocalDataSource = new LocalDataSource();

  private filterModel = {
    company: { value: '' , type: 4, col: 3},
    branch: { value: '' , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    //todo: float
    consumption: { value: 0 , type: 1, col:5},
    meterNo: { value: '' , type: 4, col: 3},
    entryId: { value: 0 , type: 1, col: 1},
    companyId: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    consumption: { dir: '' },
    meterNo: { dir: '' },
    entryId: { dir: '' },
    year: { dir: '' },

  }

  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private  toastSerivce:  NbToastrService,
              private dialog: MatDialog,
              ) {
    this.selectedCompany = 0;
    // this.loadFiltered(this.pg_current);
    this.loadFiltered(this.pg_current)
    // this.source.load(data);
  }

  public  companines = [];
  public selectedCompany: number;

  ngOnInit(): void {
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companines.push({
        id: 0,
        name: 'All'
      })
      if (d !== undefined && d.length !== 0) {
        d.forEach(c => {
          this.companines.push({
            id: c.id,
            name: c.name
          });
        })
      }
    })

  }
  onChangeCompany(value: any) {
    this.filterModel.companyId.value = value;
    this.loadFiltered(this.pg_current);
  }




  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListMunWaterEntry,
      {
        PAGE_NUMBER: this.pg_current,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.branch === undefined || data.branch === '' ){
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    // if (data.numOfMeters === undefined || data.numOfMeters === '' ){
    //   return false;
    // }
    if (data.consumption === undefined || data.consumption === '' ){
      return false;
    }
    // if (data.units === undefined || data.units === '' ){
    //   return false;
    // }


    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let numOfMeters;
    let meterNo;
    let consumption
    let units;
    let month;
    let year;
    let id;
    // this.masterData.getBranchName(dto.BRANCH_ID).subscribe(data => { branch = data; }).unsubscribe();
    this.masterData.getUnitsName(dto.UNITS).subscribe(d => units = d).unsubscribe();
    // this.masterData.getCompanyName(dto.COMPANY_ID).subscribe(data => { company = data; }).unsubscribe();
    this.masterData.getMonthName(dto.MONTH).subscribe(data => { month = data; }).unsubscribe();
    year = dto.YEAR;
    id = dto.MUNICIPAL_WATER_ENTRY_ID;
    consumption = isNaN(+dto.WATER_CONSUMPTION) ? 0.0 : (+dto.WATER_CONSUMPTION).toFixed(5);
    // numOfMeters = dto.NUMS_OF_METERS;
    // todo:
    meterNo = dto.METER_NO;

    return {
      id,
      idCom: dto.COMPANY_ID,
      idBran:dto.BRANCH_ID ,
      idMon: dto.MONTH,
      idUnit: dto.UNITS,
      company: dto.company,
      branch: dto.branch,
      // numOfMeters,
      meterNo,
      units,
      consumption,
      year,
      month,
      emission:isNaN(+dto.EMISSION_DETAILS.tco2) ? 0.0 : (+dto.EMISSION_DETAILS.tco2).toFixed(5) ,
      emissionInfo: dto.EMISSION_DETAILS,
      quantity:isNaN(+dto.EMISSION_DETAILS.quantity) ? 0.0 : (+dto.EMISSION_DETAILS.quantity).toFixed(5) ,
      ef_grid:isNaN(+dto.EMISSION_DETAILS.ef_grid) ? 0.0 : (+dto.EMISSION_DETAILS.ef_grid).toFixed(5) ,
      cf_grid:isNaN(+dto.EMISSION_DETAILS.cf_grid) ? 0.0 : (+dto.EMISSION_DETAILS.cf_grid).toFixed(5) ,
    };



  }


  onUpdate(event: any, isNew:boolean) {
    // console.log(event);
    const dialogRef = this.dialog.open(UpdateMunWaterComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });

  }


  onShowFormula($event: any) {
    const dialogRef = this.dialog.open(MunWaterFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        maxHeight: '650px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageMunWaterEntry,
          {
            DATA: {
              MUNICIPAL_WATER_ENTRY_ID: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
            // console.log(data);
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              // console.log(data.DAT.DTO);
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }

  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    // console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.entryId.value = +$event.query.query;
        break;
      } case 'consumption':  {
        this.filterModel.consumption.value = $event.query.query;
        break;
      }  case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      }  case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      }  case 'meterNo':  {
        this.filterModel.meterNo.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      consumption: { dir: '' },
      meterNo: { dir: '' },
      entryId: { dir: '' },
      year: { dir: '' },


    };
    // console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.entryId.dir = $event.direction;
        break;
      } case 'consumption':  {
        this.sortModel.consumption.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'meterNo':  {
        this.sortModel.meterNo.dir = $event.direction;
        break;
      } case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListMunWaterEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListMunWaterEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListMunWaterEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListMunWaterEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }


}
