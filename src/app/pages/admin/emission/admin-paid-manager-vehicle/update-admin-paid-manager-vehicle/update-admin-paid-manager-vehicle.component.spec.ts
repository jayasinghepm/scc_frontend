import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminPaidManagerVehicleComponent } from './update-admin-paid-manager-vehicle.component';

describe('UpdateAdminPaidManagerVehicleComponent', () => {
  let component: UpdateAdminPaidManagerVehicleComponent;
  let fixture: ComponentFixture<UpdateAdminPaidManagerVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminPaidManagerVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminPaidManagerVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
