import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPaidManagerVehicleComponent } from './admin-paid-manager-vehicle.component';

describe('AdminPaidManagerVehicleComponent', () => {
  let component: AdminPaidManagerVehicleComponent;
  let fixture: ComponentFixture<AdminPaidManagerVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPaidManagerVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPaidManagerVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
