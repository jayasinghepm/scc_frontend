import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminVehicleOthersComponent } from './admin-vehicle-others.component';

describe('AdminVehicleOthersComponent', () => {
  let component: AdminVehicleOthersComponent;
  let fixture: ComponentFixture<AdminVehicleOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminVehicleOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminVehicleOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
