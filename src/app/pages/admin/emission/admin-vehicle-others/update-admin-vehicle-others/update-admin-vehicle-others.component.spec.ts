import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminVehicleOthersComponent } from './update-admin-vehicle-others.component';

describe('UpdateAdminVehicleOthersComponent', () => {
  let component: UpdateAdminVehicleOthersComponent;
  let fixture: ComponentFixture<UpdateAdminVehicleOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminVehicleOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminVehicleOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
