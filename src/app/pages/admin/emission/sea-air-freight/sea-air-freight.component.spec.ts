import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeaAirFreightComponent } from './sea-air-freight.component';

describe('SeaAirFreightComponent', () => {
  let component: SeaAirFreightComponent;
  let fixture: ComponentFixture<SeaAirFreightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeaAirFreightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeaAirFreightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
