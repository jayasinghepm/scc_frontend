import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminPaperWasteComponent } from './update-admin-paper-waste.component';

describe('UpdateAdminPaperWasteComponent', () => {
  let component: UpdateAdminPaperWasteComponent;
  let fixture: ComponentFixture<UpdateAdminPaperWasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminPaperWasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminPaperWasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
