import {Component, Inject, OnInit} from '@angular/core';
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {BackendService} from "../../../../@core/rest/bo_service";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {NbDialogService, NbMenuService, NbToastrService} from "@nebular/theme";
import {DOCUMENT} from "@angular/common";
import {UpdateElecComponent} from "../client-electricity/update-elec/update-elec.component";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {ElecFormulaComponent} from "../../../formulas/elec-formula/elec-formula.component";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";
import {UpdateAdminPaperWasteComponent} from "./update-admin-paper-waste/update-admin-paper-waste.component";
import {UserState} from "../../../../@core/auth/UserState";
import {MatDialog} from "@angular/material/dialog";
import {PaperWasteFormulaComponent} from "../../../formulas/paper-waste-formula/paper-waste-formula.component";

@Component({
  selector: 'app-admin-paper-waste',
  templateUrl: './admin-paper-waste.component.html',
  styleUrls: ['./admin-paper-waste.component.scss']
})
export class AdminPaperWasteComponent implements OnInit {

  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      // delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {

      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,
      },
      quantity: {
        title: 'Quantity(kg)',
        type: 'number',
        filter: true,
        sort: true,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'string',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCo2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))


  private filterModel = {
    companyId: { value: '' , type: 1, col: 1},
    branch: { value: '' , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    //todo: float
    quantity: { value: '' , type: 1, col:5},
    entryId: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    quantity: { dir: '' },
    entryId: { dir: '' },
    year: { dir: '' },

  }



  source: LocalDataSource = new LocalDataSource();
  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private  toastSerivce:  NbToastrService,
              private nbMenuService: NbMenuService,
              private dialogService: NbDialogService,
              private dialog: MatDialog,
              @Inject(DOCUMENT) private document: Document
  ) {
    this.selectedCompany = 0;
    // this.loadFiltered(this.pg_current);
    this.loadFiltered(this.pg_current)
    // this.source.load(data);
  }

  public  companines = [];
  public selectedCompany: number;

  ngOnInit(): void {
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companines.push({
        id: 0,
        name: 'All'
      })
      if (d !== undefined && d.length !== 0) {
        d.forEach(c => {
          this.companines.push({
            id: c.id,
            name: c.name
          });
        })
      }
    })

  }
  onChangeCompany(value: any) {
    this.filterModel.companyId.value = value;
    this.loadFiltered(this.pg_current);
  }






  public onUpdate(event: any, isNew: boolean) {
    // console.log(event);
    // console.log(event);
    const dialogRef = this.dialog.open(UpdateAdminPaperWasteComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });


  }
  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListPaperWaste,
      {
        PAGE_NUMBER: this.pg_current,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.branch === undefined || data.branch === '' ){
      return false;
    }
    if (data.meterNo === undefined || data.meterNo === '' ){
      return false;
    }
    if (data.consumption === undefined || data.consumption === '' ){
      return false;
    }
    if (data.units === undefined || data.units === '' ){
      return false;
    }


    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {


    return {
      ...dto,
      id : dto.entryId,
      idCom: dto.companyId,
      idBran: dto.branchId,
      idMon: dto.month,
      month : dto.mon,
      emission: isNaN(+dto.emissionDetails.tco2)?   0.0 : (+dto.emissionDetails.tco2).toFixed(5),


    };



  }



  public onShowcontextMenu($event: any) {
    const dialogRef = this.dialog.open(ElecFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        maxHeight: '650px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManagePaperWaste,
          {
            DATA: {
             entryId: $event.data.id,
              isDeleted: 1,
            }
          }).then(data=> {
          // console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        })


        // });
      }
    });
  }
  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.entryId.value = +$event.query.query;
        break;
      }   case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
      case 'quantity': {
        this.filterModel.quantity.value = $event.query.query;
        break;
      }

    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      quantity: { dir: '' },

      entryId: { dir: '' },
      year: { dir: '' },

    };
    console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.entryId.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }   case 'quantity':  {
        this.sortModel.quantity.dir = $event.direction;
        break;
      }  case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListPaperWaste,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            // if ((list.length === 0 || list.length <= 20) && this.pg_current > 0) {
            //   this.pg_current = 0;
            //   this.loadFiltered(this.pg_current);
            //
            // }
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListPaperWaste,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }

    // this.boService.sendRequestToBackend(
    //   RequestGroup.Emission,
    //   RequestType.ListElectricityEntry,
    //   {
    //     PAGE_NUMBER: pageNum,
    //     FILTER_MODEL: effectiveFilter,
    //   }
    // ).then(data => {
    //   if (data.HED != undefined && data.HED.RES_STS == 1) {
    //     if (data.DAT != undefined && data.DAT.LIST != undefined) {
    //       let list = [];
    //       this.totalCount = data.DAT.TOTAL_COUNT;
    //       this.pages = Math.ceil(this.totalCount/20);
    //       this.fetchedCount = data.DAT.LIST.length;
    //       if (this.fetchedCount < 20) {
    //         this.disableNext = true;
    //       } else {
    //         this.disableNext = false;
    //       }
    //       data.DAT.LIST.forEach(val => {
    //         if (val != undefined) {
    //           list.push(this.fromListRequest(val));
    //         }
    //       });
    //       this.source.load(list);
    //     } else {
    //       //  todo: show snack bar
    //       console.log('error data 1')
    //     }
    //   } else {
    //     //  todo: show snack bar
    //     console.log('error data 2')
    //   }
    // })
  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListPaperWaste,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListPaperWaste,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }

  onShowFormula($event: any) {
    const dialogRef = this.dialog.open(PaperWasteFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }

}
