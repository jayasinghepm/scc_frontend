import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPaperWasteComponent } from './admin-paper-waste.component';

describe('AdminPaperWasteComponent', () => {
  let component: AdminPaperWasteComponent;
  let fixture: ComponentFixture<AdminPaperWasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPaperWasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPaperWasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
