import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {BackendService} from "../../../../../@core/rest/bo_service";

@Component({
  selector: 'app-update-waste-dis',
  templateUrl: './update-waste-dis.component.html',
  styleUrls: ['./update-waste-dis.component.scss']
})
export class UpdateWasteDisComponent implements OnInit {
  private isSaved = false;



  public jsonBody = {
    WASTE_DISPOSAL_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    YEAR: undefined,
    MONTH: undefined,
    UNITS: undefined,
    AMOUNT_DISPOSED: undefined,
    DISPOSAL_METHOD: undefined,
    WASTE_TYPE: undefined,
    company : undefined,
    branch: undefined,
    mon: undefined,
    disposal: undefined,
    waste: undefined,
  }

  public companies = [];
  public branches = [];
  public months = [];
  public years = [];
  public wastes = [];
  public disMethods = [];


  constructor( public dialogRef: MatDialogRef<UpdateWasteDisComponent>,
               public masterData:MassterDataService,
               private boService: BackendService,
               private toastSerivce: NbToastrService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    // console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew)
  }

  private init(data:any, isNew:boolean) {
    this.masterData.loadCompaninesForDataEnry().subscribe(d => {
      this.companies = d;
    })
    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      this.years = d;
    })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getWasteDisMethodsFull().subscribe(d => {
      this.disMethods = d;
    });

    //----------------------------branch search ---------------------
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    //---------------------------------------------------------------

    if (!isNew) {
      this.jsonBody.WASTE_DISPOSAL_ENTRY_ID = data.id;
      this.jsonBody.COMPANY_ID = data.idCom;
      this.jsonBody.BRANCH_ID = data.idBran;
      this.jsonBody.DISPOSAL_METHOD = data.idDisMeth;
      this.jsonBody.WASTE_TYPE = data.idWaste;
      this.jsonBody.MONTH = data.idMon;
      this.jsonBody.AMOUNT_DISPOSED = data.quantity;
      this.jsonBody.YEAR = data.year;

      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        // console.log(d);
        this.years = d;
      });
      this.masterData.getWasteDisTypes(this.jsonBody.DISPOSAL_METHOD).subscribe(d => {
        this.wastes = d;
      })

      //----------------branch search-------------------------
      this.branches.push({
        id: data.idBran, name: data.branch
      })
      this.selectedComId =  this.jsonBody.COMPANY_ID;

      // ----------------------------------------------------
    }
  }

  public selectedComId = 0;
  onChangeCompany(value: any) {
    this.selectedComId = value;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    this.jsonBody.company = this.companies.filter(c => c.id === value)[0].name

    // this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      // console.log(d);
      this.years = d;
    })
  }

  onChangeDisMethod(value: any) {
    // console.log(value);
    this.jsonBody.disposal = this.disMethods.filter(d => d.id === value)[0].name;
    this.masterData.getWasteDisTypes(value).subscribe(d => {
      this.wastes = d;

    })
  }

  private validateEntry( onEdit: boolean): boolean {
    // console.log(this.jsonBody)
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0 ){
    //   // console.log(1)
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){
      // console.log(2)
      return false;
    }
    if (this.jsonBody.WASTE_TYPE === undefined || this.jsonBody.WASTE_TYPE === '' ){
      // console.log(3)
      return false;
    }
    if (this.jsonBody.DISPOSAL_METHOD === undefined || this.jsonBody.DISPOSAL_METHOD === '' ){
      // console.log(4)
      return false;
    }
    if (this.jsonBody.AMOUNT_DISPOSED === undefined || this.jsonBody.AMOUNT_DISPOSED === '' ){
      // console.log(5)
      return false;
    }
    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      // console.log(6)
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      // console.log(7)
      return false;
    }
    if (onEdit && (this.jsonBody.WASTE_DISPOSAL_ENTRY_ID=== undefined || this.jsonBody.WASTE_DISPOSAL_ENTRY_ID <= 0)) {
      // console.log(8)
      return false;
    }
    return true;
  }
  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
        if (this.validateEntry( true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageWasteDisposalEntry,
            { DATA: this.jsonBody}
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })

            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }

    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWasteDisposalEntry,
          {DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.selectedComId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  onChangeWaste($event: MatSelectChange) {
    this.jsonBody.waste = this.wastes.filter(w => w.id === $event.value)[0].name;
  }
}
