import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSawDustTransComponent } from './admin-saw-dust-trans.component';

describe('AdminSawDustTransComponent', () => {
  let component: AdminSawDustTransComponent;
  let fixture: ComponentFixture<AdminSawDustTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSawDustTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSawDustTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
