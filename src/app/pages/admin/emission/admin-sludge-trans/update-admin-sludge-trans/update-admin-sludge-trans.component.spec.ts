import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminSludgeTransComponent } from './update-admin-sludge-trans.component';

describe('UpdateAdminSludgeTransComponent', () => {
  let component: UpdateAdminSludgeTransComponent;
  let fixture: ComponentFixture<UpdateAdminSludgeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminSludgeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminSludgeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
