import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSludgeTransComponent } from './admin-sludge-trans.component';

describe('AdminSludgeTransComponent', () => {
  let component: AdminSludgeTransComponent;
  let fixture: ComponentFixture<AdminSludgeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSludgeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSludgeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
