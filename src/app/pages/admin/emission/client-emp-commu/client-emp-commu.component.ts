import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {BackendService} from "../../../../@core/rest/bo_service";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {UserState} from "../../../../@core/auth/UserState";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {UpdateAirtravelComponent} from "../client-air-travel/update-airtravel/update-airtravel.component";
import {MatDialog} from "@angular/material";
import {UpdateEmpCommComponent} from "./update-emp-comm/update-emp-comm.component";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";
import {FireExtFormulaComponent} from "../../../formulas/fire-ext-formula/fire-ext-formula.component";
import {EmpCommFormulaComponent} from "../../../formulas/emp-comm-formula/emp-comm-formula.component";

@Component({
  selector: 'app-client-emp-commu',
  templateUrl: './client-emp-commu.component.html',
  styleUrls: ['./client-emp-commu.component.scss']
})
export class ClientEmpCommuComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;

  settings = {


    
    mode: 'external',
    pager: {
      display: false,
    },

   // selectMode: 'multi',
    actions: {
      // delete: true,
      delete: true,
      add: true,
      edit: true,
      select: true,
    },

    add: {
      addButtonContent: '<i class="nb-plus"></i>',
     createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
     confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
     cancelButtonContent: '<i class="nb-close"></i>',
     confirmSave: true,
    },

    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      company: {
        title: 'Company',
        type: 'string',
        filter: true,
        sort: true,
      },
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,
      },
      empIDorName: {
        title: 'Employee',
        type: 'string',
        filter: true,
        sort: true,
      },
      // twoWay: {
      //   title: 'Two way',
      //   type: 'string',
      //   filter: true,
      //   sort: true,
      //   editor: {
      //     type: 'checkbox',
      //     config: {
      //       true: 'Yes',
      //       false: 'No',
      //     }
      //   }
      // },paid: {
      //   title: 'Paid by Company',
      //   type: 'string',
      //   filter: true,
      //   sort: true,
      //   editor: {
      //     type: 'checkbox',
      //     config: {
      //       true: 'Yes',
      //       false: 'No',
      //     }
      //   }
      // },
      // noOfWorkingDays: {
      //   title: 'Working Days',
      //   type: 'number',
      //   filter: true,
      //   sort: true,
      // },

      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'number',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCo2)',
        type: 'string',
        filter: false,
        sort: false,
        editable: false,
        addable: false,
      },

    },
  };

  private filterModel = {
    company: { value: '' , type: 4, col: 3},
    branch: { value: '' , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    empId: { value: '' , type: 4, col: 3},
    id: { value: 0 , type: 1, col: 1},
    noOfWorkingDays: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
    twoWay: { value: '' , type: 4, col: 3},
    paid: { value: '' , type: 4, col: 3},
    companyId: { value: 0 , type: 1, col: 1},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    empId: { dir: '' },
    id: { dir: '' },
    noOfWorkingDays: { dir: '' },
    year: { dir: '' },
    twoWay: { dir: '' },
    paid: { dir: '' },
  }

  mySetting = JSON.parse(JSON.stringify(this.settings))

  source: LocalDataSource = new LocalDataSource();


  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private dialog: MatDialog,
    private  toastSerivce:  NbToastrService) {
    this.selectedCompany = 0;
    // this.loadFiltered(this.pg_current);
    this.loadFiltered(this.pg_current)
    // this.source.load(data);
  }

  public  companines = [];
  public selectedCompany: number;

  ngOnInit(): void {
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companines.push({
        id: 0,
        name: 'All'
      })
      if (d !== undefined && d.length !== 0) {
        d.forEach(c => {
          this.companines.push({
            id: c.id,
            name: c.name
          });
        })
      }
    })

  }
  onChangeCompany(value: any) {
    this.filterModel.companyId.value = value;
    this.loadFiltered(this.pg_current);
  }



  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.branch === undefined || data.branch == "") {
      //show snack bar
      return false;
    }
    if (data.company === undefined || data.company == "") {
      //show snack bar
      return false;
    }
    // if (data.annualEmission === undefined || data.annualEmission == "") {
    //   //show snack bar
    //   return false;
    // }
    if (data.company_distance === undefined || data.company_distance == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    } if (data.ownTrans_noOfTrips === undefined || data.ownTrans_noOfTrips == "") {
      //show snack bar
      return false;
    } if (data.ownTrans_fuelEconomy === undefined || data.ownTrans_fuelEconomy == "") {
      //show snack bar
      return false;
    } if (data.ownTrans_noOfTrips === undefined || data.ownTrans_noOfTrips == "") {
      //show snack bar
      return false;
    }
    if (data.companyFuelMode === undefined || data.companyFuelMode == "") {
      //show snack bar
      return false;
    }
    if (data.own_distance === undefined || data.own_distance == "") {
      //show snack bar
      return false;
    }
    if (data.ownTransMode === undefined || data.ownTransMode == "") {
      //show snack bar
      return false;
    }
    if (data.public_distance === undefined || data.public_distance == "") {
      //show snack bar
      return false;
    }
    if (data.empIDorName === undefined || data.empIDorName == "") {
      //show snack bar
      return false;
    }

    if (data.noEmissionMode === undefined || data.noEmissionMode == "") {
      //show snack bar
      return false;
    }
    if (data.noEmission_distance === undefined || data.noEmission_distance == "") {
      //show snack bar
      return false;
    }
    if (data.publicTranMode === undefined || data.publicTranMode == "") {
      //show snack bar
      return false;
    }
    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    // if (data.month === undefined || data.month == "") {
    //   //show snack bar
    //   return false;
    // }
    // if (data.transit1 === undefined || data.transit1 == "") {
    //   //show snack bar
    //   return false;
    // }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    if ( (data.company === undefined || data.company === '') ){
      return false;
    }
    return true;

  }

  private loadData() {
    let jsonBody   = {
      // FILTER_MODEL: {
      //   branchId: { value: UserState.getInstance().branchId , type: 1, col: 1}
      // }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListEmpCommuting,
      {
        PAGE_NUMBER: this.pg_current,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.list.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let month;
    let year;
    let id;
    let noEmissionMode_UP;
    let noEmissionMode_DOWN;
    let publicTransMode_UP;
    let publicTransMode_DOWN;
    let ownTransMode_UP;
    let ownTransMode_DOWN;
    let ownTrans_fuelType_UP;
    let ownTrans_fuelType_DOWN


    // this.masterData.getBranchName(dto.branchId).subscribe(data => { branch = data; }).unsubscribe();
    // // this.masterData.getA
    // this.masterData.getCompanyName(dto.companyId).subscribe(data => { company = data; }).unsubscribe();
    this.masterData.getMonthName(dto.month).subscribe(data => { month = data; }).unsubscribe();
    this.masterData.getVehicleTypeName(dto.noEmissionMode_UP).subscribe( data => { noEmissionMode_UP = data});
    this.masterData.getVehicleTypeName(dto.noEmissionMode_DOWN).subscribe( data => { noEmissionMode_DOWN = data});
    this.masterData.getPublicVehicleName(dto.publicTransMode_UP).subscribe(d => {  publicTransMode_UP = d;})
    this.masterData.getVehicleTypeName(dto.publicTransMode_DOWN).subscribe(d => {  publicTransMode_DOWN = d;})
    this.masterData.getVehicleTypeName(dto.ownTransMode_UP).subscribe(d => {  ownTransMode_UP = d;})
    this.masterData.getVehicleTypeName(dto.ownTransMode_DOWN).subscribe(d => {  ownTransMode_DOWN = d;})
    this.masterData.getFuelTypeName(dto.ownTrans_fuelType_UP).subscribe(d => { ownTrans_fuelType_UP = d;});
    this.masterData.getFuelTypeName(dto.ownTrans_fuelType_DOWN).subscribe(d => { ownTrans_fuelType_DOWN = d;});
    // this.masterData.getEmployeeName(dto.empId).subscribe(d => { empIDorName = d;})
    year = dto.year;
    id = dto.id;

    return {
      id,
      idCom: dto.companyId,
      idBran: dto.branchId,
      company: dto.company,
      branch: dto.branch,
      paid: dto.paid,
      twoWay: dto.twoWay,
      paidByCom: dto.paidByCom,
      isTwoWay: dto.isTwoWay,
      noOfWorkingDays: dto.noOfWorkingDays,
      empId: dto.empId,
      empIDorName: dto.empId,
      idNoEmUp: dto.noEmissionMode_UP,
      idNoEmDown: dto.noEmissionMode_DOWN,
      noEmissionMode_DOWN,
      noEmissionMode_UP,
      noEmission_distance_UP: dto.noEmission_distance_UP,
      noEmission_distance_DOWN: dto.noEmission_distance_DOWN,
      idPubTransUp:dto.publicTransMode_UP ,
      idPubTransDown: dto.publicTransMode_DOWN,
      publicTransMode_UP,
      publicTransMode_DOWN,
      publicTrans_distance_UP: dto.publicTrans_distance_UP,
      publicTrans_distance_DOWN: dto.publicTrans_distance_DOWN,
      idOwnTranUp: dto.ownTransMode_UP,
      idOwnTransDown: dto.ownTransMode_DOWN,
      ownTransMode_UP,
      ownTransMode_DOWN,
      ownTrans_distance_UP: dto.ownTrans_distance_UP,
      ownTrans_distance_DOWN: dto.ownTrans_distance_DOWN,
      ownTrans_fuelEconomy_UP: dto.ownTrans_fuelEconomy_UP,
      ownTrans_fuelEconomy_DOWN: dto.ownTrans_fuelEconomy_DOWN,
      ownTrans_fuelType_DOWN,
      ownTrans_fuelType_UP,
      idFuelTypeUp: dto.ownTrans_fuelType_UP ,
      idFuelTypeDown: dto.ownTrans_fuelType_DOWN,

      companyPetrolLiters: dto.companyPetrolLiters,
      companyDieselLiters: dto.companyDieselLiters,
      ownPetrolLiters: dto.ownPetrolLiters,
      ownDieselLiters: dto.ownDieselLiters,

      emissionInfo: dto.EMISSION_DETAILS,
      emission: isNaN(+dto.EMISSION_DETAILS.tco2) ? 0.0 : (+dto.EMISSION_DETAILS.tco2).toFixed(5),
      year,
      month,
      idMon: dto.month,
    };



  }


  onUpdate(event: any, isNew:boolean) {
    // console.log(event);
    const dialogRef = this.dialog.open(UpdateEmpCommComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '820px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      this.loadFiltered(this.pg_current);
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });

  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageEmpCommuting,
          {
            DATA: {
              id: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }

  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.id.value = $event.query.query;
        break;
      }  case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      }  case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      }  case 'empIDorName':  {
        this.filterModel.empId.value = $event.query.query;
        break;
      }  case 'noOfWorkingDays':  {
        this.filterModel.noOfWorkingDays.value = $event.query.query;
        break;
      }  case 'twoWay':  {
        this.filterModel.twoWay.value = $event.query.query;
        break;
      }  case 'paid':  {
        this.filterModel.paid.value = $event.query.query;
        break;
      }  case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      empId: { dir: '' },
      id: { dir: '' },
      noOfWorkingDays: { dir: '' },
      year: { dir: '' },
      twoWay: { dir: '' },
      paid: { dir: '' },


    };
    // console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.id.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'empIDorName':  {
        this.sortModel.empId.dir = $event.direction;
        break;
      }  case 'noOfWorkingDays':  {
        this.sortModel.noOfWorkingDays.dir = $event.direction;
        break;
      }  case 'twoWay':  {
        this.sortModel.twoWay.dir = $event.direction;
        break;
      }  case 'paid':  {
        this.sortModel.paid.dir = $event.direction;
        break;
      }  case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListEmpCommuting,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListEmpCommuting,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }

    // this.boService.sendRequestToBackend(
    //   RequestGroup.Emission,
    //   RequestType.ListElectricityEntry,
    //   {
    //     PAGE_NUMBER: pageNum,
    //     FILTER_MODEL: effectiveFilter,
    //   }
    // ).then(data => {
    //   if (data.HED != undefined && data.HED.RES_STS == 1) {
    //     if (data.DAT != undefined && data.DAT.LIST != undefined) {
    //       let list = [];
    //       this.totalCount = data.DAT.TOTAL_COUNT;
    //       this.pages = Math.ceil(this.totalCount/20);
    //       this.fetchedCount = data.DAT.LIST.length;
    //       if (this.fetchedCount < 20) {
    //         this.disableNext = true;
    //       } else {
    //         this.disableNext = false;
    //       }
    //       data.DAT.LIST.forEach(val => {
    //         if (val != undefined) {
    //           list.push(this.fromListRequest(val));
    //         }
    //       });
    //       this.source.load(list);
    //     } else {
    //       //  todo: show snack bar
    //       console.log('error data 1')
    //     }
    //   } else {
    //     //  todo: show snack bar
    //     console.log('error data 2')
    //   }
    // })
  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListEmpCommuting,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListEmpCommuting,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }

  onShowFormula($event: any) {
    const dialogRef = this.dialog.open(EmpCommFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        maxHeight: '650px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }

  onShowFormula2($event: any) {
    
    let  data1 = $event.selected
        console.log("sele===========",data1)
       
    
  }

}
