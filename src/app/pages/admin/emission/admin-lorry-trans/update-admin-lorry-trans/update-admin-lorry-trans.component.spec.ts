import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminLorryTransComponent } from './update-admin-lorry-trans.component';

describe('UpdateAdminLorryTransComponent', () => {
  let component: UpdateAdminLorryTransComponent;
  let fixture: ComponentFixture<UpdateAdminLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
