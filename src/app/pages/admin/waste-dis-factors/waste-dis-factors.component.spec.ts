import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WasteDisFactorsComponent } from './waste-dis-factors.component';

describe('WasteDisFactorsComponent', () => {
  let component: WasteDisFactorsComponent;
  let fixture: ComponentFixture<WasteDisFactorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WasteDisFactorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WasteDisFactorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
