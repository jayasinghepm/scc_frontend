import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {BackendService} from "../../../@core/rest/bo_service";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-activity-logs',
  templateUrl: './activity-logs.component.html',
  styleUrls: ['./activity-logs.component.scss']
})
export class ActivityLogsComponent implements OnInit {

  constructor(
    private boService: BackendService,
    public dialogRef: MatDialogRef<ActivityLogsComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any
  ) {
    // console.log(this.popupData)
    this.loadLogs();
  }
  public logs: {log:string , addedAt: string}[] = []

  ngOnInit(
    ) {
  }

  closeDialog() {
    this.dialogRef.close(true);
  }

  public loadLogs() {
    const jsonBody = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        userId: { type: 1, col: 1, value: this.popupData.userId},
      },
      SORT_MODEL: {
        logTime: { dir: 'desc' },
      },
    };
    this.boService.sendRequestToBackend(RequestGroup.LoginUser, RequestType.ListUserActLogs , jsonBody)
      .then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            data.DAT.list.forEach(val => {
              this.logs.push({log: val.actLog, addedAt: val.logTime})
            })
          }
        }
      })

  }
}
