import { Component, OnInit } from '@angular/core';
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {BackendService} from "../../../@core/rest/bo_service";
import * as moment from 'moment';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {

  public projectStatusOptions;
  public paymentStatusOptions;
  public projectStatusData = [
    { name: 'New', value: 0,},
    { name: 'Signed', value: 0,},
    {name: 'Approved', value: 0,},
    {name: 'Data Entry', value: 0},
    {name: 'Completeness Check', value: 0},
    {name: 'First Draft', value: 0 },
    {name: 'Final Submission', value: 0},
  ];
  public payemntStatusData = [
    { name: 'Not Paid', value: 0,},
    { name: 'Initial', value: 0,},
    {name: 'Second', value: 0,},
    {name: 'Final', value: 0},
  ]
  public expiredProjects :{name: string, comId: number, endDate: string}[] = [];
  public reports : {name: string, status: string} [] = []

  constructor(private boService: BackendService) {
    this.projectStatusOptions = {
      title : {
        text: '',
        subtext: '',
        x:'center'
      },
      tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['New', 'Signed', 'Approved', 'Data Entry', 'Completeness Check', 'First Draft', 'Final Submission', 'Verification'],

        // selected: data.selected
      },
      series : [
        {
          name: 'Status',
          type: 'pie',
          radius : '55%',
          center: ['40%', '50%'],
          data: [
            { name: 'New', value: 4,},
            { name: 'Singed', value: 7,},
            {name: 'Approved', value: 8,},
            {name: 'Data Entry', value: 10},
            {name: 'Completeness Check', value: 13},
            {name: 'First Draft', value: 32 },
            {name: 'Final Submission', value: 2},
            {name: 'Verification', value: 3},
          ],
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };
    this.paymentStatusOptions = {
      title : {
        text: '',
        subtext: '',
        x:'center'
      },
      tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['Not Paid', 'Initial', 'Second', 'Final'],

        // selected: data.selected
      },
      series : [
        {
          name: 'Status',
          type: 'pie',
          radius : '55%',
          center: ['40%', '50%'],
          data: [
            { name: 'Not Paid', value: 4,},
            { name: 'Initial', value: 7,},
            {name: 'Second', value: 8,},
            {name: 'Final', value: 10},
          ],
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };

    this.expiredProjects.push(...[
      {name: 'ghg_com-11', comId: 3, endDate: '20-Nov-2019'},
      {name: 'ghg_com-12', comId: 3, endDate: '19-Oct-2019'},
    ])
    this.reports.push(...[
      {name: 'ghg_com-23', status: 'First Draft'},
      {name: 'ghg_com_25', status: 'Final Submission'}
    ])
    this.loadData();
  }

  ngOnInit() {

  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          status: { value: 8, type: 2, col: 1}
        }
      }
    ).then(data => {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];

          this.expiredProjects = [];
          this.reports = [];
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              switch(val.status) {
                case 1: // new
                {
                  this.projectStatusData.filter(v => { return v.name === 'New'})[0].value++;
                  break;
                }
                case 2: // singend
                {
                  this.projectStatusData.filter(v => { return v.name === 'Signed'})[0].value++;
                  break;
                }
                case 3 : // approved
                {
                  this.projectStatusData.filter(v => { return v.name === 'Approved'})[0].value++;
                  break;
                }
                case 4:  // data entry
                {
                  this.projectStatusData.filter(v => { return v.name === 'Data Entry'})[0].value++;
                  break;
                }
                case 5: // completeness check
                {
                  this.projectStatusData.filter(v => { return v.name === 'Completeness Check'})[0].value++;
                  break;
                }
                case 6: // first draft submission
                {
                  this.projectStatusData.filter(v => { return v.name === 'First Draft'})[0].value++;
                  this.reports.push({name: val.name, status: 'First Draft'})
                  break;
                }
                case 7: // final submission
                {
                  this.projectStatusData.filter(v => { return v.name === 'Final Submission'})[0].value++;
                  this.reports.push({name: val.name, status: 'Final Submission'})
                  break;
                }
              }
              if (val.firstPayDate !== undefined) {
                this.payemntStatusData.filter(v=> { return v.name === 'Initial'})[0].value++;
              } else if (val.secondPayDate !== undefined) {
                this.payemntStatusData.filter(v=> {return v.name === 'Second'})[0].value++;
              } else if (val.finalPayDate) {
                this.payemntStatusData.filter(v=> { return v.name === 'Final'})[0].value++;
              } else {
                // not paid
                this.payemntStatusData.filter(v=> { return v.name === 'Not Paid'})[0].value++;
              }

              if (this.isExpired(val)) {
                this.expiredProjects.push({ name: val.name, endDate: val.endDate , comId: 1})
              }

            }
            this.projectStatusOptions = {
              title : {
                text: '',
                subtext: '',
                x:'center'
              },
              tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
              },
              legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 20,
                bottom: 20,
                data: ['New', 'Signed', 'Approved', 'Data Entry', 'Completeness Check', 'First Draft', 'Final Submission', 'Verification'],

                // selected: data.selected
              },
              series : [
                {
                  name: 'Status',
                  type: 'pie',
                  radius : '55%',
                  center: ['40%', '50%'],
                  data: this.projectStatusData,
                  itemStyle: {
                    emphasis: {
                      shadowBlur: 10,
                      shadowOffsetX: 0,
                      shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                  }
                }
              ]
            };
            this.paymentStatusOptions = {
              title : {
                text: '',
                subtext: '',
                x:'center'
              },
              tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
              },
              legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 20,
                bottom: 20,
                data: ['Not Paid', 'Initial', 'Second', 'Final'],

                // selected: data.selected
              },
              series : [
                {
                  name: 'Status',
                  type: 'pie',
                  radius : '55%',
                  center: ['40%', '50%'],
                  data: this.payemntStatusData,
                  itemStyle: {
                    emphasis: {
                      shadowBlur: 10,
                      shadowOffsetX: 0,
                      shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                  }
                }
              ]
            };
          });
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }
  
  
  private isExpired(project:any) {
    let remdays;
    if (project.extendedDate !== undefined && project.extendedDate  !== null) {
      // console.log('extended')
      const extend = moment(project.extendedDate, 'YYYY-MM-DD');
      const today = moment().startOf('day');
      remdays = extend.diff(today, 'days');
    } else {
      const today = moment().startOf('day');
      const end = moment(project.endDate , 'YYYY-MM-DD');
      // console.log(start);
      // console.log(end);
      const diff = end.diff(today, 'days');
      remdays = diff

    }
    return remdays < 0 ? true : false;
  }

}
