import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService, NbWindowService} from "@nebular/theme";
import {AddClientComponent} from "./add-client/add-client.component";
import {RemoveClientComponent} from "./remove-client/remove-client.component";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {MatDialog} from "@angular/material";
import {UpdateAirtravelComponent} from "../emission/client-air-travel/update-airtravel/update-airtravel.component";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";

@Component({
  selector: 'app-admin-clients',
  templateUrl: './admin-clients.component.html',
  styleUrls: ['./admin-clients.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminClientsComponent implements OnInit {

  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;

  private filterModel = {
    id: { value:0, col: 1, type: 1},
    firstName:{ value:'', col: 3, type: 4},
    lastName: { value:'', col: 3, type: 4},
    title:{ value:'', col: 3, type: 4},
    email: { value:'', col: 3, type: 4},
    telephoneNo: { value:'', col: 3, type: 4},
    sector: { value:'', col: 3, type: 4},
    mobileNo:{ value:'', col: 3, type: 4},
    position: { value:'', col: 3, type: 4},
    company: { value:'', col: 3, type: 4},
    branch: { value:'', col: 3, type: 4},
    companyId: { value:0, col: 1, type: 1},
  }

  private sortModel = {
    id: {dir: ''},
    firstName: {dir: ''},
    lastName:{dir: ''},
    title: {dir: ''},
    email: {dir: ''},
    telephoneNo: {dir: ''},
    mobileNo: {dir: ''},
    position:{dir: ''},
    company: {dir: ''},
    sector: {dir: ''},
    branch: {dir: ''},
  }

  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
      edit: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: true,
        sort: true,
      },
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,

      },
      firstName: {
        title: 'First Name',
        type: 'string',
        filter: true,
        sort: true,
      },lastName: {
        title: 'Last Name',
        type: 'string',
        filter: true,
        sort: true,
      },
      email: {
        title: 'Email',
        type: 'string',
        filter: true,
        sort: true,
      },
      mobileNo: {
        title: 'Mobile No',
        type: 'string',
        filter: true,
        sort: true,

      },
      telephoneNo: {
        title: 'Telephone NO',
        type: 'string',
        filter: true,
        sort: true,

      },

    }
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))

  source: LocalDataSource = new LocalDataSource();
  public selectedCompany: number;


  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private  toastSerivce:  NbToastrService,
    private windowService: NbWindowService,
    private dialog: MatDialog
  ) {
    this.selectedCompany = 0;
    this.loadData();
  }

  public companines = []
  ngOnInit() {
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companines.push({
        id: 0,
        name: 'All'
      })
      if (d !== undefined && d.length !== 0) {
        d.forEach(c => {
          this.companines.push({
            id: c.id,
            name: c.name
          });
        })
      }
    })
  }

  onChangeCompany(value: any) {
    this.filterModel.companyId.value = value;
    this.loadFiltered(this.pg_current);
  }

  public onUpdate($event: any, isEdit: boolean) {
    // console.log($event)
    const dialogRef = this.dialog.open(AddClientComponent,
      {
        data : { isEdit,data: $event.data},
        width: '480px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
    dialogRef.afterClosed().subscribe(d => {
      // console.log(d);
      if (d) {
        this.loadData();
      } });
  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
          RequestType.ManageClient,
          {
            DATA: {
              id: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.pg_current = 0;
              this.loadFiltered(0);
            }
          }
        });
      }
    });
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListClient,
      {
        PAGE_NUM: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.list.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

   // todo : validation


  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let id;
    let name;
    let email
    let mobileNo;
    let telephoneNo;
    let agreementStartDate;
    let agreementEndDate;
    let sector;
    let clientCode;
    let userId;


    id = dto.id;

    email = dto.email;
    mobileNo = dto.mobileNo;
    telephoneNo = dto.telephoneNo;
    sector = dto.sector;
    clientCode = dto.clientCode;
    agreementStartDate = dto.agreementStartDate;
    agreementEndDate = dto.agreementEndDate
    userId = dto.userId;

    // id: this.popupData.isEdit ? this.clientId : -1,
    //   title: this.titleModel,
    //   firstName: this.firstNameModel,
    //   lastName: this.lastnameModel,
    //   branchId: this.branchModel,
    //   companyId: this.companyModel,
    //   userId: this.userId,
    //   entitlements: entitleMents ,
    //   agreementStartDate: this.convert(this.startDate),
    //   agreementEndDate: this.convert(this.endDate),
    //   clientCode: this.code,
    //   sector: this.sector,
    //   company: this.company,
    //   branch: this.branch,
    return {
      id,
      company: dto.company,
      branch: dto.branch,
      firstName: dto.firstName ,
      lastName: dto.lastName,
      idCom: dto.companyId,
      idBran: dto.branchId,
      email,
      mobileNo,
      telephoneNo,
      sector,
      clientCode,
      agreementStartDate,
      agreementEndDate,
      userId
    };



  }



  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadData();
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadData();
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    switch($event.query.column.id) {
      case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      }  case 'firstName':  {
        this.filterModel.firstName.value = $event.query.query;
        break;
      }  case 'lastName':  {
        this.filterModel.lastName.value = $event.query.query;
        break;
      } case 'id':  {
        this.filterModel.id.value = $event.query.query;
        break;
      } case 'email':  {
        this.filterModel.email.value = $event.query.query;
        break;
      } case 'mobileNo':  {
        this.filterModel.mobileNo.value = $event.query.query;
        break;
      }case 'telephoneNo':  {
        this.filterModel.telephoneNo.value = $event.query.query;
        break;
      }case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  = {
      id: {dir: ''},
      firstName: {dir: ''},
      lastName:{dir: ''},
      title: {dir: ''},
      email: {dir: ''},
      telephoneNo: {dir: ''},
      mobileNo: {dir: ''},
      position:{dir: ''},
      company: {dir: ''},
      sector: {dir: ''},
      branch: {dir: ''},
    }
    // console.log($event)
    switch($event.id) {
      case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'firstName':  {
        this.sortModel.firstName.dir = $event.direction;
        break;
      }  case 'lastName':  {
        this.sortModel.lastName.dir = $event.direction;
        break;
      } case 'id':  {
        this.sortModel.id.dir = $event.direction;
        break;
      } case 'email':  {
        this.sortModel.email.dir = $event.direction;
        break;
      } case 'mobileNo':  {
        this.sortModel.mobileNo.dir = $event.direction;
        break;
      }case 'telephoneNo':  {
        this.sortModel.telephoneNo.dir = $event.direction;
        break;
      }case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListClient,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListClient,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListClient,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListClient,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }

  public createClientProfiles($event: MouseEvent) {
    if (this.selectedCompany <= 0)
    {
      this.toastSerivce.show('Select Company First', '', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      })
      return;
    }
    this.boService.sendRequestToBackend(RequestGroup.LoginUser, RequestType.CreateBranchLoginProfile, {
      comId: this.selectedCompany
    }).then(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined) {
          if(data.DAT.profiles !== undefined) {
            let keys = Object.keys(data.DAT.profiles );
            let csvData = [['User Login Name', 'Password']];
            // @ts-ignore
            for (const k of keys){
              csvData.push([k, data.DAT.profiles[k]]);
            }
            this.downloadCSvFile(csvData);
          }
        }

      }
    })
  }

  downloadCSvFile(data: any) {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    let csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    let csvArray = csv.join('\r\n');

    var a = document.createElement('a');
    var blob = new Blob([csvArray], {type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = "password.csv";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }



}


