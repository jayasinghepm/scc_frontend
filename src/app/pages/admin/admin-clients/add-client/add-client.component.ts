import {Component, Inject, OnInit, Optional} from '@angular/core';
import {BackendService} from "../../../../@core/rest/bo_service";
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MassterDataService} from "../../../../@core/service/masster-data.service";

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
  public endDate:any;
  public startDate: any;
  private userId: number;
  private clientId: number;
  private isDisabledButton = false;

   public titles = [];
   public branches = [];
   public companines = [];


  public isClientActive = false;
  // public btnText =

  public userName;
  public password;
  public header = "Create Client"
  public btnText = ""
  public disableClose = false;

  public titleConfig = {
    search: true,

  };


  public titleModel = ''
  public branchModel = 0
  public companyModel = 0
  public emissionSrcModel;

  public company;
  public branch;


  constructor(
    private boService:BackendService,
    public dialogRef: MatDialogRef<AddClientComponent>,
    private  toastSerivce:  NbToastrService,
    private masterData: MassterDataService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {

     this.masterData.getCompaninesFull().subscribe(d => { this.companines = d;})
     this.masterData.getTitles().subscribe(d => { this.titles = d;})
     if (popupData.isEdit) {
       this.header = "Edit Client"
       this.isClientActive = true;
       this.btnText = "Save";
       this.userId = this.popupData.data.userId;
       this.clientId = this.popupData.data.id;
     } else {
       this.header = "Create Client"
       this.isClientActive = false;
       this.btnText = "Save"
     }
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
  }

  ngOnInit() {

    if (this.popupData.isEdit) {
      //----------------branch search-------------------------
      this.branches.push({
        id: this.popupData.data.idBran, name: this.popupData.data.branch
      })
      // this.selectedComId =  this.jsonBody.COMPANY_ID;

      // ----------------------------------------------------

    }
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.companyModel != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.companyModel, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }


  onChangeDate($event, isStart) {
    if (isStart) {
      // console.log(this.convert(this.startDate))
    }else {
      // console.log(this.convert(this.endDate))
    }

  }

  public onClickSave() {
    if (!this.isClientActive) {
      this.isDisabledButton = true;
      if (this.userName === undefined || this.userName === '' || this.password === undefined || this.password === '') {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }
      // console.log(this.userName);
      // console.log(this.password)

      this.boService.sendRequestToBackend(
        RequestGroup.LoginUser,
        RequestType.ManageUserLogin,
        {
          DATA: {
            userId: -1,
            userType: 1,
            loginStatus: 1,
            userName: this.userName,
            password: this.password,
            isFirstTime: 2,
          }
        }
        ).then(data  => {
        this.isDisabledButton = false;
          // console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
            this.isClientActive = true;
            this.disableClose = true;

            // console.log(data.DAT);
            this.userId = data.DAT.dto.userId;
          } else {
            this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      })

    } else {
      // console.log('working')
      this.isDisabledButton = true;
      if (this.validate() ) {
        let entitleMents = [];
        let branchId;
        let companyId;
        let jsonBody = {
          DATA: {
            id: this.popupData.isEdit ? this.clientId : -1,
            title: this.titleModel,
            firstName: this.firstNameModel,
            lastName: this.lastnameModel,
            branchId: this.branchModel,
            companyId: this.companyModel,
            userId: this.userId,
            entitlements: entitleMents ,
            agreementStartDate: this.convert(this.startDate),
            agreementEndDate: this.convert(this.endDate),
            clientCode: this.code,
            sector: this.sector,
            company: this.company,
            branch: this.branch,

          }
        }
        // console.log(jsonBody)
        this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageClient,jsonBody).then( data => {
          // console.log(data)
          this.isDisabledButton = false;
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              });

              this.disableClose = false;
              // this.userId = data.DAT.dto.userId;
              this.dialogRef.close(true);
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
      }

    }
  }


  lastnameModel: any;
  firstNameModel: any;
  isViewOnly: any;
  sector: any;
  code: any;

  closeDialog() {
    if (!this.disableClose) {
      this.dialogRef.close();
    }
  }

  public onChangeCompany(value: any) {
    this.company = this.companines.filter(c => c.id === value)[0].name;
    if (value === undefined) return;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
  }

  private convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  private validate():boolean {
    // console.log(this.titleModel);
    // console.log('lastname', this.lastnameModel)
    // console.log('firsname', this.firstNameModel)
    // console.log('emiss', this.emissionSrcModel)
    // console.log('comp', this.companyModel)
    // console.log(this.branchModel)
    if (this.titleModel === undefined || this.titleModel === '') {
      // console.log(this.titleModel);
      // console.log('title')
      return false;
    }

    if (this.firstNameModel === undefined || this.firstNameModel === '') {
      // console.log('firsname', this.firstNameModel)
      return false;
    } if (this.lastnameModel === undefined || this.lastnameModel === '') {
      // console.log('lastname', this.lastnameModel)
      return false;
    }
    if (this.companyModel === undefined || this.companyModel === 0) {
      // console.log('comp', this.companyModel)
      return false;
    }
    if (this.branchModel === undefined || this.branchModel === 0) {
      // console.log('brn',this.branchModel)
      // console.log(this.branchModel)
      return false;
    }
    // if (this.startDate === undefined || this.startDate === '') {
    //   console.log('start', this.startDate)
    //   return false;
    // }
    // if (this.endDate === undefined || this.endDate === '') {
    //   console.log('end', this.endDate)
    //   return false;
    // }
    if (this.userId === undefined ||  this.userId === 0) {
      // console.log('user id ljslfjsfjsf');
      return false;
    }
    if (this.code === undefined || this.code === '') {
      return false;
    }
    if (this.popupData.isEdit && (this.clientId === undefined || this.clientId === 0)) {

      return false;
    }

    return true;
  }

  onChangeBranch($event: MatSelectChange) {
    this.branch = this.branches.filter(b => b.id === $event.value)[0].name;
  }
}
