import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {AdminHomeComponent} from "./admin-home/admin-home.component";
import {AdminCompaninesComponent} from "./admin-companines/admin-companines.component";
import {ComAdminsComponent} from "./com-admins/com-admins.component";
import {AdminClientsComponent} from "./admin-clients/admin-clients.component";
import {AdminReportsComponent} from "./admin-reports/admin-reports.component";
import {ClientAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {ClientFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {ClientRefriComponent} from "./emission/client-refri/client-refri.component";
import {ClientGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {ClientElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {ClientWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {ClientMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {ClientEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {ClientTransportComponent} from "./emission/client-transport/client-transport.component";
import {ClientWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {ClientTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {ClientSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {ClinetEmployeeComponent} from "./clinet-employee/clinet-employee.component";

import {ViewComadminComponent} from "./com-admins/view-comadmin/view-comadmin.component";


import {GhgProjectComponent} from "./ghg-project/ghg-project.component";
import {UserAccessControlComponent} from "./user-access-control/user-access-control.component";
import {UsersAdminsComponent} from "./users-admins/users-admins.component";
import {DataEntryUserComponent} from "./data-entry-user/data-entry-user.component";
import {EmissionHistoryComponent} from './emission-history/emission-history.component';
import {SeaAirFreightComponent} from "./emission/sea-air-freight/sea-air-freight.component";
import {ManageMenuComponent} from "./manage-menu/manage-menu.component";
import {BiogasComponent} from "./emission/biogas/biogas.component";
import {LpgasComponent} from "./emission/lpgas/lpgas.component";
import {BgasComponent} from "./emission/bgas/bgas.component";




import {AdminAshTransportationComponent} from "./emission/admin-ash-transportation/admin-ash-transportation.component";
import {AdminForkliftsComponent} from "./emission/admin-forklifts/admin-forklifts.component";
import {AdminFurnaceOilComponent} from "./emission/admin-furnace-oil/admin-furnace-oil.component";
import {AdminLorryTransComponent} from "./emission/admin-lorry-trans/admin-lorry-trans.component";
import {AdminOilgasTransComponent} from "./emission/admin-oilgas-trans/admin-oilgas-trans.component";
import {AdminRawMaterialTransComponent} from "./emission/admin-raw-material-trans/admin-raw-material-trans.component";
import {AdminSawDustTransComponent} from "./emission/admin-saw-dust-trans/admin-saw-dust-trans.component";
import {AdminSludgeTransComponent} from "./emission/admin-sludge-trans/admin-sludge-trans.component";
import {AdminStaffTransComponent} from "./emission/admin-staff-trans/admin-staff-trans.component";
import {EmissionCategoriesComponent} from "./emission-categories/emission-categories.component";
import {AdminVehicleOthersComponent} from "./emission/admin-vehicle-others/admin-vehicle-others.component";
import {AdminPaidManagerVehicleComponent} from "./emission/admin-paid-manager-vehicle/admin-paid-manager-vehicle.component";
import {AdminPaperWasteComponent} from "./emission/admin-paper-waste/admin-paper-waste.component";
import {PubTransFactorsComponent} from "./pub-trans-factors/pub-trans-factors.component";
import {EmissionFactorsComponent} from "./emission-factors/emission-factors.component";
import {WasteDisFactorsComponent} from "./waste-dis-factors/waste-dis-factors.component";

import {AdminProjectSummaryComponent} from "./admin-project-summary/admin-project-summary.component";

const routes: Routes =
  [
    {
      path: 'home',
      component: AdminHomeComponent
    },
    {
      path: 'project',
      component: GhgProjectComponent,
    },
    {
      path: 'companies',
      component: AdminCompaninesComponent,
    },
    {
      path: 'access',
      component: UserAccessControlComponent,
    },
    {
      path: 'com_admins',
      component: ComAdminsComponent,
    },
    {
      path: 'com_admins/view',
      component: ViewComadminComponent,
    },
    {
      path: 'admin_clients',
      component: AdminClientsComponent,
    },
    {
      path: 'reports',
      component: AdminReportsComponent,
    },
    {
      path: 'summary_data_inputs',
      component: ClientSummaryDataInputsComponent,
    },
    {
      path: 'air_travel',
      component: ClientAirTravelComponent,
    },
    {
      path: 'fire_ext',
      component: ClientFireExtComponent,
    },
    {
      path: 'refri',
      component: ClientRefriComponent
    },
    {
      path: 'generators',
      component: ClientGeneratorsComponent,
    },
    {
      path: 'electricity',
      component: ClientElectricityComponent
    },
    {
      path: 'waste_disposal',
      component: ClientWasteDisposalComponent,
    },
    {
      path: 'municipal_water',
      component: ClientMunicipalWaterComponent,
    },
    {
      path: 'emp_comm',
      component: ClientEmpCommuComponent,
    },
    {
      path: 'transport',
      component: ClientTransportComponent,
    },
    {
      path: 'waste_transport',
      component: ClientWasteTransportComponent,
    },
    {
      path: 'trans_loc_purch',
      component: ClientTransLocPurComponent,
    },
    {
      path: 'employees',
      component: ClinetEmployeeComponent,
    },
    {
      path: 'factors',
      component: EmissionFactorsComponent,
    },
    {
      path: 'waste_factors',
      component: WasteDisFactorsComponent,
    },
    {
      path: 'pub_factors',
      component: PubTransFactorsComponent,
    },
    {
      path: 'admins',
      component: UsersAdminsComponent,
    },
    {
      path: 'data_entry_users',
      component: DataEntryUserComponent,
    }, {
    path: 'emission_history',
    component: EmissionHistoryComponent,
  },
    {
      path: 'freight_transport',
      component: SeaAirFreightComponent,
    },
    {
      path: 'manage_menus',
      component: ManageMenuComponent,
    },
    {
      path: 'biomass',
      component: BiogasComponent,
    },
    {
      path: 'lpgas',
      component: LpgasComponent
    },
    {
      path: 'bgas',
      component: BgasComponent
    },
    {
      path: 'ash_transport',
      component: AdminAshTransportationComponent,

    },
    {
      path: 'forklifts',
      component: AdminForkliftsComponent,

    }
    ,
    {
      path: 'furnace-oil',
      component: AdminFurnaceOilComponent,
    }
    , {
    path: 'lorry-transport',
    component: AdminLorryTransComponent,

  },
    {
      path: 'paid-manger-vehicle',
      component: AdminPaidManagerVehicleComponent,

    }
    , {
    path: 'paper-waste',
    component: AdminPaperWasteComponent,

  }

    , {
    path: 'oilgas-transport',
    component: AdminOilgasTransComponent,

  }
    , {
    path: 'raw-material-transport',
    component: AdminRawMaterialTransComponent,

  },
    {
      path: 'saw-dust-transport',
      component: AdminSawDustTransComponent,

    },
    {
      path: 'sludge-transport',
      component: AdminSludgeTransComponent,

    },
    {
      path: 'staff-transport',
      component: AdminStaffTransComponent,

    },
    {
      path: 'emission_categories',
      component: EmissionCategoriesComponent,
    },
    {
      path: 'vehicle_others',
      component: AdminVehicleOthersComponent,
    },
    {
      path: 'project_summary',
      component: AdminProjectSummaryComponent,
    }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {

}
