import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCompaninesComponent } from './admin-companines.component';

describe('AdminCompaninesComponent', () => {
  let component: AdminCompaninesComponent;
  let fixture: ComponentFixture<AdminCompaninesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCompaninesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCompaninesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
