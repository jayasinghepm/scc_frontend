import { Component, OnInit } from '@angular/core';
import { BackendService } from "../../../@core/rest/bo_service";
import { MassterDataService } from "../../../@core/service/masster-data.service";
import { NbToastrService, NbWindowService, NbGlobalPhysicalPosition } from "@nebular/theme";
import { AddCompanyComponent } from "./add-company/add-company.component";
import { RequestGroup } from "../../../@core/enums/request-group-enum";
import { RequestType } from "../../../@core/enums/request-type-enums";
import {MatDialog} from "@angular/material";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";

@Component({
  selector: 'app-admin-companines',
  templateUrl: './admin-companines.component.html',
  styleUrls: ['./admin-companines.component.scss']
})
export class AdminCompaninesComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;


  private filterModel = {
    name: {value: '', type: 4, col: 3},
    sector: {value: '', type: 4, col: 3},
    code: {value: '', type: 4, col: 3},
    id: {value: 0, type: 1, col: 1},
  }

  private sortModel = {
    name: {dir: ''},
    sector: {dir: ''},
    code: {dir: ''},
    id: {dir: ''},
  }



  settings = {
    mode: 'internal',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      name: {
        title: 'Name',
        editable: true,
        addable: true,
        type: 'string',
        filter: true,
        sort: true,
      },
      //Todo: have to change
      sector: {
        title: 'Sector',
        type: 'string',
        filter: true,
        sort: true,
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: ['Finance', 'Telecommunication', 'Apparel', 'Hospitality', 'Plantation',
              'Transport', 'Food and Beverage', 'Manufacturing', 'Other'
              ],
            }
          }
        }

      },
      code: {
        title: 'Code',
        type: 'string',
        filter: true,
        sort: true,

      },


    }
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))


  source: LocalDataSource = new LocalDataSource();


  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private dialog: MatDialog,
  ) {
    this.loadData();
  }

  ngOnInit() {

  }


  onUpdate(event: any) {
    // console.log(event);
    // update
    if (event.data !== undefined) {
      if (JSON.stringify(event.data) === JSON.stringify(event.newData)) {
        // ignore no change
        this.toastSerivce.show('', 'No change in data', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      } else {
        if (this.validateEntry(event.newData, true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Institution,
            RequestType.ManageCompany,
            this.fromTable(event.newData, true)
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto!== undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
              event.confirm.resolve(event.newData);
              this.loadData();
              this.masterData.loadCompanies();
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }
    }
    // new
    else {
      if (this.validateEntry(event.newData, false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Institution,
            RequestType.ManageCompany,
          this.fromTable(event.newData, false)
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            event.confirm.resolve(event.newData);
            this.loadData();
          } else {
            this.toastSerivce.show('', 'Error-Already existing code.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Institution,
          RequestType.ManageCompany,
          {
            DATA: {
              id: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.pg_current = 0;

              this.source.getElements().then(companies => {
                console.log('---------------------------------------------');
                console.log(companies.filter(com => com.id == $event.data.id)[0]);
                console.log('----------------------------------------------');
              })

                this.loadFiltered(0);
            }
          }
        });
      }
    });
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.name === undefined || data.name === '' ){
      return false;
    }

    if (data.sector === undefined || data.sector === '' ){
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    if (data.code === undefined || data.code === '' ){
      return false;
    }
    return true;
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      {
        PAGE_NUMBER: this.pg_current,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.list.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);

        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  // todo : validation

  private fromListRequest(dto: any): any {

    let id;
    let name;
    let sector;
    let code;

    id = dto.id;
    name = dto.name;
    sector = dto.sector;
    code = dto.code;

    return {
      id,
      name,
      sector,
      code,
    };



  }

  private fromTable(data: any, onEdit: boolean): any {
    let id;
    let name;
    let sector;
    let code;

    return {
      // todo:
      DATA: {
        id: onEdit ? data.id : -1,
        name: data.name,
        sector: data.sector,
        code: data.code,
      }

    };
  }

  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {

      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {

      return this.loadSorted(this.pg_current)
    }

    this.loadData();
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadData();
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    switch($event.query.column.id) {
      case 'name':  {
        this.filterModel.name.value = $event.query.query;
        break;
      }  case 'id':  {
        this.filterModel.id.value = $event.query.query;
        break;
      }  case 'sector':  {
        this.filterModel.sector.value = $event.query.query;
        break;
      } case 'code':  {
        this.filterModel.code.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  = {
      name: {dir: ''},
      sector: {dir: ''},
      code: {dir: ''},
      id: {dir: ''},
    }
    // console.log($event)
    switch($event.id) {
      case 'name':  {
        this.sortModel.name.dir = $event.direction;
        break;
      }  case 'sector':  {
        this.sortModel.sector.dir = $event.direction;
        break;
      }  case 'code':  {
        this.sortModel.code.dir = $event.direction;
        break;
      } case 'id':  {
        this.sortModel.id.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {

      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListCompany,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {

        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            // if (this.pg_current != 0 &&  (list.length === 0 || list.length <= 20)) {
            //   this.pg_current = 0;
            //   this.loadFiltered(this.pg_current);
            //
            // }
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {

      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListCompany,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListCompany,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListCompany,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }


}
