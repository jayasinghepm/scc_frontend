import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComAdminsComponent } from './com-admins.component';

describe('ComAdminsComponent', () => {
  let component: ComAdminsComponent;
  let fixture: ComponentFixture<ComAdminsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComAdminsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComAdminsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
