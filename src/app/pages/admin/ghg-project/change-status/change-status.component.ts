import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {BackendService} from "../../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {ViewCell} from "../../../../ng2-smart-table/src/lib/components/cell/cell-view-mode/view-cell";

@Component({
  selector: 'app-change-status',
  templateUrl: './change-status.component.html',
  styleUrls: ['./change-status.component.scss']
})
export class ChangeStatusComponent implements ViewCell, OnInit {
  private status:number = 3;
  public statusString = '';

  @Input() value: number; // This will be the value passed from the pathID column (see next code block)
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  renderValue: string;
  public statusList = [
  ]

  constructor(
    private boService: BackendService,
    private toastSerivce: NbToastrService,
    ) {

  }


  private init():void {
    // let status =  1;
    switch(this.status) {
      case 3: {
        this.statusList  = [];
        this.statusList.push(...[
          {value: 4, status: 'Data Entry'},
        ]);
        break;
      }
      case 4: {
        this.statusList = [];
        this.statusList.push(...[
          {value: 5, status: 'Completeness Check'}
        ])
        break;
      }
      case 5: {
        this.statusList = [];
        this.statusList.push(...[
          // {value: 6, status: 'Draft Report'}
        ])
        break;
      }case 7: {
        this.statusList = [];
        this.statusList.push(...[
          {value: 8, status: 'Closed'}
        ])
        break;
      }
    }
  }

  public statusName(): string {
    switch(this.status) {
      case 1: {
        this.statusString = 'New';
        return 'New';
        break;
      }
      case 2: {
        this.statusString = 'Signed';
        return 'Signed';
        break;
      }
      case 3: {
        this.statusString = 'Approved';
        return 'Approved';
        break;
      }
      case 4: {
        this.statusString = 'Data Entry'
        return 'Data Entry';
        break;
      }
      case 5: {
        this.statusString = 'Completeness Check'
        return 'Completeness Check'
        break;
      }
      case 6: {
        this.statusString = 'Draft Report';
        return 'Draft Report';
        break;
      }
      case 7: {
        return 'Final Report'
        break;
      }
      case 8: {
        this.statusString = 'Closed';
        return 'Closed'
        break;
      }
    }
  }


  public onClickChangeStatus(value: number) {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ManageProject,
      {DATA: {
          name: this.rowData.name,
          id:  this.rowData.id,
          status: value,
          companyId: this.rowData.idCompany,
          statusStr: this.statusString,
      }}
    ).then(data => {
      console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.DTO != undefined) {
          console.log(data.DAT.DTO)
          this.status = data.DAT.DTO.status;
          this.save.emit()
          console.log(this.status)
          this.init();
          this.renderValue = this.statusName();
          this.toastSerivce.show('', 'Changed project status successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })

        }
      }
    });
  }

  ngOnInit(): void {
    this.status = this.value;
    console.log(this.value);
    console.log(this.rowData);
    this.init();
    this.renderValue =this.statusName();
  }
}
