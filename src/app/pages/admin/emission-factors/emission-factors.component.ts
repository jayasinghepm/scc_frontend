import {Component, OnInit} from '@angular/core';
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {BackendService} from "../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-emission-factors',
  templateUrl: './emission-factors.component.html',
  styleUrls: ['./emission-factors.component.scss']
})
export class EmissionFactorsComponent implements OnInit {

  currentYear: number = new Date().getFullYear();
  py :number = this.currentYear-1;
  ppy :number = this.currentYear-2;
  pppy :number = this.currentYear-3;

  public emissionFactors = {
    values: {
      den_petrol: 0,
      den_diesel : 0,
      net_caloric_petrol: 0,
      net_caloric_diesel: 0,
      ef_co2_s_diesel: 0,
      ef_ch4_s_diesel: 0,
      ef_n2o_s_diesel: 0,
      ef_co2_s_petrol: 0,
      ef_ch4_s_petrol: 0,
      ef_n20_s_petrol: 0,
      gwp_co2: 0,
      gwp_ch4: 0,
      gwp_n2o: 0,
      gwp_r22 : 0,
      gwp_r407c_w_perc_ch2f2:0,
      gwp_r407c_w_perc_cf3chf2: 0,
      gwp_r407c_w_perc_cf3ch2f: 0,
      gwp_r407c_w_coff_cf3chf2: 0,
      gwp_r407c_w_coff_ch2f2:0,
      gwp_r407c_w_coff_cf3ch2f: 0,
      gwp_r410a_w_per_ch2f2: 0,
      gwp_r410a_w_per_chf2cf3: 0,
      gwp_r410a_w_coff_ch2f2: 0,
      gwp_r410a_w_coff_chf2cf3: 0,
      ef_co2_m_diesel: 0,
      ef_ch4_m_diesel: 0,
      ef_n20_m_diesel: 0,
      ef_co2_m_gasoline: 0,
      ef_ch4_m_gasoline: 0,
      ef_n20_m_gasoline: 0,
      ef_co2_o_diesel: 0,
      ef_ch4_o_diesel: 0,
      ef_n2o_o_diesel: 0,
      ef_co2_o_gasoline: 0,
      ef_ch4_o_gasoline: 0,
      ef_n20_o_gasoline: 0,
      grid_ef: 0,
      t_d_loss_perc: 0,
      cf_mw: 0,
      avg_pig_feed_rate: 0,

      // price_diesel_liter: 0,//fin lad FH   38
      // price_petrol_liter: 0,//fin lp95 FH  39
      // price_diesel_liter_cal: 0,// cal LAD_FH 40
      // price_petrol_liter_cal: 0,//cal lp95 FH 41

      // price_diesel_liter_new: 0,//fin LSD_FH 74
      // price_petrol_liter_new: 0,//fin LP92_FH 75
      // price_diesel_liter_cal_new: 0,//cal LSD FH
      // price_petrol_liter_cal_new: 0,//cal lp92 FH

      price_lad_fh: 0,//fin lad FH   38
      price_lp95_fh: 0,//fin lp95 FH  39
      price_lad_fh_cal: 0,// cal LAD_FH 40
      price_lp95_fh_cal: 0,//cal lp95 FH 41

      price_lsd_fh: 0,//fin LSD_FH 74
      price_lp92_fh: 0,//fin LP92_FH 75
      price_lsd_fh_cal: 0,//cal LSD FH 76
      price_lp92_fh_cal: 0,//cal lp92 FH 77

     
      price_lp95_sh: 0,//fin lp95 SH  78
      price_lp92_sh: 0,//fin LP92_SH 79
      price_lad_sh: 0,//fin lad SH   80
      price_lsd_sh: 0,//fin LSD_SH 81

     
      price_lp95_sh_cal: 0,//cal lp95 SH 82
      price_lp92_sh_cal: 0,//cal lp92 SH 83
      price_lad_sh_cal: 0,// cal LAD_SH 84
      price_lsd_sh_cal: 0,//cal LSD SH 85
   
      //pyear
      price_lp95_p: 0,//fin lp95 SH  78
      price_lp92_p: 0,//fin LP92_SH 79
      price_lad_p: 0,//fin lad SH   80
      price_lsd_p: 0,//fin LSD_SH 81

     
      price_lp95_p_cal: 0,//cal lp95 SH 82
      price_lp92_p_cal: 0,//cal lp92 SH 83
      price_lad_p_cal: 0,// cal LAD_SH 84
      price_lsd_p_cal: 0,//cal LSD SH 85

     //ppyear
      price_lp95_pp: 0,//fin lp95 SH  78
      price_lp92_pp: 0,//fin LP92_SH 79
      price_lad_pp: 0,//fin lad SH   80
      price_lsd_pp: 0,//fin LSD_SH 81

     
      price_lp95_pp_cal: 0,//cal lp95 SH 82
      price_lp92_pp_cal: 0,//cal lp92 SH 83
      price_lad_pp_cal: 0,// cal LAD_SH 84
      price_lsd_pp_cal: 0,//cal LSD SH 85
   
   
      

      em_intensity_finance: 0,
      em_intensity_tel: 0,
      em_intensity_apparel: 0,
      em_intensity_hospitality: 0,
      em_intensity_plant: 0,
      em_intensity_trans: 0,
      em_intensity_food: 0,
      em_intensity_manuf: 0,
      em_intensity_other: 0,
      cf_nautic_to_km : 0,
      ef_co2_air_freight_range1 : 0,
      ef_co2_air_freight_range2 :0,
      ef_co2_air_freight_range3 : 0,
      ef_co2_sea_freight : 0,
      ncv_biomass : 0,
      ef_co2_biomass: 0,
      ef_ch4_biomass: 0,
      ef_n20_biomass : 0,
      ncv_lpgas : 0,
      ef_co2_lpgas: 0,
      ef_ch4_lpgas: 0,
      ef_n20_lpgas : 0,
      ef_co2_paperwaste: 0,

    },
    refs: {
      den_petrol: '',
      den_diesel : '',
      net_caloric_petrol: '',
      net_caloric_diesel: '',
      ef_co2_s_diesel: '',
      ef_ch4_s_diesel: '',
      ef_n2o_s_diesel: '',
      ef_co2_s_petrol: '',
      ef_ch4_s_petrol: '',
      ef_n20_s_petrol: '',
      gwp_co2: '',
      gwp_ch4: '',
      gwp_n2o: '',
      gwp_r22 : '',
      gwp_r407c_w_perc_ch2f2:'',
      gwp_r407c_w_perc_cf3chf2: '',
      gwp_r407c_w_perc_cf3ch2f: '',
      gwp_r407c_w_coff_cf3chf2: '',
      gwp_r407c_w_coff_ch2f2:'',
      gwp_r407c_w_coff_cf3ch2f: '',
      gwp_r410a_w_per_ch2f2: '',
      gwp_r410a_w_per_chf2cf3: '',
      gwp_r410a_w_coff_ch2f2: '',
      gwp_r410a_w_coff_chf2cf3: '',
      ef_co2_m_diesel: '',
      ef_ch4_m_diesel: '',
      ef_n20_m_diesel: '',
      ef_co2_m_gasoline: '',
      ef_ch4_m_gasoline: '',
      ef_n20_m_gasoline: '',
      ef_co2_o_diesel: '',
      ef_ch4_o_diesel: '',
      ef_n2o_o_diesel: '',
      ef_co2_o_gasoline: '',
      ef_ch4_o_gasoline: '',
      ef_n20_o_gasoline: '',
      grid_ef: '',
      t_d_loss_perc: '',
      cf_mw: '',
      avg_pig_feed_rate: '',

      // price_diesel_liter: '',
      // price_petrol_liter: '',
      // price_diesel_liter_cal: '',
      // price_petrol_liter_cal: '',

      // price_diesel_liter_new: '',
      // price_petrol_liter_new: '',
      // price_diesel_liter_cal_new: '',
      // price_petrol_liter_cal_new: '',

      price_lad_fh: '',//fin lad FH   38
      price_lp95_fh: '',//fin lp95 FH  39
      price_lad_fh_cal: '',// cal LAD_FH 40
      price_lp95_fh_cal: '',//cal lp95 FH 41

      price_lsd_fh: '',//fin LSD_FH 74
      price_lp92_fh: '',//fin LP92_FH 75
      price_lsd_fh_cal: '',//cal LSD FH 76
      price_lp92_fh_cal: '',//cal lp92 FH 77

      price_lp95_sh: '',//fin lp95 SH  78
      price_lp92_sh: '',//fin LP92_SH 79
      price_lad_sh: '',//fin lad SH   80
      price_lsd_sh: '',//fin LSD_SH 81

     
      price_lp95_sh_cal: '',//cal lp95 SH 82
      price_lp92_sh_cal: '',//cal lp92 SH 83
      price_lad_sh_cal: '',// cal LAD_SH 84
      price_lsd_sh_cal: '',//cal LSD SH 85

        //pyear
        price_lp95_p: '',//fin lp95 SH  86
        price_lp92_p: '',//fin LP92_SH 87
        price_lad_p: '',//fin lad SH   88
        price_lsd_p: '',//fin LSD_SH 89
  
       
        price_lp95_p_cal: '',//cal lp95 SH 90
        price_lp92_p_cal: '',//cal lp92 SH 91
        price_lad_p_cal: '',// cal LAD_SH 92
        price_lsd_p_cal: '',//cal LSD SH 93

        //ppyear
        price_lp95_pp: '',//fin lp95 SH  94
        price_lp92_pp: '',//fin LP92_SH 95
        price_lad_pp: '',//fin lad SH   96
        price_lsd_pp: '',//fin LSD_SH 97
  
       
        price_lp95_pp_cal: '',//cal lp95 SH 98
        price_lp92_pp_cal: '',//cal lp92 SH 99
        price_lad_pp_cal: '',// cal LAD_SH 100
        price_lsd_pp_cal: '',//cal LSD SH 101

      em_intensity_finance: '',
      em_intensity_tel: '',
      em_intensity_apparel: '',
      em_intensity_hospitality: '',
      em_intensity_plant: '',
      em_intensity_trans: '',
      em_intensity_food: '',
      em_intensity_manuf: '',
      em_intensity_other: '',
      cf_nautic_to_km : '',
      ef_co2_air_freight_range1 : '',
      ef_co2_air_freight_range2 :'',
      ef_co2_air_freight_range3 : '',
      ef_co2_sea_freight : '',
      ncv_biomass : '',
      ef_co2_biomass: '',
      ef_ch4_biomass: '',
      ef_n20_biomass : '',
      ncv_lpgas : '',
      ef_co2_lpgas: '',
      ef_ch4_lpgas: '',
      ef_n20_lpgas : '',
      ef_co2_paperwaste: '',

    }
  }


  constructor(private boService: BackendService, private toastSerivce: NbToastrService) {
    this.loadData()
  }

  ngOnInit() {

  }

  public onUpdate(id: number) {
    let jsonBody = {
      id: undefined,
      value: undefined,
      reference: undefined,
    }
    switch(id) {
      case 1: {
        jsonBody.value = this.emissionFactors.values.den_diesel;
        jsonBody.id = id ;
        jsonBody.reference = this.emissionFactors.refs.den_diesel;

        break;
      }
      case 2: {
        jsonBody.value = this.emissionFactors.values.den_petrol;
        jsonBody.id = id;
        jsonBody.reference =  this.emissionFactors.refs.den_petrol;
        break;
      }
      case 3: {
        jsonBody.value = this.emissionFactors.values.net_caloric_diesel ;
        jsonBody.id = id ;
        jsonBody.reference = this.emissionFactors.refs.net_caloric_diesel;
        break;
      }
      case 4: {
        jsonBody.value = this.emissionFactors.values.net_caloric_petrol;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.net_caloric_petrol;
        break;
      }
      case 5: {
        jsonBody.value = this.emissionFactors.values.ef_co2_s_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_s_diesel;
        break;
      }
      case 6: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_s_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_s_diesel;
        break;
      }
      case 7: {
        jsonBody.value = this.emissionFactors.values.ef_n2o_s_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n2o_s_diesel ;
        break;
      }
      case 8: {
        jsonBody.value = this.emissionFactors.values.gwp_co2;
        jsonBody.id = id;
        jsonBody.reference =this.emissionFactors.refs.gwp_co2 ;
        break;
      }
      case 9: {
        jsonBody.value =   this.emissionFactors.values.gwp_ch4;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_ch4;
        break;
      }
      case 10: {
        jsonBody.value = this.emissionFactors.values.gwp_n2o;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_n2o;
        break;
      }
      case 11: {
        jsonBody.value =this.emissionFactors.values.gwp_r22 ;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r22;
        break;
      }
      case 12: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_perc_ch2f2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_perc_ch2f2;
        break;
      }
      case 13: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_perc_cf3chf2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_perc_cf3chf2 ;
        break;
      }
      case 14: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_perc_cf3ch2f;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_perc_cf3ch2f;

        break;
      }
      case 15: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_coff_cf3chf2;
        jsonBody.id =id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_coff_cf3chf2;

        break;
      }
      case 16: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_coff_ch2f2 ;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_coff_ch2f2;
        break;
      }
      case 17: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_coff_cf3ch2f;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_coff_cf3ch2f;
        break;
      }
      case 18: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_per_ch2f2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_per_ch2f2;
        break;
      }
      case 19: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_per_chf2cf3;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_per_chf2cf3;
        break;
      }
      case 20: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_coff_ch2f2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_coff_ch2f2;
        break;
      }
      case 21: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_coff_chf2cf3;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_coff_chf2cf3;
        break;
      }
      case 22: {
        jsonBody.value = this.emissionFactors.values.ef_co2_m_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_m_gasoline;
        break;
      }
      case 23: {
        jsonBody.value = this.emissionFactors.values.ef_co2_m_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_m_diesel ;
        break;
      }
      case 24: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_m_gasoline;
        jsonBody.id =id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_m_gasoline;
        break;
      }
      case 25: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_m_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_m_diesel;
        break;
      }
      case 26: {
        jsonBody.value = this.emissionFactors.values.ef_n20_m_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_m_gasoline;
        break;
      }
      case 27: {
        jsonBody.value =this.emissionFactors.values.ef_n20_m_diesel ;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_m_diesel ;
        break;
      }
      case 28: {
        jsonBody.value = this.emissionFactors.values.ef_co2_o_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_o_gasoline;
        break;
      }
      case 29: {
        jsonBody.value = this.emissionFactors.values.ef_co2_o_diesel
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_o_diesel
        break;
      }
      case 30: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_o_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_o_gasoline;
        break;
      }
      case 31: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_o_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_o_diesel;
        break;
      }
      case 32: {
        jsonBody.value = this.emissionFactors.values.ef_n20_o_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_o_gasoline;
        break;
      }
      case 33: {
        jsonBody.value = this.emissionFactors.values.ef_n2o_o_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n2o_o_diesel;
        break;
      }
      case 34: {
        jsonBody.value = this.emissionFactors.values.grid_ef;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.grid_ef;
        break;
      }
      case 35: {
        jsonBody.value = this.emissionFactors.values.t_d_loss_perc;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.t_d_loss_perc;
        break;
      }
      case 36: {
        jsonBody.value = this.emissionFactors.values.cf_mw;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.cf_mw;

        break;
      }
      case 37: {
        jsonBody.value = this.emissionFactors.values.avg_pig_feed_rate;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.avg_pig_feed_rate;
        break;
      }
      // case 38: {
      //   jsonBody.value = this.emissionFactors.values.price_diesel_liter;
      //   jsonBody.id = id;
      //   jsonBody.reference = this.emissionFactors.refs.price_diesel_liter;
      //   break;
      // }
      // case 39: {
      //   jsonBody.value = this.emissionFactors.values.price_petrol_liter;
      //   jsonBody.id = id;
      //   jsonBody.reference = this.emissionFactors.refs.price_petrol_liter;
      //   break;
      // }
      // case 40: {
      //   jsonBody.id = id;
      //   jsonBody.value = this.emissionFactors.values.price_diesel_liter_cal;
      //   jsonBody.reference = this.emissionFactors.refs.price_diesel_liter_cal;
      //   break;
      // }
      // case 41: {
      //   jsonBody.id = id;
      //   jsonBody.value = this.emissionFactors.values.price_petrol_liter_cal;
      //   jsonBody.reference = this.emissionFactors.refs.price_petrol_liter_cal;
      //   break;
      // }

      case 38: {
        jsonBody.value = this.emissionFactors.values.price_lad_fh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh;
        break;
      }
      case 39: {
        jsonBody.value = this.emissionFactors.values.price_lp95_fh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh;
        break;
      }
      case 40: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lad_fh_cal;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_cal;
        break;
      }
      case 41: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_cal;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_cal;
        break;
      }

      case 42: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_finance;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_finance;
        break;
      }
      case 43: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_tel;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_tel;
        break;
      }
      case 44: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_apparel;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_apparel;
        break;
      }
      case 45: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_hospitality;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_hospitality;
        break;
      }
      case 46: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_plant;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_plant;
        break;
      }
      case 47: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_trans;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_trans;
        break;
      }
      case 48: {
        jsonBody.id  = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_food;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_food;
        break;
      }
      case 49: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_manuf;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_manuf;
        break;
      }
      case 50: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_other;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_other;
        break;

      }
      case 51: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.cf_nautic_to_km;
        jsonBody.reference = this.emissionFactors.refs.cf_nautic_to_km;
        break;

      }
      case 52: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_air_freight_range1;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_air_freight_range1;
        break;

      }
      case 53: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_air_freight_range2;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_air_freight_range2;
        break;

      }
      case 54: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_air_freight_range3;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_air_freight_range3;
        break;

      }
      case 55: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_sea_freight;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_sea_freight;
        break;

      }
      case 56: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ncv_biomass;
        jsonBody.reference = this.emissionFactors.refs.ncv_biomass;
        break;

      }
      case 57: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_ch4_biomass;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_biomass;
        break;

      }
      case 58: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_biomass;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_biomass;
        break;

      }
      case 59: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_n20_biomass;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_biomass;
        break;

      }

      case 60: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ncv_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ncv_lpgas;
        break;

      }
      case 61: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_ch4_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_lpgas;
        break;

      }
      case 62: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_lpgas;
        break;

      }
      case 63: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_n20_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_lpgas;
        break;

      }
    
      //new added
      case 74: {
        jsonBody.value = this.emissionFactors.values.price_lsd_fh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh;
        break;
      }
      case 75: {
        jsonBody.value = this.emissionFactors.values.price_lp92_fh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh;
        break;
      }
      case 76: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_cal;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_cal;
        break;
      }
      case 77: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_cal;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_cal;
        break;
      }

    
    case 78: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh;
      break;
    }
    case 79: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh;
      break;
    }
    case 80: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh;
      break;
    }
    case 81: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh;
      break;
    }

    case 82: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_cal;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_cal;
      break;
    }
    case 83: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_cal;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_cal;
      break;
    }
    case 84: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_cal;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_cal;
      break;
    }
    case 85: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_cal;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_cal;
      break;
    }


    case 86: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p;
      break;
    }
    case 87: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p;
      break;
    }
    case 88: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p;
      break;
    }
    case 89: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p;
      break;
    }

    case 90: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_cal;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_cal;
      break;
    }
    case 91: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_cal;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_cal;
      break;
    }
    case 92: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_cal;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_cal;
      break;
    }
    case 93: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_cal;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_cal;
      break;
    }
      
        case 94: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp;
          break;
        }
        case 95: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp;
          break;
        }
        case 96: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp;
          break;
        }
        case 97: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp;
          break;
        }
    
        case 98: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_cal;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_cal;
          break;
        }
        case 99: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_cal;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_cal;
          break;
        }
        case 100: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_cal;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_cal;
          break;
        }
        case 101: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_cal;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_cal;
          break;
        }

    }

    if (jsonBody.value === '' || jsonBody.value === undefined) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      })
      return;
    }
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ManageEmissionFactors,
      { DATA: jsonBody}
      ).then(data => {

      if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
        this.toastSerivce.show('', 'Saved data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        // event.confirm.resolve(event.newData);
        // this.loadData();
      } else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
      }

    )
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ListEmFactors,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
           console.log(data.DAT.LIST)
          data.DAT.LIST.forEach(dto => {
            if (dto === undefined) return;
            switch(dto.id) {
              case 1: {
                this.emissionFactors.values.den_diesel = dto.value;
                this.emissionFactors.refs.den_diesel = dto.reference;
                break;
              }
              case 2: {
                this.emissionFactors.values.den_petrol = dto.value;
                this.emissionFactors.refs.den_petrol = dto.reference;
                break;
              }
              case 3: {
                this.emissionFactors.values.net_caloric_diesel = dto.value;
                this.emissionFactors.refs.net_caloric_diesel = dto.reference;
                break;
              }
              case 4: {
                this.emissionFactors.values.net_caloric_petrol = dto.value;
                this.emissionFactors.refs.net_caloric_petrol = dto.reference;
                break;
              }
              case 5: {
                this.emissionFactors.values.ef_co2_s_diesel = dto.value;
                this.emissionFactors.refs.ef_co2_s_diesel = dto.reference;
                break;
              }
              case 6: {
                this.emissionFactors.values.ef_ch4_s_diesel = dto.value;
                this.emissionFactors.refs.ef_ch4_s_diesel = dto.reference;
                break;
              }
              case 7: {
                this.emissionFactors.values.ef_n2o_s_diesel = dto.value;
                this.emissionFactors.refs.ef_n2o_s_diesel = dto.reference;
                break;
              }
              case 8: {
                this.emissionFactors.values.gwp_co2 = dto.value;
                this.emissionFactors.refs.gwp_co2 = dto.reference;
                break;
              }
              case 9: {
                this.emissionFactors.values.gwp_ch4 = dto.value;
                this.emissionFactors.refs.gwp_ch4  = dto.reference;
                break;
              }
              case 10: {
                this.emissionFactors.values.gwp_n2o = dto.value;
                this.emissionFactors.refs.gwp_n2o = dto.reference;
                break;
              }
              case 11: {
                this.emissionFactors.values.gwp_r22 = dto.value;
                this.emissionFactors.refs.gwp_r22 = dto.reference;
                break;
              }
              case 12: {
                this.emissionFactors.values.gwp_r407c_w_perc_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_perc_ch2f2 = dto.reference;
                break;
              }
              case 13: {
                this.emissionFactors.values.gwp_r407c_w_perc_cf3chf2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_perc_cf3chf2 = dto.reference;
                break;
              }
              case 14: {
                this.emissionFactors.values.gwp_r407c_w_perc_cf3ch2f = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_perc_cf3ch2f = dto.reference;
                break;
              }
              case 15: {
                this.emissionFactors.values.gwp_r407c_w_coff_cf3chf2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_coff_cf3chf2 = dto.reference;
                break;
              }
              case 16: {
                this.emissionFactors.values.gwp_r407c_w_coff_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_coff_ch2f2 = dto.reference;
                break;
              }
              case 17: {
                this.emissionFactors.values.gwp_r407c_w_coff_cf3ch2f = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_coff_cf3ch2f = dto.reference;
                break;
              }
              case 18: {
                this.emissionFactors.values.gwp_r410a_w_per_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_per_ch2f2 = dto.reference;
                break;
              }
              case 19: {
                this.emissionFactors.values.gwp_r410a_w_per_chf2cf3 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_per_chf2cf3 = dto.reference;
                break;
              }
              case 20: {
                this.emissionFactors.values.gwp_r410a_w_coff_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_coff_ch2f2 = dto.reference;
                break;
              }
              case 21: {
                this.emissionFactors.values.gwp_r410a_w_coff_chf2cf3 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_coff_chf2cf3 = dto.reference;
                break;
              }
              case 22: {
                this.emissionFactors.values.ef_co2_m_gasoline = dto.value;
                this.emissionFactors.refs.ef_co2_m_gasoline = dto.reference;
                break;
              }
              case 23: {
                this.emissionFactors.values.ef_co2_m_diesel = dto.value;
                this.emissionFactors.refs.ef_co2_m_diesel = dto.reference;
                break;
              }
              case 24: {
                this.emissionFactors.values.ef_ch4_m_gasoline = dto.value;
                this.emissionFactors.refs.ef_ch4_m_gasoline = dto.reference;
                break;
              }
              case 25: {
                this.emissionFactors.values.ef_ch4_m_diesel = dto.value;
                this.emissionFactors.refs.ef_ch4_m_diesel = dto.reference;
                break;
              }
              case 26: {
                this.emissionFactors.values.ef_n20_m_gasoline = dto.value;
                this.emissionFactors.refs.ef_n20_m_gasoline = dto.reference;
                break;
              }
              case 27: {
                this.emissionFactors.values.ef_n20_m_diesel = dto.value;
                this.emissionFactors.refs.ef_n20_m_diesel = dto.reference;
                break;
              }
              case 28: {
                this.emissionFactors.values.ef_co2_o_gasoline = dto.value;
                this.emissionFactors.refs.ef_co2_o_gasoline = dto.reference;
                break;
              }
              case 29: {
                this.emissionFactors.values.ef_co2_o_diesel = dto.value;
                this.emissionFactors.refs.ef_co2_o_diesel = dto.reference;
                break;
              }
              case 30: {
                this.emissionFactors.values.ef_ch4_o_gasoline = dto.value;
                this.emissionFactors.refs.ef_ch4_o_gasoline = dto.reference;
                break;
              }
              case 31: {
                this.emissionFactors.values.ef_ch4_o_diesel = dto.value;
                this.emissionFactors.refs.ef_ch4_o_diesel = dto.reference;
                break;
              }
              case 32: {
                this.emissionFactors.values.ef_n20_o_gasoline = dto.value;
                this.emissionFactors.refs.ef_n20_o_gasoline = dto.reference;
                break;
              }
              case 33: {
                this.emissionFactors.values.ef_n2o_o_diesel = dto.value;
                this.emissionFactors.refs.ef_n2o_o_diesel = dto.reference;
                break;
              }
              case 34: {
                this.emissionFactors.values.grid_ef = dto.value;
                this.emissionFactors.refs.grid_ef = dto.reference;
                break;
              }
              case 35: {
                this.emissionFactors.values.t_d_loss_perc = dto.value;
                this.emissionFactors.refs.t_d_loss_perc = dto.reference;
                break;
              }
              case 36: {
                this.emissionFactors.values.cf_mw = dto.value;
                this.emissionFactors.refs.cf_mw = dto.reference;
                break;
              }
              case 37: {
                this.emissionFactors.values.avg_pig_feed_rate = dto.value;
                this.emissionFactors.refs.avg_pig_feed_rate = dto.reference;
                break;
              }
              // case 38: {
              //   this.emissionFactors.values.price_diesel_liter = dto.value;
              //   this.emissionFactors.refs.price_diesel_liter = dto.reference;
              //   break;
              // }
              // case 39: {
              //   this.emissionFactors.values.price_petrol_liter = dto.value;
              //   this.emissionFactors.refs.price_petrol_liter  = dto.reference;
              //   break;
              // }
              // case 40: {
              //   this.emissionFactors.values.price_diesel_liter_cal = dto.value;
              //   this.emissionFactors.refs.price_diesel_liter_cal = dto.reference;
              //   break;
              // }
              // case 41: {
              //   this.emissionFactors.values.price_petrol_liter_cal = dto.value;
              //   this.emissionFactors.refs.price_petrol_liter_cal = dto.reference;
              //   break;
              // }

            
      
              case 38: {
                this.emissionFactors.values.price_lad_fh = dto.value;
                this.emissionFactors.refs.price_lad_fh = dto.reference;
                break;
              }
              case 39: {
                this.emissionFactors.values.price_lp95_fh = dto.value;
                this.emissionFactors.refs.price_lp95_fh  = dto.reference;
                break;
              }
              case 40: {
                this.emissionFactors.values.price_lad_fh_cal = dto.value;
                this.emissionFactors.refs.price_lad_fh_cal = dto.reference;
                break;
              }
              case 41: {
                this.emissionFactors.values.price_lp95_fh_cal = dto.value;
                this.emissionFactors.refs.price_lp95_fh_cal = dto.reference;
                break;
              }

              
              case 42: {
                this.emissionFactors.values.em_intensity_finance = dto.value;
                this.emissionFactors.refs.em_intensity_finance  = dto.reference;
                break;
              }
              case 43: {
                this.emissionFactors.values.em_intensity_tel = dto.value;
                this.emissionFactors.refs.em_intensity_tel = dto.reference;
                break;
              }
              case 44: {
                this.emissionFactors.values.em_intensity_apparel = dto.value;
                this.emissionFactors.refs.em_intensity_apparel = dto.reference;
                break;
              }
              case 45: {
                this.emissionFactors.values.em_intensity_hospitality = dto.value;
                this.emissionFactors.refs.em_intensity_hospitality = dto.reference;
                break;
              }
              case 46:{
                this.emissionFactors.values.em_intensity_plant = dto.value;
                this.emissionFactors.refs.em_intensity_plant = dto.reference;
                break;
              }
              case 47: {
                this.emissionFactors.values.em_intensity_trans = dto.value;
                this.emissionFactors.refs.em_intensity_trans= dto.reference;
                break;
              }
              case 48: {
                this.emissionFactors.values.em_intensity_food = dto.value;
                this.emissionFactors.refs.em_intensity_food = dto.reference;
                break;
              }
              case 49: {
                this.emissionFactors.values.em_intensity_manuf = dto.value;
                this.emissionFactors.refs.em_intensity_manuf = dto.reference;
                break;
              }
              case 50: {
                this.emissionFactors.values.em_intensity_other = dto.value;
                this.emissionFactors.refs.em_intensity_other = dto.reference;
                break;
              }
              case 51: {
                this.emissionFactors.values.cf_nautic_to_km = dto.value;
                this.emissionFactors.refs.cf_nautic_to_km = dto.reference;
                break;
              }
              case 52: {
                this.emissionFactors.values.ef_co2_air_freight_range1 = dto.value;
                this.emissionFactors.refs.ef_co2_air_freight_range1 = dto.reference;
                break;
              }
              case 53: {
                this.emissionFactors.values.ef_co2_air_freight_range2 = dto.value;
                this.emissionFactors.refs.ef_co2_air_freight_range2 = dto.reference;
                break;
              }
              case 54: {
                this.emissionFactors.values.ef_co2_air_freight_range3 = dto.value;
                this.emissionFactors.refs.ef_co2_air_freight_range3 = dto.reference;
                break;
              }
              case 55: {
                this.emissionFactors.values.ef_co2_sea_freight = dto.value;
                this.emissionFactors.refs.ef_co2_sea_freight = dto.reference;
                break;
              }
              case 56: {
                this.emissionFactors.values.ncv_biomass = dto.value;
                this.emissionFactors.refs.ncv_biomass = dto.reference;
                break;
              }

              case 57: {
                this.emissionFactors.values.ef_ch4_biomass = dto.value;
                this.emissionFactors.refs.ef_ch4_biomass = dto.reference;
                break;
              }
              case 58: {
                this.emissionFactors.values.ef_co2_biomass = dto.value;
                this.emissionFactors.refs.ef_co2_biomass = dto.reference;
                break;
              }
              case 59: {
                this.emissionFactors.values.ef_n20_biomass = dto.value;
                this.emissionFactors.refs.ef_n20_biomass = dto.reference;
                break;
              }
              case 60: {
                this.emissionFactors.values.ncv_lpgas = dto.value;
                this.emissionFactors.refs.ncv_lpgas = dto.reference;
                break;
              }
              case 61: {
                this.emissionFactors.values.ef_ch4_lpgas = dto.value;
                this.emissionFactors.refs.ef_ch4_lpgas = dto.reference;
                break;
              }
              case 62: {
                this.emissionFactors.values.ef_co2_lpgas = dto.value;
                this.emissionFactors.refs.ef_co2_lpgas = dto.reference;
                break;
              }
              case 63: {
                this.emissionFactors.values.ef_n20_lpgas = dto.value;
                this.emissionFactors.refs.ef_n20_lpgas = dto.reference;
                break;
              }
              //new added
          
              case 74: {
                this.emissionFactors.values.price_lsd_fh = dto.value;
                this.emissionFactors.refs.price_lp92_fh = dto.reference;
                break;
              }
              case 75: {
                this.emissionFactors.values.price_lp92_fh = dto.value;
                this.emissionFactors.refs.price_lp92_fh  = dto.reference;
                break;
              }
              case 76: {
                this.emissionFactors.values.price_lsd_fh_cal = dto.value;
                this.emissionFactors.refs.price_lsd_fh_cal = dto.reference;
                break;
              }
              case 77: {
                this.emissionFactors.values.price_lp92_fh_cal = dto.value;
                this.emissionFactors.refs.price_lp92_fh_cal = dto.reference;
                break;
              }
              
          
              case 78: {
                this.emissionFactors.values.price_lp95_sh = dto.value;
                this.emissionFactors.refs.price_lp95_sh = dto.reference;
                break;
              }
              case 79: {
                this.emissionFactors.values.price_lp92_sh = dto.value;
                this.emissionFactors.refs.price_lp92_sh  = dto.reference;
                break;
              }
              case 80: {
                this.emissionFactors.values.price_lad_sh = dto.value;
                this.emissionFactors.refs.price_lad_sh = dto.reference;
                break;
              }
              case 81: {
                this.emissionFactors.values.price_lsd_sh = dto.value;
                this.emissionFactors.refs.price_lsd_sh = dto.reference;
                break;
              }

              case 82: {
                this.emissionFactors.values.price_lp95_sh_cal = dto.value;
                this.emissionFactors.refs.price_lp95_sh_cal = dto.reference;
                break;
              }
              case 83: {
                this.emissionFactors.values.price_lp92_sh_cal = dto.value;
                this.emissionFactors.refs.price_lp92_sh_cal  = dto.reference;
                break;
              }
              case 84: {
                this.emissionFactors.values.price_lad_sh_cal = dto.value;
                this.emissionFactors.refs.price_lad_sh_cal = dto.reference;
                break;
              }
              case 85: {
                this.emissionFactors.values.price_lsd_sh_cal = dto.value;
                this.emissionFactors.refs.price_lsd_sh_cal = dto.reference;
                break;
              }

              case 86: {
                this.emissionFactors.values.price_lp95_p = dto.value;
                this.emissionFactors.refs.price_lp95_p = dto.reference;
                break;
              }
              case 87: {
                this.emissionFactors.values.price_lp92_p = dto.value;
                this.emissionFactors.refs.price_lp92_p  = dto.reference;
                break;
              }
              case 88: {
                this.emissionFactors.values.price_lad_p = dto.value;
                this.emissionFactors.refs.price_lad_p = dto.reference;
                break;
              }
              case 89: {
                this.emissionFactors.values.price_lsd_p = dto.value;
                this.emissionFactors.refs.price_lsd_p = dto.reference;
                break;
              }
              case 90: {
                this.emissionFactors.values.price_lp95_p_cal = dto.value;
                this.emissionFactors.refs.price_lp95_p_cal = dto.reference;
                break;
              }
              case 91: {
                this.emissionFactors.values.price_lp92_p_cal = dto.value;
                this.emissionFactors.refs.price_lp92_p_cal  = dto.reference;
                break;
              }
              case 92: {
                this.emissionFactors.values.price_lad_p_cal = dto.value;
                this.emissionFactors.refs.price_lad_p_cal = dto.reference;
                break;
              }
              case 93: {
                this.emissionFactors.values.price_lsd_p_cal = dto.value;
                this.emissionFactors.refs.price_lsd_p_cal = dto.reference;
                break;
              }
    

        case 94: {
          this.emissionFactors.values.price_lp95_pp = dto.value;
          this.emissionFactors.refs.price_lp95_pp = dto.reference;
          break;
        }
        case 95: {
          this.emissionFactors.values.price_lp92_pp = dto.value;
          this.emissionFactors.refs.price_lp92_pp  = dto.reference;
          break;
        }
        case 96: {
          this.emissionFactors.values.price_lad_pp = dto.value;
          this.emissionFactors.refs.price_lad_pp = dto.reference;
          break;
        }
        case 97: {
          this.emissionFactors.values.price_lsd_pp = dto.value;
          this.emissionFactors.refs.price_lsd_pp = dto.reference;
          break;
        }
        case 98: {
          this.emissionFactors.values.price_lp95_pp_cal = dto.value;
          this.emissionFactors.refs.price_lp95_pp_cal = dto.reference;
          break;
        }
        case 99: {
          this.emissionFactors.values.price_lp92_pp_cal = dto.value;
          this.emissionFactors.refs.price_lp92_pp_cal  = dto.reference;
          break;
        }
        case 100: {
          this.emissionFactors.values.price_lad_pp_cal = dto.value;
          this.emissionFactors.refs.price_lad_pp_cal = dto.reference;
          break;
        }
        case 101: {
          this.emissionFactors.values.price_lsd_pp_cal = dto.value;
          this.emissionFactors.refs.price_lsd_pp_cal = dto.reference;
          break;
        }

            }
          })
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })


  }


}
