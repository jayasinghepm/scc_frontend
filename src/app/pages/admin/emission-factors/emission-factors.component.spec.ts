import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmissionFactorsComponent } from './emission-factors.component';

describe('EmissionFactorsComponent', () => {
  let component: EmissionFactorsComponent;
  let fixture: ComponentFixture<EmissionFactorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmissionFactorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmissionFactorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
