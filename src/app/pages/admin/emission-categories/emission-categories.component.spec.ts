import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmissionCategoriesComponent } from './emission-categories.component';

describe('EmissionCategoriesComponent', () => {
  let component: EmissionCategoriesComponent;
  let fixture: ComponentFixture<EmissionCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmissionCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmissionCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
