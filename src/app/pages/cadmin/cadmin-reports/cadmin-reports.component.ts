import { Component, OnInit } from '@angular/core';
import {UserState} from "../../../@core/auth/UserState";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {BackendService} from "../../../@core/rest/bo_service";

@Component({
  selector: 'app-cadmin-reports',
  templateUrl: './cadmin-reports.component.html',
  styleUrls: ['./cadmin-reports.component.scss']
})
export class CadminReportsComponent implements OnInit {

  public reports  = [];
  constructor(private boService: BackendService) {
    this.loadReports();
  }

  ngOnInit() {
  }

  onClickDownloadReport(url: string) {
    window.open(url, "_blank");
  }

  public loadReports() {
    const reportfilter = {
      FILTER_MODEL: {
        comId: { value: UserState.getInstance().companyId, type: 1, col: 1}
      },
      PAGE_NUMBER: -1,
    }

    this.boService.sendRequestToBackend(
      RequestGroup.Report,
      RequestType.ListReport,
      reportfilter
    ).then(data => {
      if (data !== undefined && data.DAT !== undefined && data.DAT.LIST !== undefined) {
        data.DAT.LIST.forEach(r => {
          let name;
          if (r.pdf !== undefined){
            let list = r.pdf.split('/');
            if (list.length !== 0) {
              name = list.filter(p => p.includes('pdf'))[0]
            }
          }
          if (name !== undefined) {
            this.reports.push({
              name:name,
              type: r.reportType === 1? 'Draft': (r.reportType ===2 ? 'Final': ''),
              url: r.pdf,
            })
          }

        })
      }
    })
  }

}
