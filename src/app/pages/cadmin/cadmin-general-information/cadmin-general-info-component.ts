import {Component, OnInit} from '@angular/core';
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {BackendService} from "../../../@core/rest/bo_service";
import {DomSanitizer} from "@angular/platform-browser";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {UserState} from "../../../@core/auth/UserState";
import {CadminStatus} from "../../../@core/enums/CadminStatus";
import {validateEmail, validateMobile, validateTelephone, validateYear} from "../../../@core/utils/validator";
import {Router} from "@angular/router";
import {MatRadioChange} from "@angular/material";


class ImageSnippet {
  constructor(public src: string, public file: File) {
  }
}

@Component({
  selector: 'app-cadmin-users',
  templateUrl: './cadmin-general-info-component.html',
  styleUrls: ['./cadmin-general-info-component.scss']
})
export class CadminGeneralInfoComponent implements OnInit {
  public selectedFile: ImageSnippet;
  public logoAvailable = false;

  private invalidFieldMessage = undefined;

  public cadminStatus = CadminStatus.NotSubmitted;
  public cadminStatusMsg = '';

  public currentYear :any  = '';
  public emissionYearStr = '';
  public baseYearStr = '';
  public isFinacialYear = true;

  public completed = {
    1: false,
    2: false,
    3: false,
    4: false,
  }

  public titlesOptions: string [];
  public titleConfig = {
    search: false,

  };
  public districtConfig = {
    search: false,
  }

  public districtsOptions: string[] =[]


  public cadminInfo= {
    firstName: '',
    lastName: '',
    title: '',
    telephoneNo: '',
    mobileNo: '',
    position: '',
    email: ''
  }

  public companyBasicInfo = {
    name: '',
    sector: '',
    code: '',
    noOfEmployees: 0,
    noOfBranches: 0,
    addr1: '',
    addr2: '',
    district: '',

  }
  public comAdvInfo = {
    logo: '',
    baseYear: '',
    yearEmission: [],
    targetGHG: 0,
  }
  public comFinInfo = {
    fyMonthStart: '',
    fyMonthEnd: '',
    fyCurrentStart: 0,
    fyCurrentEnd: 0,
    annumRevFY: 0,
    isFinancialYear: 0,
    targetGHG: 0,
  }



  public emission = {
    fyyearStart: '',
    fyyearEnd: '',
    emission: '',
  }

  public emissionYears : any[] = [];
  logoImage = ''
  public monthOptions: string[] = [];
  public projectStatus: number;
  public projectId: number;



  constructor(private masterData : MassterDataService,
              private boService: BackendService,
              private _sanitizer: DomSanitizer,
              private  toastSerivce:  NbToastrService,
              private router :Router
  ) {


    if (!UserState.getInstance().existActiveProject) {
      console.log("exist project " + !UserState.getInstance().existActiveProject)
      this.router.navigate(['/pages/com_admin/reports']);
    }else {
      this.masterData.getTitles().subscribe(d =>{ this.titlesOptions =d;});
      this.masterData.getDistricts().subscribe(d => { this.districtsOptions = d});
      this.masterData.getMonths().subscribe(d =>  this.monthOptions =d);
      // Todo: intiate data with existing
      // this.image = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + popupData.QuestionEntity.image);
      // todo:

      this.logoAvailable = true;
      this.loadCadminInfo();
      this.loadCompanyInfo();
      this.initProject();
      this.loadPreviousYearReports();
    }

  }

  private exp;

  ngOnInit() {
  }

  public disableCompanyBasicInfo(): Boolean {
    if (this.cadminStatus == CadminStatus.NotSubmitted && !this.completed["1"]) {
      return true;
    }
    return false;
  }

  public disableCompanyOtherInfo(): Boolean {
    if (this.cadminStatus == CadminStatus.NotSubmitted && !this.completed["4"]) {
      return true;
    }
    return false;
  }

  public disableCompanyAdvanceInfo(): Boolean {
    if (this.cadminStatus == CadminStatus.NotSubmitted && !this.completed["2"]) {
      return true;
    }
    return false;
  }


  public onClickAddEmission($event: MouseEvent) {
    if(!this.validateEmissionYearStr()) {
      this.toastSerivce.show( 'Uncheck Financial Year Checkbox or Follow correct year format','', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if (this.emission.fyyearEnd !== undefined && this.emission.fyyearEnd !== '' && this.emission.fyyearStart !== undefined && this.emission.fyyearStart !== '' && this.emission.emission !== undefined && this.emission.emission !== '') {
      console.log("jlsjfsfs")
      if (this.emissionYears === undefined) this.emissionYears = []
      this.emissionYears.push(JSON.parse(JSON.stringify(this.emission)));
      this.emission.emission = '';
      this.emission.fyyearStart = '';
      this.emission.fyyearEnd = ''
    }
  }
  public  onClickDeleteEmissionYears(item: any) {
    this.emissionYears = this.emissionYears.filter(v => { return !(v.fyyearStart === item.fyyearStart && v.fyyearEnd === item.fyyearEnd && v.emission === item.emission)})
  }

  public onFileChanged(image: any): void {
    const file: File = image.target.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedFile =new ImageSnippet(event.target.result, file);
      if (this.selectedFile) {
        this.comAdvInfo.logo = this.selectedFile.src;
        console.log(this.comAdvInfo.logo)
        if (this.comAdvInfo.logo !== '' && this.comAdvInfo.logo !== undefined) {
          this.logoImage = this.comAdvInfo.logo;
          this.logoAvailable = true;
          // this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.companyBasicInfo.logo)
        }
      }
    });

    reader.readAsDataURL(file);
  }


  public onMonthChange($event: any, num: number) {
    if ($event.value === '' || $event.value === undefined) return;
    if (num === 1) {
      this.comFinInfo.fyMonthStart = $event.value;
      // this.masterData.getMonthId($event.value).subscribe(d => this.comFinInfo.fyMonthStart =d);
    } else if(num === 2) {
      this.comFinInfo.fyMonthEnd = $event.value;
      // this.masterData.getMonthId($event.value).subscribe(d => this.comFinInfo.fyMonthEnd =d);
    }
  }


  private loadCadminInfo(): void {
    this.boService.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListComAdmin,
      {
        FILTER_MODEL: {
          id: { value: UserState.getInstance().clientAdminId , type: 1, col: 1}
        },
        PAGE_NUM: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.cadminInfo.firstName = val.firstName;
              this.cadminInfo.lastName = val.lastName;
              this.cadminInfo.telephoneNo = val.telephoneNo;
              this.cadminInfo.mobileNo = val.mobileNo;
              this.cadminInfo.title = val.title;
              this.cadminInfo.email = val.email;
              this.cadminInfo.position = val.position;
              this.cadminStatus = val.status;
              this.cadminStatusMsg = val.statusMessage ;
              if (val.firstName !== undefined && val.firstName !== ''
                && val.lastName !== undefined && val.lastName !== ''
                && val.telephoneNo !== undefined && val.telephoneNo !== ''
                && val.mobileNo !== undefined && val.mobileNo !== ''
                && val.title !== undefined && val.title !== ''
                && val.email !== undefined && val.email !== ''
                && val.position !== undefined && val.position !== ''
              ) {
                this.completed["1"] = true;
              }
              console.log(this.completed)
            }
          });
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  public loadCompanyInfo(): void {
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      {
        isLogoRequired: 1,
        FILTER_MODEL: {
          id: { value: UserState.getInstance().companyId , type: 1, col: 1}
        },
        PAGE_NUM: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.companyBasicInfo.name = val.name;
              this.companyBasicInfo.code = val.code ;
              this.companyBasicInfo.sector = val.sector ;
              this.companyBasicInfo.addr1 = val.addr1 ;
              this.companyBasicInfo.addr2 = val.addr2 ;
              this.companyBasicInfo.district = val.district;
              this.companyBasicInfo.noOfBranches = val.noOfBranches ;
              this.companyBasicInfo.noOfEmployees = val.noOfEmployees;

              if (val.name !== undefined && val.name !== ''
                && val.code !== undefined && val.code !== ''
                && val.sector !== undefined && val.sector !== ''
                && val.addr1 !== undefined && val.addr1 !== ''
                && val.addr2 !== undefined && val.addr2 !== ''
                && val.district !== undefined  && val.district !== ''
                && val.noOfBranches !== 0 && val.noOfBranches !== ''
                && val.noOfEmployees !== 0 &&  val.noOfEmployees !== ''
              ) {
                this.completed["2"] = true;
              }


              this.comAdvInfo.baseYear = val.baseYear;
              this.baseYearStr = this.comAdvInfo.baseYear;
              this.comAdvInfo.logo = val.logo;
              this.logoImage = val.logo;

              if (val.baseYear !== undefined && val.baseYear !== ''
                && val.logo !== undefined && val.logo !== ''
                // && val.yearEmission.list !== undefined && val.yearEmission.list.length !== 0
              ) {
                this.completed["3"] = true;
              }

              if (this.logoImage !== undefined && this.logoImage !== '') {
                this.logoAvailable = true;
              }
              this.comAdvInfo.yearEmission = val.yearEmission.list;
              this.emissionYears = this.comAdvInfo.yearEmission;

              this.comFinInfo.annumRevFY = val.annumRevFY ;
              let monStart;
              let monEnd;
              this.masterData.getMonthName(val.fyMonthEnd ).subscribe(d => { monEnd  = d; console.log(d)} )
              this.comFinInfo.fyMonthEnd = monEnd ;
              this.masterData.getMonthName(val.fyMonthStart).subscribe(d => { monStart = d; console.log(d)})
              this.comFinInfo.fyMonthStart = monStart ;
              this.comFinInfo.fyCurrentEnd = val.fyCurrentEnd ;
              this.comFinInfo.fyCurrentStart = val.fyCurrentStart ;
              this.isFinacialYear = val.isFinancialYear === 2 ? true : false;
              this.comFinInfo.isFinancialYear = this.isFinacialYear ? 2 : 1;
              if (this.isFinacialYear) {
                this.currentYear = `${this.comFinInfo.fyCurrentStart}/${this.comFinInfo.fyCurrentEnd % 100}`
              } else {
                this.currentYear = `${this.comFinInfo.fyCurrentStart}`;
              }
              if (val.annumRevFY !== undefined && val.annumRevFY !== 0
                && val.fyMonthEnd !== undefined && val.fyMonthEnd <= 11
                && val.fyMonthStart !== undefined && val.fyMonthStart <=11
                && val.fyCurrentStart !== undefined && val.fyCurrentStart > 0
                && val.fyCurrentEnd !== undefined && val.fyCurrentEnd > 0

              ) {
                this.completed["4"] = true;
              }
              console.log(this.comFinInfo)

              console.log(this.completed)
              this.loadEmissionIntensity();
            }
          });

        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
   
  }

  private defEmIntensity = 0;
  public loadEmissionIntensity() {
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ListEmFactors,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          console.log(data.DAT.LIST)
          data.DAT.LIST.forEach(dto => {
            if (dto === undefined) return;
            if (this.companyBasicInfo.sector === 'Other'){
                if (dto.id === 50) {
                  this.defEmIntensity = dto.value;
                }
            }else if (this.companyBasicInfo.sector === 'Finance') {
              if (dto.id === 42) {
                this.defEmIntensity = dto.value;
              }
            }else if (this.companyBasicInfo.sector == 'Telecommunication') {
              if (dto.id === 43) {
                this.defEmIntensity = dto.value;
              }
            }else if (this.companyBasicInfo.sector == 'Apparel') {
              if (dto.id === 44) {
                this.defEmIntensity = dto.value;
              }
            }else if (this.companyBasicInfo.sector == 'Hospitality') {
              if (dto.id === 45) {
                this.defEmIntensity = dto.value;
              }
            }else if (this.companyBasicInfo.sector === 'Plantation') {
              if (dto.id === 46) {
                this.defEmIntensity = dto.value;
              }
            }else if (this.companyBasicInfo.sector === 'Transport') {
              if (dto.id === 47) {
                this.defEmIntensity = dto.value;
              }
            }
            else if (this.companyBasicInfo.sector === 'Food and Beverage') {
              if (dto.id === 48) {
                this.defEmIntensity = dto.value;
              }
            }else if (this.companyBasicInfo.sector === 'Manufacturing') {
              if (dto.id === 49) {
                this.defEmIntensity = dto.value;
              }
            }
          })
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })

  }

  public updateCompanyBasicInfo() :void {
    if (!this.validateComBasicInfo()) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    let jsonData ={
      DATA: {
        id: UserState.getInstance().companyId,
        name: this.companyBasicInfo.name,
        sector: this.companyBasicInfo.sector,
        code: this.companyBasicInfo.code,
        noOfEmployees: this.companyBasicInfo.noOfEmployees,
        noOfBranches: this.companyBasicInfo.noOfBranches,
        addr1: this.companyBasicInfo.addr1,
        addr2: this.companyBasicInfo.addr2,
        district: this.companyBasicInfo.district,
      }
    }

      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ManageCompany,
        jsonData
      ).then( data => {
        if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
          this.toastSerivce.show('', 'Saved data successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          this.completed["2"] = true;
          console.log(this.completed)
          if (this.completed["1"] && this.completed["4"] && this.completed["3"]) {
            console.log(this.completed)
            this.changeToPending()
          }
        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      })

  }

  public updateCompanyAdvInfo():void {
    this.comAdvInfo.yearEmission = this.emissionYears;
    if (!this.validateBaseyear()) {
      this.toastSerivce.show('Uncheck Financial Year Checkbox or Follow correct year format', '', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if (!this.validateComAdvInfo()) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }

    if (this.emissionYears === undefined || this.emissionYears.length ==0) {
      this.comAdvInfo.targetGHG =  this.defEmIntensity * this.comFinInfo.annumRevFY;
      this.comFinInfo.targetGHG = this.comAdvInfo.targetGHG;
    }else {
      let emIntensity =0;
      this.emissionYears.forEach(o => {
        emIntensity += o.emission;
      })
      emIntensity = (emIntensity/ this.emissionYears.length);
      this.comAdvInfo.targetGHG = emIntensity *  this.comFinInfo.annumRevFY;
      this.comFinInfo.targetGHG = this.comAdvInfo.targetGHG;
    }
    let jsonData ={
      DATA: {
        id: UserState.getInstance().companyId ,
        yearEmission: {list: this.comAdvInfo.yearEmission},
        baseYear: this.comAdvInfo.baseYear ,
        logo: this.comAdvInfo.logo,
        targetGHG: this.comAdvInfo.targetGHG,
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ManageCompany,
      jsonData
    ).then( data => {
      if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
        this.toastSerivce.show('', 'Saved data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        this.completed["3"] = true;
        console.log(this.completed)
        if (this.completed["1"] && this.completed["2"] && this.completed["4"]) {
          console.log(this.completed)
          this.changeToPending()
        }
      } else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }
  public updateCompanyFinInfo():void {
    if (!this.validateAndConvertCurrentYear()) {
      this.toastSerivce.show('Uncheck Financial Year Checkbox or Follow correct year format', '', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if (!this.validateComFinaInfo()) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return
    }
    let monStart;
    let monEnd;
    this.masterData.getMonthId(this.comFinInfo.fyMonthStart).subscribe(d => { monStart =d;});
    this.masterData.getMonthId(this.comFinInfo.fyMonthEnd).subscribe(d => { monEnd = d;})
    if (this.emissionYears == undefined || this.emissionYears.length ==0) {
      this.comAdvInfo.targetGHG =  this.defEmIntensity * this.comFinInfo.annumRevFY * 1000000;
      this.comFinInfo.targetGHG = this.comAdvInfo.targetGHG;
    }else {
      let emIntensity =0;
      this.emissionYears.forEach(o => {
        emIntensity += o.emission;
      })
      emIntensity = (emIntensity/ this.emissionYears.length);
      this.comAdvInfo.targetGHG = emIntensity *  this.comFinInfo.annumRevFY * 1000000;
      this.comFinInfo.targetGHG = this.comAdvInfo.targetGHG;
    }

    let jsonData ={
      DATA: {
        id: UserState.getInstance().companyId,
        annumRevFY: this.comFinInfo.annumRevFY,
        fyMonthStart:monStart ,
        fyMonthEnd: monEnd,
        fyCurrentStart: this.comFinInfo.fyCurrentStart,
        fyCurrentEnd: this.comFinInfo.fyCurrentEnd,
        isFinancialYear: this.isFinacialYear ? 2 : 1,
        targetGHG: this.comFinInfo.targetGHG,
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ManageCompany,
      jsonData
    ).then( data => {
      if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
        this.toastSerivce.show('', 'Saved data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        this.completed["4"] = true;
        console.log(this.completed)
        if (this.completed["1"] && this.completed["2"] && this.completed["3"]) {
          console.log(this.completed)
          this.changeToPending()
        }
      } else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  public updateCadminInfo(): void {
    if (!this.validateCadminInfo()) {
      if (this.invalidFieldMessage !== undefined) {
        this.toastSerivce.show('', this.invalidFieldMessage, {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    let jsonBody = {
      DATA: {
        id: UserState.getInstance().clientAdminId,
        title: this.cadminInfo.title,
        firstName: this.cadminInfo.firstName,
        lastName: this.cadminInfo.lastName,
        // userId: this.userId,
        email: this.cadminInfo.email,
        // companyId: companyId,
        telephoneNo: this.cadminInfo.telephoneNo,
        mobileNo: this.cadminInfo.mobileNo,
        position: this.cadminInfo.position,
        // status
        // entitlements: entitleMents ,
        // agreementStartDate: this.convert(this.startDate),
        // agreementEndDate: this.convert(this.endDate)
      }
    }

    this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
      console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.dto != undefined) {
          this.toastSerivce.show('', 'Saved data successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          this.completed["1"] = true;
          console.log(this.completed)
          if (this.completed["2"] && this.completed["3"] && this.completed["4"]) {
            // todo status to pending
            console.log(this.completed)
            this.changeToPending()
          }


        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  private validateComBasicInfo(): boolean {
    console.log(this.companyBasicInfo)
    if (this.companyBasicInfo.name === undefined || this.companyBasicInfo.name === '') {
      return false;
    }
    if (this.companyBasicInfo.sector === undefined || this.companyBasicInfo.sector === '') {
      return false;
    }
    if (this.companyBasicInfo.code === undefined || this.companyBasicInfo.code === '') {
      return false;
    }
    if (this.companyBasicInfo.addr1 === undefined || this.companyBasicInfo.addr1 === '') {
      return false;
    }
    if (this.companyBasicInfo.addr2 === undefined || this.companyBasicInfo.addr2 === '') {
      return false;
    }
    if (this.companyBasicInfo.district === undefined || this.companyBasicInfo.district === '' || (typeof(this.companyBasicInfo.district) === typeof([]) && this.companyBasicInfo.district.length === 0)) {
      console.log(this.companyBasicInfo.district)
      return false;
    }
    if (this.companyBasicInfo.noOfEmployees === undefined || this.companyBasicInfo.noOfEmployees <= 0) {
      return false;
    }
    if (this.companyBasicInfo.noOfBranches === undefined || this.companyBasicInfo.noOfBranches <= 0) {
      return false;
    }
    return true;
  }
  private validateComAdvInfo(): boolean  {
    if (this.comAdvInfo.logo === undefined || this.comAdvInfo.logo === '') {
      return false;
    }

    // if (this.comAdvInfo.baseYear === undefined || this.comAdvInfo.baseYear === '') {
    //   return false;
    // }
    // if (this.comAdvInfo.yearEmission === undefined || this.comAdvInfo.yearEmission.length == 0) {
    //   return false;
    // }
    return true;
  }
  private validateComFinaInfo(): boolean {
    this.comFinInfo.isFinancialYear = this.isFinacialYear ? 2 : 1;
    if (this.comFinInfo.fyMonthStart === undefined || this.comFinInfo.fyMonthStart === '') {
      return false;
    }
    if (this.comFinInfo. fyMonthEnd === undefined || this.comFinInfo. fyMonthEnd  === '') {
      return false;
    }
    // if (this.comFinInfo. fyCurrentStart === undefined || this.comFinInfo. fyCurrentStart <= 0 ) {
    //   return false;
    // }
    // if (this.comFinInfo.fyCurrentEnd === undefined || this.comFinInfo.fyCurrentEnd <= 0 ) {
    //   return false;
    // }
    if (this.comFinInfo.annumRevFY === undefined || this.comFinInfo.annumRevFY <= 0 ) {
      return false;
    }
    return true;

  }
  private validateCadminInfo(): boolean {
    this.invalidFieldMessage = undefined;
      if (this.cadminInfo.firstName === '' || this.cadminInfo.firstName == undefined) {
        return false;
      }
    if (this.cadminInfo.lastName === '' || this.cadminInfo.lastName == undefined) {
      return false;
    }
    if (this.cadminInfo.title === '' || this.cadminInfo.title == undefined) {
      return false;
    }
    if (this.cadminInfo.telephoneNo === '' || this.cadminInfo.telephoneNo == undefined) {
      return false;
    } else if (!validateTelephone(this.cadminInfo.telephoneNo)) {
      this.invalidFieldMessage = 'Enter Valid Telephone Number'
      return false;
    }
    if (this.cadminInfo.mobileNo === '' || this.cadminInfo.mobileNo == undefined) {
      return false;
    } else if (!validateMobile(this.cadminInfo.mobileNo)) {
      this.invalidFieldMessage = 'Enter Valid Mobile Number'
      return false;
    }
    if (this.cadminInfo.position === '' || this.cadminInfo.position == undefined) {
      return false;
    }
    if (this.cadminInfo.email === '' || this.cadminInfo.email == undefined) {
      return false;
    } else if (!validateEmail(this.cadminInfo.email)) {
      this.invalidFieldMessage = 'Enter Valid Email';
      return false;
    }
    return true;
  }

  private changeToPending(): void {
    if (this.projectStatus === 1) {
      this.toastSerivce.show('', 'Accept Agreement First', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if (this.projectStatus === -1) {
      this.toastSerivce.show('', 'Wait until Climate SI admin creates new Project', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if(this.projectStatus >= 3) {
      return;
    }
    let jsonBody = {
      DATA: {
        id: UserState.getInstance().clientAdminId,
        // title: this.cadminInfo.title,
        // firstName: this.cadminInfo.firstName,
        // lastName: this.cadminInfo.lastName,
        // // userId: this.userId,
        // email: this.cadminInfo.email,
        // // companyId: companyId,
        // telephoneNo: this.cadminInfo.telephoneNo,
        // mobileNo: this.cadminInfo.mobileNo,
        // position: this.cadminInfo.position,
        status: CadminStatus.Pending,
        // entitlements: entitleMents ,
        // agreementStartDate: this.convert(this.startDate),
        // agreementEndDate: this.convert(this.endDate)
      }
    }

    this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
      console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.dto != undefined) {
          this.toastSerivce.show('', 'Information was sent to ClimateSI.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          this.cadminStatus = CadminStatus.Pending;



        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  private validateAndConvertCurrentYear():boolean {
    this.comFinInfo.isFinancialYear  = this.isFinacialYear ? 2 : 1;
    if (this.isFinacialYear) {

        if (this.currentYear.indexOf('/') === -1) {
          return false;

        }
        let i = this.currentYear.indexOf('/')
        let y1 = this.currentYear.substring(0, i);
        let y2 = this.currentYear.substring(i+1, this.currentYear.length);
        y1= y1.trim();
        y2 = y2.trim();
        if (y1.length !== 4) {
          return false;
        }
        if (y2.length !== 2) {
          return false;
        }
        if (isNaN(+y1) || +y1 <= 0 || (+y1 -2000)<= 0 || this.digits_count(+y1) !== 4) {
          return false;
        }

        if (isNaN(+y2) || +y2 <= 0  || this.digits_count(+y2) !== 2) {
          return false;
        }
        this.comFinInfo.fyCurrentStart = +y1;
        this.comFinInfo.fyCurrentEnd = 2000 + (+y2);

      this.comFinInfo.fyMonthStart  = 'April'
      this.comFinInfo.fyMonthEnd  = 'March'

      console.log(this.comFinInfo)
        return true;

    } else {
      let year = +this.currentYear;

      if (isNaN(year)) {
        return false;
      }
      if (this.digits_count(year) == 4 && year > 0 && (year - 2000) > 0) {
        this.comFinInfo.fyMonthStart  = 'January'
        this.comFinInfo.fyMonthEnd  = 'December'
        this.comFinInfo.fyCurrentEnd = year;
        this.comFinInfo.fyCurrentStart = year;
        return true;
      }
    }
  }

  onchangeYear($event: MatRadioChange) {
    this.isFinacialYear = $event.value === 'true'? true : false;
  }


  private validateEmissionYearStr():boolean {
    if (this.isFinacialYear) {
      if (this.emissionYearStr.indexOf('/') === -1) {
        return false;

      }
      let i = this.emissionYearStr.indexOf('/')
      let y1 = this.emissionYearStr.substring(0, i);
      let y2 = this.emissionYearStr.substring(i+1, this.emissionYearStr.length);
      y1= y1.trim();
      y2 = y2.trim();
      if (y1.length !== 4) {
        return false;
      }
      if (y2.length !== 2) {
        return false;
      }
      if (isNaN(+y1) || +y1 <= 0 || (+y1 -2000)<= 0 || this.digits_count(+y1) !== 4) {
        return false;
      }

      if (isNaN(+y2) || +y2 <= 0  || this.digits_count(+y2) !== 2) {
        return false;
      }
      this.emission.fyyearStart = `${+y1}`;
      this.emission.fyyearEnd =  `${2000 + (+y2)}`;

      // this.comFinInfo.fyMonthStart  = 'April'
      // this.comFinInfo.fyMonthEnd  = 'March'
      return true;

    } else {
      let year = +this.emissionYearStr;

      if (isNaN(year)) {
        return false;
      }
      if (this.digits_count(year) == 4 && year > 0 && (year - 2000) > 0) {
        // this.comFinInfo.fyMonthStart  = 'January'
        // this.comFinInfo.fyMonthEnd  = 'December'
        this.emission.fyyearStart = `${year}`;
        this.emission.fyyearEnd =  `${year}`;
        return true;
      }
    }
  }

  private validateBaseyear(): boolean {
    if (this.isFinacialYear) {
      if (this.baseYearStr.indexOf('/') === -1) {
        return false;

      }
      let i = this.baseYearStr.indexOf('/')
      let y1 = this.baseYearStr.substring(0, i);
      let y2 = this.baseYearStr.substring(i+1, this.baseYearStr.length);
      y1= y1.trim();
      y2 = y2.trim();
      if (y1.length !== 4) {
        return false;
      }
      if (y2.length !== 2) {
        return false;
      }
      if (isNaN(+y1) || +y1 <= 0 || (+y1 -2000)<= 0 || this.digits_count(+y1) !== 4) {
        return false;
      }

      if (isNaN(+y2) || +y2 <= 0  || this.digits_count(+y2) !== 2) {
        return false;
      }
      this.comAdvInfo.baseYear = `${+y1}/${+y2}`;

      // this.comFinInfo.fyMonthStart  = 'April'
      // this.comFinInfo.fyMonthEnd  = 'March'
      return true;

    } else {
      let year = +this.baseYearStr;

      if (isNaN(year)) {
        return false;
      }
      if (this.digits_count(year) == 4 && year > 0 && (year - 2000) > 0) {
        // this.comFinInfo.fyMonthStart  = 'January'
        // this.comFinInfo.fyMonthEnd  = 'December'
       this.comAdvInfo.baseYear = `${year}`;
        return true;
      }
    }
  }

  private digits_count(n): number {
    let count = 0;
    if (n >= 1) ++count;

    while (n / 10 >= 1) {
      n /= 10;
      ++count;
    }

    return count;
  }

  initProject() {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          companyId: { value: UserState.getInstance().companyId , type: 1, col: 1},
          status: { value: 7, type: 2, col: 1}
        }
      }
    ).then(data => {
      console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {

          if (data.DAT.LIST.length > 0) {
            let list = data.DAT.LIST.filter(v => { return v.status !== 9;})
            this.projectStatus = list[0].status;
            this.projectId = list[0].id;
          } else {
            this.projectStatus = -1;
            this.projectId = -1;
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  onClickAgree() {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ManageProject,
      {DATA: { id: this.projectId, status: 2 }}
    ).then(data => {

      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.DTO != undefined) {
          console.log(data.DAT.DTO)
          this.projectStatus = data.DAT.DTO.status;
          this.toastSerivce.show('', 'Successfully Agreed.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
        }
      }

    });
  }

  public reports = [

  ]
  public yearOfReport = '';
  private pdfReportUploaed;
  onReportUploaded(pdf: any) {
    const file: File = pdf.target.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      const pdfsrc = new ImageSnippet(event.target.result, file);
      if (pdfsrc !== undefined) {
        this.pdfReportUploaed= pdfsrc.src;
        //todo check

        // console.log(this.jsonbodyUploadReport.pdf)
        // if (this.comAdvInfo.logo !== '' && this.comAdvInfo.logo !== undefined) {
        //   this.logoImage = this.comAdvInfo.logo;
        //   this.logoAvailable = true;
        //   // this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.companyBasicInfo.logo)
        // }
      }
    });

    reader.readAsDataURL(file);
  }

  private climatesiReports = [
  ]
  loadPreviousYearReports() {
    this.reports = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Report,
      RequestType.ListReport,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          comId: { value: UserState.getInstance().companyId , type: 1, col: 1},
          reportType: { value: 3, type: 1, col: 1}
        }
      }
    ).then(data => {
      console.log(data);
      if (data !== undefined && data.DAT !== undefined  && data.DAT.LIST !== undefined) {
        for (const dto of data.DAT.LIST) {
          console.log(dto)
          if (dto.projectId >0) {
            this.climatesiReports.push(dto.year)
            continue
          }
          this.reports.push({
            year: dto.year,
            projectId: dto.projectId,
            url: dto.pdf,
            comId: dto.comId,
          })
        }
      }
    })
  }

  uploadReport($event: MouseEvent) {
    //validate year and report
    if (this.yearOfReport === undefined || this.yearOfReport === '') {
      this.toastSerivce.show( 'Fill Year.','', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if(this.pdfReportUploaed === undefined || this.pdfReportUploaed === '') {
      this.toastSerivce.show( 'Upload a PDF report.','', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    //validate year
    if(!validateYear(this.yearOfReport, this.isFinacialYear)) {
      this.toastSerivce.show( 'Enter a valid year.','', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if(this.yearOfReport === this.currentYear) {
      this.toastSerivce.show( 'You can not submit report for ' + this.yearOfReport  + ".",'', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if (this.reports.map(obj => obj.year ).includes(this.yearOfReport)) {
      this.toastSerivce.show( 'Enter a valid year.','', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    if (this.climatesiReports.includes(this.yearOfReport)) {
      this.toastSerivce.show( 'You can not submit report for ' + this.yearOfReport  + ".",'', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }

    this.boService.sendRequestToBackend(
      RequestGroup.Report,
      RequestType.ManageReport,
      {
        DATA: {
          projectId: -1,
          comId: UserState.getInstance().companyId,
          pdf: this.pdfReportUploaed,
          reportType: 3, // external insitution report
          year: this.yearOfReport
        }
      }
    ).then(data => {
      console.log(data)
      if (data !== undefined && data.DAT !== undefined) {

        this.toastSerivce.show( 'Report Uploaded Successfully.','', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        this.loadPreviousYearReports()
        return;
        // {year: '2020', url: 'jslfjsf', projectId: 1, comId:1}
        // this.reports.push({
        //   year: data.DAT.year,
        //   url: data.DAT.pdf,
        //   projectId: data.DAT.projectId,
        //
        }

    })
    //backend request and update tables
  }

  deleteReport($event: MouseEvent, projectId: number, index: number) {
    this.boService.sendRequestToBackend(
      RequestGroup.Report,
      RequestType.ManageReport,
      {
        DATA: {
          projectId: projectId,
          isDeleted: 1
        }
      }
    ).then(data => {
      if (data !== undefined && data.DAT !== undefined) {

        this.toastSerivce.show( 'Report Deleted Successfully.','', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        this.loadPreviousYearReports()
        return;
        // {year: '2020', url: 'jslfjsf', projectId: 1, comId:1}
        // this.reports.push({
        //   year: data.DAT.year,
        //   url: data.DAT.pdf,
        //   projectId: data.DAT.projectId,
        //
      }

    })
  }
}
