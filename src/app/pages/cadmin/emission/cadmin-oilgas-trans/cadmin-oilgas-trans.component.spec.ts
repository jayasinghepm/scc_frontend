import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminOilgasTransComponent } from './cadmin-oilgas-trans.component';

describe('CadminOilgasTransComponent', () => {
  let component: CadminOilgasTransComponent;
  let fixture: ComponentFixture<CadminOilgasTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminOilgasTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminOilgasTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
