import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminOilgasComponent } from './update-cadmin-oilgas.component';

describe('UpdateCadminOilgasComponent', () => {
  let component: UpdateCadminOilgasComponent;
  let fixture: ComponentFixture<UpdateCadminOilgasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminOilgasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminOilgasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
