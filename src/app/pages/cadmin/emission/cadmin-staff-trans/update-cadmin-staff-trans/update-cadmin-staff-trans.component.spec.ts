import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminStaffTransComponent } from './update-cadmin-staff-trans.component';

describe('UpdateCadminStaffTransComponent', () => {
  let component: UpdateCadminStaffTransComponent;
  let fixture: ComponentFixture<UpdateCadminStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
