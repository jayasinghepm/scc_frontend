import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminStaffTransComponent } from './cadmin-staff-trans.component';

describe('CadminStaffTransComponent', () => {
  let component: CadminStaffTransComponent;
  let fixture: ComponentFixture<CadminStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
