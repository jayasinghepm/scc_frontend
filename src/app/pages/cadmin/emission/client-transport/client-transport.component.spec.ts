import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientTransportComponent } from './client-transport.component';

describe('ClientTransportComponent', () => {
  let component: ClientTransportComponent;
  let fixture: ComponentFixture<ClientTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
