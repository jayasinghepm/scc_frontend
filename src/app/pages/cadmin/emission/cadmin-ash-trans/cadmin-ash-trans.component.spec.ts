import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminAshTransComponent } from './cadmin-ash-trans.component';

describe('CadminAshTransComponent', () => {
  let component: CadminAshTransComponent;
  let fixture: ComponentFixture<CadminAshTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminAshTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminAshTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
