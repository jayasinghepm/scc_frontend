import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiomassComponent } from './biomass.component';

describe('BiomassComponent', () => {
  let component: BiomassComponent;
  let fixture: ComponentFixture<BiomassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiomassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiomassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
