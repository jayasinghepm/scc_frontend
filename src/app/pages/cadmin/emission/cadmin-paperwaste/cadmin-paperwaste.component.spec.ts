import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminPaperwasteComponent } from './cadmin-paperwaste.component';

describe('CadminPaperwasteComponent', () => {
  let component: CadminPaperwasteComponent;
  let fixture: ComponentFixture<CadminPaperwasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminPaperwasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminPaperwasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
