import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminSawDustTransComponent } from './update-cadmin-saw-dust-trans.component';

describe('UpdateCadminSawDustTransComponent', () => {
  let component: UpdateCadminSawDustTransComponent;
  let fixture: ComponentFixture<UpdateCadminSawDustTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminSawDustTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminSawDustTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
