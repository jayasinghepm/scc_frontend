import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminSawDustTransComponent } from './cadmin-saw-dust-trans.component';

describe('CadminSawDustTransComponent', () => {
  let component: CadminSawDustTransComponent;
  let fixture: ComponentFixture<CadminSawDustTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminSawDustTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminSawDustTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
