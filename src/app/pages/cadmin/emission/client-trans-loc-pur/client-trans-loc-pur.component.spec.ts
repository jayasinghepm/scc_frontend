import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientTransLocPurComponent } from './client-trans-loc-pur.component';

describe('ClientTransLocPurComponent', () => {
  let component: ClientTransLocPurComponent;
  let fixture: ComponentFixture<ClientTransLocPurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientTransLocPurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientTransLocPurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
