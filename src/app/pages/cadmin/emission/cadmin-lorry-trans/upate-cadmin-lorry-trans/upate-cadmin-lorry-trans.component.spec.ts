import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpateCadminLorryTransComponent } from './upate-cadmin-lorry-trans.component';

describe('UpateCadminLorryTransComponent', () => {
  let component: UpateCadminLorryTransComponent;
  let fixture: ComponentFixture<UpateCadminLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpateCadminLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpateCadminLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
