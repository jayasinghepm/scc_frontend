import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSeaAirFreightComponent } from './update-sea-air-freight.component';

describe('UpdateSeaAirFreightComponent', () => {
  let component: UpdateSeaAirFreightComponent;
  let fixture: ComponentFixture<UpdateSeaAirFreightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSeaAirFreightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSeaAirFreightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
