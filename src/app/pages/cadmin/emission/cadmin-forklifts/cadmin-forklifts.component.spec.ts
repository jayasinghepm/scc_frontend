import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminForkliftsComponent } from './cadmin-forklifts.component';

describe('CadminForkliftsComponent', () => {
  let component: CadminForkliftsComponent;
  let fixture: ComponentFixture<CadminForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
