import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-generators',
  templateUrl: './update-generators.component.html',
  styleUrls: ['./update-generators.component.scss']
})
export class CadminUpdateGeneratorsComponent implements OnInit {

  public isSaved = false;

  public jsonBody = {
    GENERATORS_ENTRY_ID: -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined,
    YEAR: undefined,
    MONTH: undefined,
    FUEL_CONSUMPTION: undefined,
    UNITS: undefined,
    FUEL_TYPE: undefined,
    NUM_GENERATORS: undefined,
    GENERATOR_NUMBER: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
  };


  public months = [];
  public companies = []
  public years = [];
  public branches = [];
  public fuels = [];
  public units = [];


  constructor(
    public dialogRef: MatDialogRef<CadminUpdateGeneratorsComponent>,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private boService: BackendService,

    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    this.init(this.popupData.data, this.popupData.isNew)
    console.log(this.popupData)
  }

  private init(data: any, isNew: boolean) {
    this.jsonBody.COMPANY_ID = UserState.getInstance().companyId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.COMPANY_ID)[0].name;
    })
    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      console.log(d);
      this.years = d;
    })
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d;
    //   console.log(d);
    // })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getFuelTypesGeneratorFull().subscribe(data => {
      this.fuels = data;
    })
    this.masterData.getUnitsGeneratorsFull().subscribe(data => {
      this.units = data;
    });

    this.branches.push({
      id: 0, name: 'Search Branch'
    })

    if (!isNew) {
      this.jsonBody.MONTH = data.idMon ;
      this.jsonBody.YEAR = data.year ;
      this.jsonBody.GENERATORS_ENTRY_ID = data.id ;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.GENERATOR_NUMBER = data.generatorNumber;
      this.jsonBody.UNITS = data.idUnits ;
      this.jsonBody.FUEL_TYPE = data.idFuel ;
      this.jsonBody.FUEL_CONSUMPTION  = data.amountOfFuel;
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        console.log(d);
        this.years = d;
      })

      this.branches.push({
        id: data.idBran, name: data.branch
      })
    }
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: this.jsonBody.COMPANY_ID, type: 1, col: 1},
      },
    }
    // if (this.selectedComId != 0) {
    //   queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    // }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry( onEdit: boolean): boolean {
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === ''  || this.jsonBody.BRANCH_ID === 0){
    //   return false;
    // }

    // if (this.jsonBody.numOfGenerators === undefined || this.jsonBody.numOfGenerators === '' ){
    //   return false;
    // }
    if (this.jsonBody.GENERATOR_NUMBER === undefined || this.jsonBody.GENERATOR_NUMBER === '' ){
      return false;
    }
    if (this.jsonBody.FUEL_TYPE === undefined || this.jsonBody.FUEL_TYPE === '' ){
      return false;
    }
    if (this.jsonBody.FUEL_CONSUMPTION === undefined || this.jsonBody.FUEL_CONSUMPTION === '' ){
      return false;
    }
    if (this.jsonBody.UNITS === undefined || this.jsonBody.UNITS === '' ){
      return false;
    }


    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.GENERATORS_ENTRY_ID === undefined || this.jsonBody.GENERATORS_ENTRY_ID <= 0)) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
        if (this.validateEntry( true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageGeneratorsEntry,
            { DATA: this.jsonBody}
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })

            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageGeneratorsEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

}
