import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminPaidManagerVehicleComponent } from './update-cadmin-paid-manager-vehicle.component';

describe('UpdateCadminPaidManagerVehicleComponent', () => {
  let component: UpdateCadminPaidManagerVehicleComponent;
  let fixture: ComponentFixture<UpdateCadminPaidManagerVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminPaidManagerVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminPaidManagerVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
