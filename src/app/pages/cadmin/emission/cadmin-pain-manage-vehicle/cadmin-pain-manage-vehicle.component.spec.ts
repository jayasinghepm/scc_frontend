import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminPainManageVehicleComponent } from './cadmin-pain-manage-vehicle.component';

describe('CadminPainManageVehicleComponent', () => {
  let component: CadminPainManageVehicleComponent;
  let fixture: ComponentFixture<CadminPainManageVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminPainManageVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminPainManageVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
