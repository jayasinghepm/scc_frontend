import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminBranchesComponent } from './cadmin-branches.component';

describe('CadminBranchesComponent', () => {
  let component: CadminBranchesComponent;
  let fixture: ComponentFixture<CadminBranchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminBranchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminBranchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
