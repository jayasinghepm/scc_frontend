import {Component, OnInit} from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {NbDialogService, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
import {MatDialog} from "@angular/material/dialog";
import {UpdateSolarProjectComponentComponent} from "./components/update-solar-project-component/update-solar-project-component.component";
import {UserState} from "../../../@core/auth/UserState";
import { LocalDataSource } from 'src/app/ng2-smart-table/src/public-api';
import { MassterDataService } from 'src/app/@core/service/masster-data.service';

@Component({
  selector: 'app-solar-component',
  templateUrl: './solar-component.component.html',
  styleUrls: ['./solar-component.component.scss']
})
export class SolarComponentComponent implements OnInit {

  public projects = [];
  source: LocalDataSource = new LocalDataSource();
  data: any;
  constructor(private boService:BackendService,
              private masterData: MassterDataService,
              private toastService: NbToastrService,
              private dialogService: NbDialogService,
              private dialog: MatDialog,
              ) {
    this.loadProjects()
  }

  ngOnInit() {
    this.mySetting.actions = this.initActions()
    this.settings = Object.assign({}, this.mySetting);
  }

  initActions():any {

    if (UserState.getInstance().projectStatus == 4) { //data entry
      const entitle = UserState.getInstance().pageRoutes.filter(v => {
        let page = v.split('-')[0];
        if (+page === 4) {
          return v;
        }
      })[0];
      if (entitle !== undefined) {
        const add = entitle.split('-')[1] === '1'? true: false;
        const edit = entitle.split('-')[2] === '1'? true: false;
        const del = entitle.split('-')[3] === '1'? true: false;
        return {
          add: add,
          edit: edit,
          delete: del,
        }

      }
    }else {
      return {
        add: false,
        edit: false,
        delete: false,
      }
    }

  }

  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      // delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      // },
      type: {
        title: 'Mitigation ACtion Type',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      name: {
        title: 'Name',
        type: 'string',
        filter: false,
        sort: false,
      },
      description: {
        title: 'Description',
        type: 'string',
        filter: false,
        sort: false,
      }
    },
  };

  mySetting = JSON.parse(JSON.stringify(this.settings))

  openEditProject(event: any, data:any) {
   console.log('dddd',event.data)
   let data1=event.data;
    const dialogRef = this.dialog.open(UpdateSolarProjectComponentComponent,
      {
        data : {
          isEdit : data1 !== undefined,
          data: event.data,
        },
        width: '800px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(canLoad => {
        if (canLoad) {
          this.loadProjects();
        }
    });
  }
  onClickPrevPage(){}
  onClickNextPage(){}
  loadProjects() {
    this.boService.sendRequestToBackend(RequestGroup.Mitigation, RequestType.GetMitigationProjects, {

        comId: UserState.getInstance().companyId,
        branchId: -1,

    }).then(data=> {
      console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        //todo filter only solar projects
        if (data.DAT.projects) {
          // this.projects = data.DAT.projects.filter(project => project.type ==1);
          this.projects = data.DAT.projects;
          for(let project of this.projects){
            this.masterData.getMitigationActionTypeName(project.type).subscribe((res=>{
              console.log('iiiiiiiiiiiiiiiiiiiiii',res)
              project.type=res;
              console.log('iiii',this.projects)
              
            }))
          }
          this.source.load(this.projects);
         
        }
      }
    })
  }

  onDeleteProject(event:any) {
    console.log(event.data.projectId)
      if (event.data.projectId) {
        const dialogRef = this.dialog.open(DeleteConfirmComponent,
          {
            data : { },
            width: '400px',
            panelClass: 'no-border-page-wrapper',
            disableClose: true,
          }).afterClosed().subscribe(d => {
          if (d) {
            this.boService.sendRequestToBackend(RequestGroup.Mitigation, RequestType.ManageMitigationProject, {

                dto: {
                  action: 3,
                  projectId: event.data.projectId,
                }

            }).then(data=> {
              // console.log(data)
              if (data.HED != undefined && data.HED.RES_STS == 1) {
                this.toastService.success('','Deleted successfully!')
                this.loadProjects();
              }
            })


            // });
          }
        });

      }
  }



}
