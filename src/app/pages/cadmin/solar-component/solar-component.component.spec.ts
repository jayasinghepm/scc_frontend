import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolarComponentComponent } from './solar-component.component';

describe('SolarComponentComponent', () => {
  let component: SolarComponentComponent;
  let fixture: ComponentFixture<SolarComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolarComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
