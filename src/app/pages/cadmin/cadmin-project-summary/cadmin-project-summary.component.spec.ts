import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminProjectSummaryComponent } from './cadmin-project-summary.component';

describe('CadminProjectSummaryComponent', () => {
  let component: CadminProjectSummaryComponent;
  let fixture: ComponentFixture<CadminProjectSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminProjectSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminProjectSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
