import { Component, OnInit } from '@angular/core';
import {UserState} from "../../../../@core/auth/UserState";
import {PageIdEnums} from "../../../../@core/enums/PageIdEnums";
import {BackendService} from "../../../../@core/rest/bo_service";
import {NbDialogService, NbToastrService} from "@nebular/theme";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-mitigation-view',
  templateUrl: './mitigation-view.component.html',
  styleUrls: ['./mitigation-view.component.scss']
})
export class MitigationViewComponent implements OnInit {

  colors = ['#5470C6', '#91CC75', '#EE6666'];

  public chart_options =   {
    color: this.colors,

    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross'
      }
    },
    grid: {
      right: '20%'
    },
    toolbox: {
      feature: {
        dataView: {show: false, readOnly: false},
        restore: {show: false},
        saveAsImage: {show: false}
      }
    },
    legend: {
      data: ['Electricity', 'Saved Emissions'],
    },
    xAxis: [
      {
        type: 'category',
        axisTick: {
          alignWithLabel: true
        },
        data: []
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: 'Electricity',
        min: 0,

        position: 'left',
        axisLine: {
          show: true,
          lineStyle: {
            color: this.colors[0]
          }
        },
        axisLabel: {
          formatter: '{value} kWh'
        }
      },
      {
        type: 'value',
        name: 'Saved Emissions',
        min: 0,
        position: 'right',
        offset: 80,
        axisLine: {
          show: true,
          lineStyle: {
            color: this.colors[1]
          }
        },
        axisLabel: {
          formatter: '{value} tCO2e'
        }
      }

    ],
    series: [
      {
        name: 'Electricity',
        type: 'bar',
        data: []
      },

      {
        name: 'Saved Emissions',
        type: 'line',
        yAxisIndex: 1,
        data: []
      }
    ]
  };

  public selectedProject: number = -1;

  public projects = [{id: -1, name: 'Not Selected'}];

  public data :{[projectId: number] : {
      annual: {
        y : {
          emissions : any[],
          productions: any[],
        },
        x: any[],
      },
      monthly: {
       [year:string] : {
         y: {
           emissions : any[],
           productions: any[],
         },
         x: any[],
       }
      }
    }} = {}

  public mode: number = 1; //1-monthly, 2-annual

  public selectedYear = '';
  public years =[];

  public months = [];

  constructor(private boService:BackendService,
              private masterData: MassterDataService,) {

    this.loadData();
  }

  ngOnInit() {
    this.months = [
      { id: 0, name: 'Jan'},
      { id: 1, name: 'Feb'},
      { id: 2, name: 'Mar'},
      { id: 3, name: 'Apr'},
      { id: 4, name: 'May'},
      { id: 5, name: 'Jun'},
      { id: 6, name: 'Jul'},
      { id: 7, name: 'Aug'},
      { id: 8, name: 'Sep'},
      { id: 9, name: 'Oct'},
      { id: 10, name: 'Nov'},
      { id: 11, name: 'Dec'},

    ]
  }

  getMonth(month) {
    return this.months.filter(m => m.id == month)[0].name;
  }

  loadData() {
    this.boService.sendRequestToBackend(RequestGroup.Mitigation, RequestType.GetMitigationProjectMonthlyActivity, {

      comId: UserState.getInstance().companyId,
      branchId: -1,

    }).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        //todo data and projects
        if (data.DAT && data.DAT.actData && data.DAT.actData && data.DAT.actData.projectsData) {
           data.DAT.actData.projectsData;
           this.convertData(data.DAT.actData.projectsData);


          this.projects = data.DAT.actData.projectsData ? data.DAT.actData.projectsData.map(project => {
            return {
              id : project.projectId,
              name: project.projectName,
            }
          }) : [];
          if (this.projects.length) {
            this.selectedProject = this.projects[0].id;
            this.onChangeProject(null, this.selectedProject)

            // if (data.DAT.actData.projectsData[0].annualData && data.DAT.actData.projectsData[0].annualData.length) {
            //   this.selectedYear = data.DAT.actData.projectsData[0].annualData[0].year;
            //
            //   this.mode = 1;
            // }
          }
        }

      }
    })
  }

  convertData(data:any) {
    console.log(data)
    if (data) {
      data.forEach(project => {

        if (project && project.annualData) {
          this.data[project.projectId] = {
            annual: {
              y: {
                emissions : [],
                productions: [],
              },
              x: [],
            },
            monthly: {

            }
          }

          this.data[project.projectId].annual.x= project.annualData.map(dataPoint => dataPoint.year);




          project.annualData.forEach(yearData => {


            this.data[project.projectId].annual.y.emissions.push(yearData.emissionsSaved);
            this.data[project.projectId].annual.y.productions.push(yearData.value);

            this.data[project.projectId].monthly = {
              ...this.data[project.projectId].monthly,
               [yearData.year] : {
                 x: [],
                 y: {
                   emissions: [],
                   productions: [],
                 }
               }
            }

            if (yearData.monthlyData) {
              yearData.monthlyData.forEach(monData => {
                this.data[project.projectId].monthly[yearData.year].x.push(this.getMonth(monData.month));
                this.data[project.projectId].monthly[yearData.year].y.productions.push(monData.value)
                this.data[project.projectId].monthly[yearData.year].y.emissions.push(monData.emissionSaved)
              })

            }




          })
        }
      })
    }

    this.chart_options = JSON.parse(JSON.stringify(this.chart_options))
  }

  showChart():Boolean {
    return new Boolean(UserState.getInstance().pages[PageIdEnums.RenewableSolar-1] );
  }


  onChangeProject($event, projectId) {

    this.years = this.data[projectId].annual.x;
    if (!this.years || !this.years.length || !Object.keys(this.data[projectId].monthly)[0] ){
      this.chart_options.xAxis = [{
        type: 'category',
        axisTick: {
          alignWithLabel: true
        },
        data:  [],
      }]

      this.chart_options.series = [
        {
          name: 'Electricity',
          type: 'bar',
          data:  [],
        },

        {
          name: 'Saved Emissions',
          type: 'line',
          yAxisIndex: 1,
          data:  [],
        }
      ]

      this.mode = 1;
      this.selectedYear = ""

      this.chart_options = JSON.parse(JSON.stringify(this.chart_options))

      return
    }

    this.chart_options.xAxis = [{
      type: 'category',
      axisTick: {
        alignWithLabel: true
      },
      data: !this.data[projectId] ? [] : Object.keys(this.data[projectId].monthly)? this.data[projectId].monthly[Object.keys(this.data[projectId].monthly)[0]].x : [],
    }]

    this.chart_options.series = [
      {
        name: 'Electricity',
        type: 'bar',
        data: !this.data[projectId] ? [] : Object.keys(this.data[projectId].monthly)? this.data[projectId].monthly[Object.keys(this.data[projectId].monthly)[0]].y.productions : [],
      },

      {
        name: 'Saved Emissions',
        type: 'line',
        yAxisIndex: 1,
        data: !this.data[projectId] ? [] : Object.keys(this.data[projectId].monthly)? this.data[projectId].monthly[Object.keys(this.data[projectId].monthly)[0]].y.emissions : [],
      }
    ]

    this.mode = 1;
    this.selectedYear = Object.keys(this.data[projectId].monthly)[0];

    this.chart_options = JSON.parse(JSON.stringify(this.chart_options))

  }

  //annual or monthly
  onChangeMode($event, mode: number) {

    this.selectedYear = "";

    const projectId = this.selectedProject;

    if (mode == 1) {

      this.onChangeProject(null, projectId);


    }else if (mode ==2) {
      this.chart_options.xAxis = [{
        type: 'category',
        axisTick: {
          alignWithLabel: true
        },
        data: !this.data[projectId] ? [] :  this.data[projectId].annual.x,
      }]

      this.chart_options.series = [
        {
          name: 'Electricity',
          type: 'bar',
          data: !this.data[projectId] ? [] :  this.data[projectId].annual.y.productions ,
        },

        {
          name: 'Saved Emissions',
          type: 'line',
          yAxisIndex: 1,
          data: !this.data[projectId] ? [] :  this.data[projectId].annual.y.emissions ,
        }
      ]
    }
    this.chart_options = JSON.parse(JSON.stringify(this.chart_options))
  }

  onChangeYear($event, year: string) {


    const projectId = this.selectedProject;

    console.log(this.data[projectId].monthly)

    this.chart_options.xAxis = [{
      type: 'category',
      axisTick: {
        alignWithLabel: true
      },
      data: !this.data[projectId] ? [] : Object.keys(this.data[projectId].monthly)? this.data[projectId].monthly[year].x : [],
    }]

    this.chart_options.series = [
      {
        name: 'Electricity',
        type: 'bar',
        data: !this.data[projectId] ? [] : Object.keys(this.data[projectId].monthly)? this.data[projectId].monthly[year].y.productions : [],
      },

      {
        name: 'Saved Emissions',
        type: 'line',
        yAxisIndex: 1,
        data: !this.data[projectId] ? [] : Object.keys(this.data[projectId].monthly)? this.data[projectId].monthly[year].y.emissions : [],
      }
    ]

    this.mode = 1;

    this.chart_options = JSON.parse(JSON.stringify(this.chart_options))
  }




}
