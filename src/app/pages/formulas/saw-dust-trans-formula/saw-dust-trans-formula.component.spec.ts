import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SawDustTransFormulaComponent } from './saw-dust-trans-formula.component';

describe('SawDustTransFormulaComponent', () => {
  let component: SawDustTransFormulaComponent;
  let fixture: ComponentFixture<SawDustTransFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SawDustTransFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SawDustTransFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
