import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-freight-formula',
  templateUrl: './freight-formula.component.html',
  styleUrls: ['./freight-formula.component.scss']
})
export class FreightFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<FreightFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
