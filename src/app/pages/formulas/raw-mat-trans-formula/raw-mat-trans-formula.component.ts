import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-raw-mat-trans-formula',
  templateUrl: './raw-mat-trans-formula.component.html',
  styleUrls: ['./raw-mat-trans-formula.component.scss']
})
export class RawMatTransFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<RawMatTransFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
