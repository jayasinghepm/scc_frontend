import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawMatTransFormulaComponent } from './raw-mat-trans-formula.component';

describe('RawMatTransFormulaComponent', () => {
  let component: RawMatTransFormulaComponent;
  let fixture: ComponentFixture<RawMatTransFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawMatTransFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawMatTransFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
