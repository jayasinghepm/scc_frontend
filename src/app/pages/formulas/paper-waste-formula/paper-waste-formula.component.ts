import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-paper-waste-formula',
  templateUrl: './paper-waste-formula.component.html',
  styleUrls: ['./paper-waste-formula.component.scss']
})
export class PaperWasteFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<PaperWasteFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
