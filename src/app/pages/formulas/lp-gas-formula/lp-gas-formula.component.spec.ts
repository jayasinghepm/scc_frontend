import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpGasFormulaComponent } from './lp-gas-formula.component';

describe('LpGasFormulaComponent', () => {
  let component: LpGasFormulaComponent;
  let fixture: ComponentFixture<LpGasFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpGasFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpGasFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
