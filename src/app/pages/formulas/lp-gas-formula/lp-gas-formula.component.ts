import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-lp-gas-formula',
  templateUrl: './lp-gas-formula.component.html',
  styleUrls: ['./lp-gas-formula.component.scss']
})
export class LpGasFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<LpGasFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }
}
