import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-forklifts-formula',
  templateUrl: './forklifts-formula.component.html',
  styleUrls: ['./forklifts-formula.component.scss']
})
export class ForkliftsFormulaComponent implements OnInit {

  public isPetrol : boolean = false;

  constructor( public dialogRef: MatDialogRef<ForkliftsFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)

     if (popupData.data.emissionDetails.isPetrol) {
       this.isPetrol = true;
     }

  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
