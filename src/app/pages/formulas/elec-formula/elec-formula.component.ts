import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-elec-formula',
  templateUrl: './elec-formula.component.html',
  styleUrls: ['./elec-formula.component.scss']
})
export class ElecFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<ElecFormulaComponent>,
  @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }
}
