import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElecFormulaComponent } from './elec-formula.component';

describe('ElecFormulaComponent', () => {
  let component: ElecFormulaComponent;
  let fixture: ComponentFixture<ElecFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElecFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElecFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
