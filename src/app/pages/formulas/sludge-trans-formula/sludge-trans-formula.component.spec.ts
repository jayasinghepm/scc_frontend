import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SludgeTransFormulaComponent } from './sludge-trans-formula.component';

describe('SludgeTransFormulaComponent', () => {
  let component: SludgeTransFormulaComponent;
  let fixture: ComponentFixture<SludgeTransFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SludgeTransFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SludgeTransFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
