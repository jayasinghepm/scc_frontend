import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-furnace-oil-formula',
  templateUrl: './furnace-oil-formula.component.html',
  styleUrls: ['./furnace-oil-formula.component.scss']
})
export class FurnaceOilFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<FurnaceOilFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
