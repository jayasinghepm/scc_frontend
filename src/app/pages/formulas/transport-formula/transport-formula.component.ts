import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-transport-formula',
  templateUrl: './transport-formula.component.html',
  styleUrls: ['./transport-formula.component.scss']
})
export class TransportFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<TransportFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }
}
