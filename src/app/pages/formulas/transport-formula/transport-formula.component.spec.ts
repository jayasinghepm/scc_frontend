import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportFormulaComponent } from './transport-formula.component';

describe('TransportFormulaComponent', () => {
  let component: TransportFormulaComponent;
  let fixture: ComponentFixture<TransportFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
