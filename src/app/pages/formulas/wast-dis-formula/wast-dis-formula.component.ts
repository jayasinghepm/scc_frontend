import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-wast-dis-formula',
  templateUrl: './wast-dis-formula.component.html',
  styleUrls: ['./wast-dis-formula.component.scss']
})
export class WastDisFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<WastDisFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
