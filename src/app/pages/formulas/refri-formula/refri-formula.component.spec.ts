import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefriFormulaComponent } from './refri-formula.component';

describe('RefriFormulaComponent', () => {
  let component: RefriFormulaComponent;
  let fixture: ComponentFixture<RefriFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefriFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefriFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
