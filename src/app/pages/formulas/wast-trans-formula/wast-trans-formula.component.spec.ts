import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WastTransFormulaComponent } from './wast-trans-formula.component';

describe('WastTransFormulaComponent', () => {
  let component: WastTransFormulaComponent;
  let fixture: ComponentFixture<WastTransFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WastTransFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WastTransFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
