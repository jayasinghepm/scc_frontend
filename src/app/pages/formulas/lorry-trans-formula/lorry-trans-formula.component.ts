import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-lorry-trans-formula',
  templateUrl: './lorry-trans-formula.component.html',
  styleUrls: ['./lorry-trans-formula.component.scss']
})
export class LorryTransFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<LorryTransFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }
}
