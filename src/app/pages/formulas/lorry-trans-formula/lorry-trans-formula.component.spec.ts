import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LorryTransFormulaComponent } from './lorry-trans-formula.component';

describe('LorryTransFormulaComponent', () => {
  let component: LorryTransFormulaComponent;
  let fixture: ComponentFixture<LorryTransFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LorryTransFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LorryTransFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
