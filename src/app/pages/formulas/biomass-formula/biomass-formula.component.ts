import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-biomass-formula',
  templateUrl: './biomass-formula.component.html',
  styleUrls: ['./biomass-formula.component.scss']
})
export class BiomassFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<BiomassFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
