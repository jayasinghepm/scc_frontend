import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MunWaterFormulaComponent } from './mun-water-formula.component';

describe('MunWaterFormulaComponent', () => {
  let component: MunWaterFormulaComponent;
  let fixture: ComponentFixture<MunWaterFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MunWaterFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MunWaterFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
