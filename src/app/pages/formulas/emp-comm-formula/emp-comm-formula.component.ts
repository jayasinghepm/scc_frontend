import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-emp-comm-formula',
  templateUrl: './emp-comm-formula.component.html',
  styleUrls: ['./emp-comm-formula.component.scss']
})
export class EmpCommFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<EmpCommFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }
}
