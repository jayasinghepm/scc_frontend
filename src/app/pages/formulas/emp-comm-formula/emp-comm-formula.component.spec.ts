import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpCommFormulaComponent } from './emp-comm-formula.component';

describe('EmpCommFormulaComponent', () => {
  let component: EmpCommFormulaComponent;
  let fixture: ComponentFixture<EmpCommFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpCommFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpCommFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
