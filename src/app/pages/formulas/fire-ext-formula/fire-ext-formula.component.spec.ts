import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FireExtFormulaComponent } from './fire-ext-formula.component';

describe('FireExtFormulaComponent', () => {
  let component: FireExtFormulaComponent;
  let fixture: ComponentFixture<FireExtFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FireExtFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FireExtFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
