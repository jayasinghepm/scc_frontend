import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-ash-trans-formula',
  templateUrl: './ash-trans-formula.component.html',
  styleUrls: ['./ash-trans-formula.component.scss']
})
export class AshTransFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<AshTransFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
