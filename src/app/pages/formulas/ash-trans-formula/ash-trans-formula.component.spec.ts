import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AshTransFormulaComponent } from './ash-trans-formula.component';

describe('AshTransFormulaComponent', () => {
  let component: AshTransFormulaComponent;
  let fixture: ComponentFixture<AshTransFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AshTransFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AshTransFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
