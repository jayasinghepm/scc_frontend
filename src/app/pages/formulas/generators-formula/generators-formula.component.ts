import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-generators-formula',
  templateUrl: './generators-formula.component.html',
  styleUrls: ['./generators-formula.component.scss']
})
export class GeneratorsFormulaComponent implements OnInit {
  public t_dg = 0;

  constructor( public dialogRef: MatDialogRef<GeneratorsFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
