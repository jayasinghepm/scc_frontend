import { NgModule } from '@angular/core';
import {NbIconModule, NbMenuModule} from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './routing/pages-routing.module';
import {CadminModule} from "./cadmin/cadmin.module";
import {ClientModule} from "./client/client.module";
import {AdminModule} from "./admin/admin.module";
import {GuardsModule} from "./routing/guards/guards.module";
import { SettingComponent } from './setting/setting.component';
import { WasteDisFactorsComponent } from './admin/waste-dis-factors/waste-dis-factors.component';
import { PubTransFactorsComponent } from './admin/pub-trans-factors/pub-trans-factors.component';
import { DeleteConfirmComponent } from './delete-confirm/delete-confirm.component';
import { UpdateAdminFurnaceComponent } from './admin/emission/admin-furnace-oil/update-admin-furnace/update-admin-furnace.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbIconModule,
    GuardsModule.forRoot(),
  ],
  declarations: [
    PagesComponent,
    DeleteConfirmComponent,


  ],
  providers: [
  ],
  entryComponents:[
    DeleteConfirmComponent,
  ]
})
export class PagesModule {
}
