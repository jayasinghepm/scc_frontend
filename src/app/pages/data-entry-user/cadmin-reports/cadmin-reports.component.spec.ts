import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminReportsComponent } from './cadmin-reports.component';

describe('CadminReportsComponent', () => {
  let component: CadminReportsComponent;
  let fixture: ComponentFixture<CadminReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
