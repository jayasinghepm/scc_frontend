import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/@core/rest/bo_service';
import { MassterDataService } from 'src/app/@core/service/masster-data.service';
import {NbToastrService, NbWindowService, NbGlobalPhysicalPosition} from '@nebular/theme';
import { Source } from 'webpack-sources';
import { RequestGroup } from 'src/app/@core/enums/request-group-enum';
import { RequestType } from 'src/app/@core/enums/request-type-enums';
import {UserState} from "../../../@core/auth/UserState";
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-cadmin-branches',
  templateUrl: './cadmin-branches.component.html',
  styleUrls: ['./cadmin-branches.component.scss']
})
export class DataEntryUserBranchesComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'internal',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      name: {
        title: 'Name',
        type: 'string',
        filter: true,
        sort: true,
    
      },
      code: {
        title: 'Code',
        type: 'string',
        filter: true,
        sort: true,
      },
      addr1: {
        title: 'Address 1',
        type: 'string',
        filter: true,
        sort: true,
      },
      addr2: {
        title: 'Address 2',
        type: 'string',
        filter: true,
        sort: true,
    

      },
      district: {
        title: 'District',
        type: 'string',
        filter: true,
        sort: true,
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: [],
            }
          }
        }
      }
    },
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))


  private filterModel = {
    name: {value: '', type: 4, col: 3},
    addr1: {value: '', type: 4, col: 3},
    addr2: {value: '', type: 4, col: 3},
    district: {value: '', type: 4, col: 3},
    code: {value: '', type: 4, col: 3},
    id: {value: 0, type: 1, col: 1},
    companyId: {value: UserState.getInstance().companyId, type: 1, col: 1},
  }

  private sortModel = {
    name: {dir: ''},
    addr1: {dir: ''},
    addr2: {dir: ''},
    district: {dir: ''},
    code: {dir: ''},
    id: {dir: ''},
  }


  source: LocalDataSource = new LocalDataSource();


  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private  toastSerivce:  NbToastrService,
    private windowService: NbWindowService,
    private dialog: MatDialog,
  )  { 
    this.loadFiltered(this.pg_current);
  }

  ngOnInit() {
    this.masterData.getDistricts().subscribe(d=> {
      this.mySetting.columns.district.editor.config.completer.data = d;
      this.settings = Object.assign({}, this.mySetting)
    })
  }

  


  onUpdate(event: any, isNew) {
    console.log(event);
    // update
    if (event.data !== undefined) {
      if (JSON.stringify(event.data) === JSON.stringify(event.newData)) {
        // ignore no change
        this.toastSerivce.show('', 'No change in data', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      } else {
        if (this.validateEntry(event.newData, true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Institution,
            RequestType.ManageBranch,
            this.fromTable(event.newData, true)
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto!== undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
              this.loadFiltered(this.pg_current);
              event.confirm.resolve(event.newData);
              this.masterData.loadBranches();
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }
    }
    // new
    else {
      if (this.validateEntry(event.newData, false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Institution,
           RequestType.ManageBranch,
          this.fromTable(event.newData, false)
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            this.loadFiltered(this.pg_current);
            event.confirm.resolve(event.newData);
            this.masterData.loadBranches();
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }
  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      {
        PAGE_NUMBER: this.pg_current,
        FILTER_MODEL: {
          companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
        }
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.list.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.code === undefined || data.code === '' ){
      console.log(1)
      return false;
    }
    // if (data.addr1 === undefined || data.addr1 === '' ){
    //   console.log(2)
    //   return false;
    // }
    // if (data.addr2 === undefined || data.addr2 === '' ){
    //   console.log(3)
    //   return false;
    // }
    if (data.district === undefined || data.district === '' ){
      console.log(4)
      return false;
    }

    if (data.name === undefined || data.name === '' ){
      console.log(4)
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      console.log(5)
      //show snack bar
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      console.log(6)
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {
    let company;
    let name;
    let code;
    let id;
    let addr1;
    let addr2;
    let district;


    // this.masterData.getCompanyName(dto.companyId).subscribe(data => { company = data; }).unsubscribe();

    id = dto.id;
    name = dto.name;
    code = dto.code;
    addr1 = dto.addr1
    addr2 = dto.addr2;
    district = dto.district;

    return {
      id,
      company: dto.company === undefined ?  UserState.getInstance().companyName : dto.company,
      name,
      addr1,
      addr2,
      code,
      district
    };



  }

  private fromTable(data: any, onEdit: boolean): any {
    let company;
    let name = data.name;
    let code = data.code;
    let id = onEdit ? data.id : -1;
    let addr1  = data.addr1;
    let addr2 = data.addr2;
    let district = data.district;
    
 // todo : company
 //    this.masterData.getCompanyId(data.company).subscribe(d => company = d)
    // this.masterData.getBranchId(data.branch).subscribe(data => { branch = data;});
    return {
      DATA: {
        id,
        companyId: UserState.getInstance().companyId,
      name,
      addr1,
      addr2,
      code,
      district,
        company: UserState.getInstance().companyName,
      }
    }

  }


  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    switch($event.query.column.id) {
      case 'name':  {
        this.filterModel.name.value = $event.query.query;
        break;
      }  case 'id':  {
        this.filterModel.id.value = $event.query.query;
        break;
      }  case 'addr1':  {
        this.filterModel.addr1.value = $event.query.query;
        break;
      }  case 'addr2':  {
        this.filterModel.addr2.value = $event.query.query;
        break;
      }
      case 'district':  {
        this.filterModel.district.value = $event.query.query;
        break;
      }
      case 'code':  {
        this.filterModel.code.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  = {
      name: {dir: ''},
      addr1: {dir: ''},
      addr2: {dir: ''},
      district: {dir: ''},
      code: {dir: ''},
      id: {dir: ''},
    }
    console.log($event)
    switch($event.id) {
      case 'name':  {
        this.sortModel.name.dir = $event.direction;
        break;
      }  case 'addr1':  {
        this.sortModel.addr1.dir = $event.direction;
        break;
      }  case 'addr2':  {
        this.sortModel.addr2.dir = $event.direction;
        break;
      }  case 'district':  {
        this.sortModel.district.dir = $event.direction;
        break;
      }  case 'code':  {
        this.sortModel.code.dir = $event.direction;
        break;
      } case 'id':  {
        this.sortModel.id.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListBranch,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListBranch,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListBranch,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListBranch,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,

        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }
  }

  onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data: {},
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Institution,
          RequestType.ManageBranch,
          {
            DATA: {
              id: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }


  public createClientProfiles($event: MouseEvent) {
    // if (this.selectedCompany <= 0)
    // {
    //   this.toastSerivce.show('Select Company First', '', {
    //     status: 'warning',
    //     destroyByClick: true,
    //     duration: 2000,
    //     hasIcon: false,
    //     position: NbGlobalPhysicalPosition.TOP_RIGHT,
    //     preventDuplicates: true,
    //   })
    //   return;
    // }
    this.boService.sendRequestToBackend(RequestGroup.LoginUser, RequestType.CreateBranchLoginProfile, {
      comId: UserState.getInstance().companyId,
    }).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined) {
          if(data.DAT.profiles !== undefined) {
            let keys = Object.keys(data.DAT.profiles );
            let csvData = [['User Login Name', 'Password']];
            // @ts-ignore
            for (const k of keys){
              csvData.push([k, data.DAT.profiles[k]]);
            }
            this.downloadCSvFile(csvData);
          }
        }

      }
    });
  }


  downloadCSvFile(data: any) {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    let csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    let csvArray = csv.join('\r\n');

    var a = document.createElement('a');
    var blob = new Blob([csvArray], {type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = "password.csv";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }


}
