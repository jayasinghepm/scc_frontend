import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {DataEntryUserHomeComponent} from "./cadmin-home/cadmin-home.component";
import {DataEntryUserTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {DataEntryUserWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {DataEntryUserTransportComponent} from "./emission/client-transport/client-transport.component";
import {DataEntryUserEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {DataEntryUserMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {DataEntryUserBranchesComponent} from "./cadmin-branches/cadmin-branches.component";
import {DataEntryUserAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {DataEntryUserFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {DataEntryUserRefriComponent} from "./emission/client-refri/client-refri.component";
import {DataEntryUserElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {DataEntryUserWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {DataEntryUserGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {DataEntryUserSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {DataEntryUserReportsComponent} from "./cadmin-reports/cadmin-reports.component";
import {SeaAirFreightComponent} from "./emission/sea-air-freight/sea-air-freight.component";
import {BiomassComponent} from "./emission/biomass/biomass.component";
import {LpgasComponent} from "./emission/lpgas/lpgas.component";
import {BgasComponent} from "./emission/bgas/bgas.component";
import {DeuAshTransComponent} from "./emission/deu-ash-trans/deu-ash-trans.component";
import {DeuForkliftsComponent} from "./emission/deu-forklifts/deu-forklifts.component";
import {DeuFurnaceComponent} from "./emission/deu-furnace/deu-furnace.component";
import {DeuLorryTransComponent} from "./emission/deu-lorry-trans/deu-lorry-trans.component";
import {DeuOilGasTransComponent} from "./emission/deu-oil-gas-trans/deu-oil-gas-trans.component";
import {DeuRawMaterialTransComponent} from "./emission/deu-raw-material-trans/deu-raw-material-trans.component";
import {DeuSawDustTransComponent} from "./emission/deu-saw-dust-trans/deu-saw-dust-trans.component";
import {DeuSludgeTransComponent} from "./emission/deu-sludge-trans/deu-sludge-trans.component";
import {DueStaffTransComponent} from "./emission/due-staff-trans/due-staff-trans.component";
import {DeuVehicleOthersComponent} from "./emission/deu-vehicle-others/deu-vehicle-others.component";
import {DeuPaperWasteComponent} from "./emission/deu-paper-waste/deu-paper-waste.component";
import {DeuPaidManagerVehicleComponent} from "./emission/deu-paid-manager-vehicle/deu-paid-manager-vehicle.component";
import {
  NbCardModule,
  NbContextMenuModule,
  NbDialogModule,
  NbIconModule,
  NbListModule,
  NbMenuModule, NbTabsetModule
} from "@nebular/theme";
import {Ng2SmartTableModule} from "../../ng2-smart-table/src/lib/ng2-smart-table.module";
import {PagesRoutingModule} from "../routing/pages-routing.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {ThemeModule} from "../../@theme/theme.module";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatButtonModule} from "@angular/material/button";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatCardModule} from "@angular/material/card";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatChipsModule} from "@angular/material/chips";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatDialogModule} from "@angular/material/dialog";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatMenuModule} from "@angular/material/menu";
import {MatNativeDateModule, MatRippleModule} from "@angular/material/core";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatRadioModule} from "@angular/material/radio";
import {MatSelectModule} from "@angular/material/select";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatSliderModule} from "@angular/material/slider";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatStepperModule} from "@angular/material/stepper";
import {MatTableModule} from "@angular/material/table";
import {MatTabsModule} from "@angular/material/tabs";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatTooltipModule} from "@angular/material/tooltip";
import {SelectDropDownModule} from "ngx-select-dropdown";
import {NgxEchartsModule} from "ngx-echarts";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {EmissionFactorsComponent} from "./emission-factors/emission-factors.component";
import {PubTransFactorsComponent} from "./pub-trans-factors/pub-trans-factors.component";
import {WasteDisComponent} from "./waste-dis/waste-dis.component";
import {DeuProjectSummaryComponent} from "./deu-project-summary/deu-project-summary.component";
import {DeoSoloarComponentComponent} from "./deo-soloar-component/deo-soloar-component.component";



const routes: Routes = [
  {
    path: 'home',
    component: DataEntryUserHomeComponent
  },
  {
    path: 'reports',
    component: DataEntryUserReportsComponent,
  },
  {
    path: 'branches',
    component: DataEntryUserBranchesComponent
  },
  {
    path: 'summary_data_inputs',
    component: DataEntryUserSummaryDataInputsComponent
  },
  {
    path: 'air_travel',
    component: DataEntryUserAirTravelComponent,

  },
  {
    path: 'fire_ext',
    component: DataEntryUserFireExtComponent,
  },
  {
    path: 'refri',
    component: DataEntryUserRefriComponent,
  },
  {
    path: 'generators',
    component: DataEntryUserGeneratorsComponent,
  },
  {
    path: 'electricity',
    component: DataEntryUserElectricityComponent,
  },
  {
    path: 'waste_disposal',
    component: DataEntryUserWasteDisposalComponent,
  },
  {
    path: 'municipal_water',
    component: DataEntryUserMunicipalWaterComponent,
  },
  {
    path: 'emp_comm',
    component: DataEntryUserEmpCommuComponent,
  },
  {
    path: 'transport',
    component: DataEntryUserTransportComponent,
  },
  {
    path: 'waste_transport',
    component: DataEntryUserWasteTransportComponent
  },
  {
    path: 'trans_loc_purch',
    component: DataEntryUserTransLocPurComponent
  },
  {
    path: 'freight_transport',
    component: SeaAirFreightComponent
  },
  {
    path: 'biomass',
    component: BiomassComponent,

  },
  {
    path: 'lpgas',
    component: LpgasComponent,

  },
  {
    path: 'bgas',
    component: BgasComponent,

  },
  {
    path: 'ash_transport',
    component: DeuAshTransComponent,

  },
  {
    path: 'forklifts',
    component: DeuForkliftsComponent,

  }
  ,
  {
    path: 'furnace-oil',
    component: DeuFurnaceComponent,

  }
  ,{
    path: 'lorry-transport',
    component: DeuLorryTransComponent,

  }
  ,{
    path: 'oilgas-transport',
    component: DeuOilGasTransComponent,

  },
  {
    path: 'paid-manager-vehicle',
    component: DeuPaidManagerVehicleComponent,
  },
  {
    path: 'paper-waste',
    component: DeuPaperWasteComponent
  }
  ,{
    path: 'raw-material-transport',
    component: DeuRawMaterialTransComponent,

  },
  {
    path: 'saw-dust-transport',
    component: DeuSawDustTransComponent,

  },
  {
    path: 'sludge-transport',
    component: DeuSludgeTransComponent,

  },
  {
    path: 'staff-transport',
    component: DueStaffTransComponent,

  },
  {
    path: 'vehicle_others',
    component: DeuVehicleOthersComponent,
  },
  {
    path: 'factors',
    component: EmissionFactorsComponent,

  },
  {
    path: 'pub_factors',
    component: PubTransFactorsComponent,

  },
  {
    path: 'waste_factors',
    component: WasteDisComponent,
  },
  {
    path: 'project_summary',
    component: DeuProjectSummaryComponent,
  },
  {
    path: 'solar',
    component: DeoSoloarComponentComponent,
  }

];


@NgModule({
  declarations: [  ],
  imports: [
    NbContextMenuModule,
    Ng2SmartTableModule,

    FormsModule,
    CommonModule,
    ThemeModule,
    NbMenuModule,
    NbIconModule,
    NbCardModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    SelectDropDownModule,
    NbDialogModule,
    NbListModule,
    NgxEchartsModule,
    NbTabsetModule,
    NgbModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class DataEntryUserRoutingModule { }
