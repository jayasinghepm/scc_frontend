import {Component, Inject, OnInit, Optional} from '@angular/core';
import {NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {validateYear} from "../../../../../@core/utils/validator";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";

@Component({
  selector: 'app-deo-data-entry-solar-component',
  templateUrl: './deo-data-entry-solar-component.component.html',
  styleUrls: ['./deo-data-entry-solar-component.component.scss']
})
export class DeoDataEntrySolarComponentComponent implements OnInit {

  public actData = {
   action : 1,
   id: -1,
   projectId: -1,
   value: 0,
   year: '',
   month: 0,
   unit: 1
  }

  public months = [];

  constructor(
    public dialogRef: MatDialogRef<DeoDataEntrySolarComponentComponent>,
    private toastService: NbToastrService,
    private boService: BackendService,
    private masterData: MassterDataService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: {isEdit: boolean, data: any, projectName: string, projectId: number}
  ) {


  }

  ngOnInit() {
    console.log(this.popupData)
    this.months = [
      { id: 0, name: 'Jan'},
      { id: 1, name: 'Feb'},
      { id: 2, name: 'Mar'},
      { id: 3, name: 'Apr'},
      { id: 4, name: 'May'},
      { id: 5, name: 'Jun'},
      { id: 6, name: 'Jul'},
      { id: 7, name: 'Aug'},
      { id: 8, name: 'Sep'},
      { id: 9, name: 'Oct'},
      { id: 10, name: 'Nov'},
      { id: 11, name: 'Dec'},

    ]
    this.actData.projectId = this.popupData.projectId;
    if (this.popupData.isEdit) {
      // this.actData.projectId = this.popupData.projectId;
      this.actData.month = this.popupData.data.month;
      this.actData.year = this.popupData.data.year;
      this.actData.value = this.popupData.data.value;
      this.actData.id = this.popupData.data.id;
    }
    console.log(this.actData)
  }

  save($event) {
    if (!this.validateData()) {
      this.toastService.warning("Enter valid inputs!");
      return;
    }

    this.boService.sendRequestToBackend(RequestGroup.Mitigation, RequestType.ManageMitigationProjectMonthlyActivity, {
        data: {
          ...this.actData,
          action: this.popupData.isEdit ? 2 : 1,
        }
    }).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        this.toastService.success('','Saved successfully!')
        this.dialogRef.close(true);
      }
    })

  }

  closeDialog() {
    this.dialogRef.close();

  }

  onChangeMonth($event: MatSelectChange) {
    // this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  validateData() : boolean {
    if (this.popupData.isEdit && this.actData.id <=0) {

      return false;
    }
    if (this.actData.month < 0) {

    }
    if (!this.actData.year || !(validateYear(this.actData.year, false) || validateYear(this.actData.year, true))) {

      return false;
    }
    if (this.actData.value < 0) {

      return false;
    }
    return true;

  }


}
