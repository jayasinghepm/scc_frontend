import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeoDataEntrySolarComponentComponent } from './deo-data-entry-solar-component.component';

describe('DeoDataEntrySolarComponentComponent', () => {
  let component: DeoDataEntrySolarComponentComponent;
  let fixture: ComponentFixture<DeoDataEntrySolarComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeoDataEntrySolarComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeoDataEntrySolarComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
