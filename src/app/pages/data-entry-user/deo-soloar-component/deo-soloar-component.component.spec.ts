import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeoSoloarComponentComponent } from './deo-soloar-component.component';

describe('DeoSoloarComponentComponent', () => {
  let component: DeoSoloarComponentComponent;
  let fixture: ComponentFixture<DeoSoloarComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeoSoloarComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeoSoloarComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
