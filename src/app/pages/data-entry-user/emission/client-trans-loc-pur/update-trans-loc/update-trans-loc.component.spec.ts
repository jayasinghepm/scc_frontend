import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTransLocComponent } from './update-trans-loc.component';

describe('UpdateTransLocComponent', () => {
  let component: UpdateTransLocComponent;
  let fixture: ComponentFixture<UpdateTransLocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTransLocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTransLocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
