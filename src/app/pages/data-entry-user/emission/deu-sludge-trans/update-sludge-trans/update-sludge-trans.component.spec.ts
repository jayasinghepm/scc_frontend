import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSludgeTransComponent } from './update-sludge-trans.component';

describe('UpdateSludgeTransComponent', () => {
  let component: UpdateSludgeTransComponent;
  let fixture: ComponentFixture<UpdateSludgeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSludgeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSludgeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
