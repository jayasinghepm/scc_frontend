import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientElectricityComponent } from './client-electricity.component';

describe('ClientElectricityComponent', () => {
  let component: ClientElectricityComponent;
  let fixture: ComponentFixture<ClientElectricityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientElectricityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientElectricityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
