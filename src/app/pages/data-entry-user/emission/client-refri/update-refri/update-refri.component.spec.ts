import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRefriComponent } from './update-refri.component';

describe('UpdateRefriComponent', () => {
  let component: UpdateRefriComponent;
  let fixture: ComponentFixture<UpdateRefriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRefriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRefriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
