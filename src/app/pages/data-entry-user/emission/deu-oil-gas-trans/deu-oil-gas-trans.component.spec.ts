import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuOilGasTransComponent } from './deu-oil-gas-trans.component';

describe('DeuOilGasTransComponent', () => {
  let component: DeuOilGasTransComponent;
  let fixture: ComponentFixture<DeuOilGasTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuOilGasTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuOilGasTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
