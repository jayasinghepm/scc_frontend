import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientMunicipalWaterComponent } from './client-municipal-water.component';

describe('ClientMunicipalWaterComponent', () => {
  let component: ClientMunicipalWaterComponent;
  let fixture: ComponentFixture<ClientMunicipalWaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientMunicipalWaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientMunicipalWaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
