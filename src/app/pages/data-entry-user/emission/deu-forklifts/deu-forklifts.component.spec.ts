import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuForkliftsComponent } from './deu-forklifts.component';

describe('DeuForkliftsComponent', () => {
  let component: DeuForkliftsComponent;
  let fixture: ComponentFixture<DeuForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
