import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuForkliftsComponent } from './update-deu-forklifts.component';

describe('UpdateDeuForkliftsComponent', () => {
  let component: UpdateDeuForkliftsComponent;
  let fixture: ComponentFixture<UpdateDeuForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
