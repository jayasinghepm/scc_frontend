import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEmpCommComponent } from './update-emp-comm.component';

describe('UpdateEmpCommComponent', () => {
  let component: UpdateEmpCommComponent;
  let fixture: ComponentFixture<UpdateEmpCommComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEmpCommComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEmpCommComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
