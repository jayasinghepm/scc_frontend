import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientEmpCommuComponent } from './client-emp-commu.component';

describe('ClientEmpCommuComponent', () => {
  let component: ClientEmpCommuComponent;
  let fixture: ComponentFixture<ClientEmpCommuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientEmpCommuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientEmpCommuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
