import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuLorryTransComponent } from './deu-lorry-trans.component';

describe('DeuLorryTransComponent', () => {
  let component: DeuLorryTransComponent;
  let fixture: ComponentFixture<DeuLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
