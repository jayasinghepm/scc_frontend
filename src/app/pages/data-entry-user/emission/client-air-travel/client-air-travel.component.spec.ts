import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAirTravelComponent } from './client-air-travel.component';

describe('ClientAirTravelComponent', () => {
  let component: ClientAirTravelComponent;
  let fixture: ComponentFixture<ClientAirTravelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAirTravelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAirTravelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
