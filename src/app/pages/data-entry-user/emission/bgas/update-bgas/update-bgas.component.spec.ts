import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBgasComponent } from './update-bgas.component';

describe('UpdateBgasComponent', () => {
  let component: UpdateBgasComponent;
  let fixture: ComponentFixture<UpdateBgasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBgasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBgasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
