import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuPaperWasteComponent } from './update-deu-paper-waste.component';

describe('UpdateDeuPaperWasteComponent', () => {
  let component: UpdateDeuPaperWasteComponent;
  let fixture: ComponentFixture<UpdateDeuPaperWasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuPaperWasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuPaperWasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
