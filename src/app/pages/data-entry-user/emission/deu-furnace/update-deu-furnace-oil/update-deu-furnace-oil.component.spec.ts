import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuFurnaceOilComponent } from './update-deu-furnace-oil.component';

describe('UpdateDeuFurnaceOilComponent', () => {
  let component: UpdateDeuFurnaceOilComponent;
  let fixture: ComponentFixture<UpdateDeuFurnaceOilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuFurnaceOilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuFurnaceOilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
