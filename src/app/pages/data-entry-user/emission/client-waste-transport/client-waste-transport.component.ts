import { Component, OnInit } from '@angular/core';
import {SmartTableData} from "../../../../@core/data/smart-table-completer";
import {SmartTableService} from "../../../../@core/service/smart-table.service";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {BackendService} from "../../../../@core/rest/bo_service";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {UserState} from "../../../../@core/auth/UserState";
import {MatDialog} from "@angular/material";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {Router} from "@angular/router";
import {CadminUpdateWasteTransportComponent} from "../../../cadmin/emission/client-waste-transport/update-waste-transport/update-waste-transport.component";
import {DataEntryUserUpdateWasteTransportComponent} from "./update-waste-transport/update-waste-transport.component";
import {AshTransFormulaComponent} from "../../../formulas/ash-trans-formula/ash-trans-formula.component";
import {WastTransFormulaComponent} from "../../../formulas/wast-trans-formula/wast-trans-formula.component";

@Component({
  selector: 'app-client-waste-transport',
  templateUrl: './client-waste-transport.component.html',
  styleUrls: ['./client-waste-transport.component.scss']
})
export class DataEntryUserWasteTransportComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,

      },
      wasteType: {
        title: 'Type of Waste',
        type: 'string',
        filter: true,
        sort: true,
      },
      subContractor: {
        title: 'Subcontractor',
        type: 'string',
        filter: true,
        sort: true,
      },
      vehicleType: {
        title: 'Vehicle Type',
        type: 'string',
        filter: true,
        sort: true,
      },
      fuelType: {
        title: 'Fuel Type',
        type: 'string',
        filter: true,
        sort: true,
      },
      distance: {
        title: 'Distance(up and down)km',
        type: 'number',
        filter: true,
        sort: true,
      },
      wasteTons: {
        title: 'Quantity(tons)',
        type: 'number',
        filter: true,
        sort: true,
      },
      loadingCapacity: {
        title: 'Vehicle Loading Capactity(tons)',
        type: 'number',
        filter: true,
        sort: true,
      },
      fuelEconomy: {
        title: 'Fuel Economy(km/l)',
        type: 'number',
        filter: true,
        sort: true,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'string',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCo2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };

  mySetting = JSON.parse(JSON.stringify(this.settings))

  private filterModel = {
    companyId: { value: UserState.getInstance().companyId , type: 1, col: 1},
    branch: { value: '' , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    fuelEconomy: { value: 0 , type: 1, col:5},
    loadingCapacity: { value: 0 , type: 1, col:5},
    distanceTravelled: { value: 0 , type: 1, col:5},
    wasteTons: { value: 0 , type: 1, col:5},
    subContractorName: { value: '' , type: 4, col: 3},
    entryId: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
    waste: { value: '' , type: 4, col: 3},
    fuel: { value: '' , type: 4, col: 3},
    vehicle: { value: '' , type: 4, col: 3},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    fuelEconomy: { dir: '' },
    loadingCapacity: { dir: '' },
    distanceTravelled: { dir: '' },
    wasteTons: { dir: '' },
    subContractorName: { dir: '' },
    entryId: { dir: '' },
    year: { dir: '' },
    waste: { dir: '' },
    fuel: { dir: '' },
    vehicle: { dir: '' },
  }


  source: LocalDataSource = new LocalDataSource();

  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private dialog: MatDialog,
              private  toastSerivce:  NbToastrService,
              private router :Router
  ) {

    if (!UserState.getInstance().existActiveProject) {
      // this.router.navigate(['/pages/com_admin/reports']);
    }else {


    }
    this.loadData()

  }


  ngOnInit() {

    this.mySetting.actions = this.initActions()
    this.settings = Object.assign({}, this.mySetting);
  }

  initActions():any {

    if (UserState.getInstance().projectStatus == 4) { //data entry
      const entitle = UserState.getInstance().pageRoutes.filter(v => {
        let page = v.split('-')[0];
        if (+page === 9) {
          return v;
        }
      })[0];
      if (entitle !== undefined) {
        const add = entitle.split('-')[1] === '1'? true: false;
        const edit = entitle.split('-')[2] === '1'? true: false;
        const del = entitle.split('-')[3] === '1'? true: false;
        return {
          add: add,
          edit: edit,
          delete: del,
        }

      }
    }else {
      return {
        add: false,
        edit: false,
        delete: false,
      }
    }



  }


  private loadData() {
    let jsonBody   = {
      PAGE_NUMBER: this.pg_current,
      FILTER_MODEL: {
        companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListWasteTransportEntry,
      jsonBody
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    // if (data.branch === undefined || data.branch === '' ){
    //   return false;
    // }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    if (data.wasteType === undefined || data.wasteType === '' ){
      return false;
    }
    if (data.subContractor === undefined || data.subContractor === '' ){
      return false;
    }
    if (data.vehicleType === undefined || data.vehicleType === '' ){
      return false;
    }
    if (data.fuelType === undefined || data.fuelType === '' ){
      return false;
    }
    if (data.distance === undefined || data.distance === '' ){
      return false;
    }
    if (data.fuelEconomy === undefined || data.fuelEconomy === '' ){
      return false;
    }
    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    }
    if (data.loadingCapacity === undefined || data.loadingCapacity == "") {
      //show snack bar
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }

    return true;
  }



  onUpdate(event: any, isNew:boolean) {
    console.log(event);
    const dialogRef = this.dialog.open(DataEntryUserUpdateWasteTransportComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '600px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let wasteType;
    let subContractor;
    let vehicleType
    let fuelType;
    let distance;
    let fuelEconomy;
    let month;
    let year;
    let id;
    let loadingCapacity;
    // this.masterData.getBranchName(dto.BRANCH_ID).subscribe(data => { branch = data; }).unsubscribe();
    // this.masterData.getA
    this.masterData.getCompanyName(dto.COMPANY_ID).subscribe(data => { company = data; }).unsubscribe();
    this.masterData.getMonthName(dto.MONTH).subscribe(data => { month = data; }).unsubscribe();
    this.masterData.getVehicleTypeName(dto.VEHICLE_TYPE).subscribe(data => { vehicleType = data;}).unsubscribe();
    this.masterData.getWastTypeName(dto.WASTE_TYPE).subscribe(data => { wasteType = data;}).unsubscribe();
    this.masterData.getFuelTypeName(dto.FUEL_TYPE).subscribe(data=> { fuelType = data;}).unsubscribe();
    year = dto.YEAR;
    id = dto.WASTE_TRANSPORT_ENTRY_ID;
    fuelEconomy = dto.FUEL_ECONOMY;
    loadingCapacity = dto.LOADING_CAPACITY;
    distance = dto.DISTANCE_TRAVELLED;
    subContractor = dto.SUB_CONTRACTOR_NAME;

    return {
      id,
      idCom: dto.COMPANY_ID,
      idBran: dto.BRANCH_ID,
      idWaste:dto.WASTE_TYPE ,
      idVehicle: dto.VEHICLE_TYPE,
      idFuel: dto.FUEL_TYPE,
      idMon: dto.MONTH,
      wasteType,
      vehicleType,
      fuelType,
      company: UserState.getInstance().companyName,
      branch: dto.branch,
      loadingCapacity,
      fuelEconomy,
      distance,
      subContractor,
      year,
      month,
      wasteTons: dto.WASTE_TONS,
      noOfTurns: dto.noOfTurns,
      emission: isNaN(+dto.EMISSION_INFO.tco2) ? 0.0 : (+dto.EMISSION_INFO.tco2).toFixed(5) ,
      emission_info: dto.EMISSION_INFO,
    };



  }


  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWasteTransportEntry,
          {
            DATA: {
              WASTE_TRANSPORT_ENTRY_ID: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }

  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.entryId.value = $event.query.query;
        break;
      } case 'distance':  {
        this.filterModel.distanceTravelled.value = $event.query.query;
        break;
      } case 'fuelType':  {
        this.filterModel.fuel.value = $event.query.query;
        break;
      } case 'loadingCapacity':  {
        this.filterModel.loadingCapacity.value = $event.query.query;
        break;
      }  case 'vehicleType':  {
        this.filterModel.vehicle.value = $event.query.query;
        break;
      } case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      }  case 'fuelEconomy':  {
        this.filterModel.fuelEconomy.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      } case 'wasteType':  {
        this.filterModel.waste.value = $event.query.query;
        break;
      } case 'subContractor':  {
        this.filterModel.subContractorName.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      fuelEconomy: { dir: '' },
      loadingCapacity: { dir: '' },
      distanceTravelled: { dir: '' },
      wasteTons: { dir: '' },
      subContractorName: { dir: '' },
      entryId: { dir: '' },
      year: { dir: '' },
      waste: { dir: '' },
      fuel: { dir: '' },
      vehicle: { dir: '' },
    };
    console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.entryId.dir = $event.direction;
        break;
      } case 'distance':  {
        this.sortModel.distanceTravelled.dir = $event.direction;
        break;
      } case 'fuelType':  {
        this.sortModel.fuel.dir = $event.direction;
        break;
      }  case 'vehicleType':  {
        this.sortModel.vehicle.dir = $event.direction;
        break;
      }  case 'loadingCapacity':  {
        this.sortModel.loadingCapacity.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'fuelEconomy':  {
        this.sortModel.fuelEconomy.dir = $event.direction;
        break;
      } case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      } case 'wasteType':  {
        this.sortModel.waste.dir = $event.direction;
        break;
      }case 'subContractor':  {
        this.sortModel.subContractorName.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {

        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          console.log(k)
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWasteTransportEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWasteTransportEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWasteTransportEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWasteTransportEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }
  }

  onShowFormula($event: any) {
    if (UserState.getInstance().disableFormulas) {
      return;
    }
    const dialogRef = this.dialog.open(WastTransFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }
}
