import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuAshTransComponent } from './deu-ash-trans.component';

describe('DeuAshTransComponent', () => {
  let component: DeuAshTransComponent;
  let fixture: ComponentFixture<DeuAshTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuAshTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuAshTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
