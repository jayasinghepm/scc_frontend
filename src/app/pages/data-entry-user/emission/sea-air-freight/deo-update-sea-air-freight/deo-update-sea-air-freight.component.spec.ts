import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeoUpdateSeaAirFreightComponent } from './deo-update-sea-air-freight.component';

describe('DeoUpdateSeaAirFreightComponent', () => {
  let component: DeoUpdateSeaAirFreightComponent;
  let fixture: ComponentFixture<DeoUpdateSeaAirFreightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeoUpdateSeaAirFreightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeoUpdateSeaAirFreightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
