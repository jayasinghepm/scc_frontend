import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuStaffTransComponent } from './update-deu-staff-trans.component';

describe('UpdateDeuStaffTransComponent', () => {
  let component: UpdateDeuStaffTransComponent;
  let fixture: ComponentFixture<UpdateDeuStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
