import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {MatSelectChange} from "@angular/material/select";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-biomass',
  templateUrl: './update-biomass.component.html',
  styleUrls: ['./update-biomass.component.scss']
})
export class UpdateBiomassComponent implements OnInit {

  public isSaved = false;

  public  jsonBody = {
    entryId: -1,
    branchId :undefined,
    companyId :undefined,
    month :undefined,
    year :undefined,
    biomassType :undefined, // 1-saw dust , 2 - wchip , 3 -
    quantity :undefined, //km
    company :undefined,
    branch :undefined,
    mon :undefined,
    bioMassTypeText :undefined,

  };



  public months = [];
  public companies = []
  public years = [];
  public branches = [];
  public biomassTypes = [];




  constructor(
    public dialogRef: MatDialogRef<UpdateBiomassComponent>,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private boService: BackendService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {
    this.init(this.popupData.data, this.popupData.isNew);

  }


  ngOnInit() {
  }

  private init(data: any, isNew:boolean) {
    this.jsonBody.companyId = UserState.getInstance().companyId;
    console.log(this.jsonBody.companyId )
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(d => {return d.id === UserState.getInstance().companyId})[0].name;
    })
    this.masterData.getYearsForCompany(this.jsonBody.companyId ).subscribe(d => {
      console.log(d);
      this.years = d;
    })
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d;
    //
    //   console.log(d);
    // })
    //----------------------------branch search ---------------------
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => {
    //   this.branches = d;
    //   console.log(d);
    // })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });

    this.masterData.getBiomassTypesFull().subscribe(data => {
      this.biomassTypes = data;
    })


    this.branches.push({
      id: 0, name: 'Search Branch'
    })

    if (!isNew) {
      this.jsonBody.companyId = data.idCom;
      this.jsonBody.entryId = data.entryId ;
      this.jsonBody.branchId = data.idBran ;
      this.jsonBody.quantity = data.quantity ;

      this.jsonBody.biomassType = data.biomassType
      this.jsonBody.bioMassTypeText= data.bioMassTypeText;


      this.jsonBody.year = data.year ;
      this.jsonBody.month = data.idMon ;
      // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
        console.log(d);
        this.years = d;
      })

      this.branches.push({
        id: data.idBran, name: data.branch
      })
      this.companies.push({
        id: data.idCom, name: data.company
      })

      console.log(this.jsonBody)

    }

  }

  public selectedComId = 0;
  onChangeCompany(value: any) {
    this.selectedComId = value;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    this.jsonBody.company = this.companies.filter(c => c.id === value)[0].name

    // this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.selectedComId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  private validateEntry(onEdit: boolean): boolean {
    console.log(this.jsonBody)
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0 ){
    //   return false;
    // }
    if (onEdit && (this.jsonBody.companyId === undefined || this.jsonBody.companyId === '') ){
      return false;
    }

    if (this.jsonBody.quantity === undefined || this.jsonBody.quantity === '' ){
      return false;
    }



    if (this.jsonBody.year === undefined || this.jsonBody.year == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.month=== undefined|| this.jsonBody.month < 0) {
      console.log(this.jsonBody.month)
      //show snack bar
      return false;
    }
    if (this.jsonBody.biomassType === undefined || this.jsonBody.biomassType === '') {
      return false;
    }
    if (onEdit && (this.jsonBody.entryId === undefined || this.jsonBody.entryId <=0)) {
      //show snack bar
      return false;
    }
    return true;
  }

  public onClickSave() {
    // // update
    if (!this.popupData.isNew) {
      if (this.validateEntry( true)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageBioMassEntry,
          { DATA: this.jsonBody }
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })

          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageBioMassEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }

  onChangeBioMassType($event: MatSelectChange) {
    this.jsonBody.bioMassTypeText = this.biomassTypes.filter(m => m.id === $event.value)[0].name;
  }


}
