import { Component, OnInit } from '@angular/core';
import {UserState} from "../../../@core/auth/UserState";
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {Row, Workbook, Worksheet} from "exceljs";
import * as fs from "file-saver";
import { CadminGeneralInfoComponent } from '../../cadmin/cadmin-general-information/cadmin-general-info-component';
import { ClientEmpCommuComponent } from '../../client/emission/client-emp-commu/client-emp-commu.component';

@Component({
  selector: 'app-deu-project-summary',
  templateUrl: './deu-project-summary.component.html',
  styleUrls: ['./deu-project-summary.component.scss']
})
export class DeuProjectSummaryComponent implements OnInit {


  public companyBasicInfo = {
    name: '',
    sector: '',
    code: '',
    noOfEmployees: 0,
    noOfBranches: 0,
    addr1: '',
    addr2: '',
    district: '',

  }


  public userType = UserState.getInstance().userType;

  public inputExcludedReason  = {
    reason: '',
    srcName: '',
  }

  public inputSuggestion = {
    title: '',
    description: '',
  }

  public  summaryData = {

    companyId: 1,

    projectId : 2,

    fy: '',

    isFy: false,

    companyName: "test company",

    methodology: "ghg protocoal",

    protocol: "ghp protocol",

    totalEmission: 20.0,

    totalDirectEmission: 12.23,

    totalIndirectEmission: 8.00,
    excludedReasons: [{
      srcName: 'src 1',
      reason: 'reason 1',
    }],

    suggestions: [{
      title: 'title 1',
      description: 'desc 1',
    }],

    directSources: [
      {

        srcName: 'src 1',
        srcId: 1,

        emissionFactors: [{
          name: '',
          value: 2,
          unit: 1,
          unitStr: 'liters'
        }],


        activityData: [
          {
            metricsName: 'act data 1',
            values: [0, 12, 2, 232, 1, 23, 2, 3, 2, 3, 2, 2,2],
            unit: 1,
            unitStr: 'liters',
          }
        ],


        sourceOfData : '',

        emission: 2,

        

      }
    ],

    indirectSources: [
      {

        srcName: 'src 1',
        srcId: 1,

        emissionFactors: [{
          name: '',
          value: 2,
          unit: 1,
          unitStr: 'liters'
        }],


        activityData: [
          {
            metricsName: 'act data 1',
            values: [0, 12, 2, 232, 1, 23, 2, 3, 2, 3, 2, 2,2],
            unit: 1,
            unitStr: 'liters',
          }
        ],


        sourceOfData : '',

        emission: 2,

      }
    ],

    unCategorized : [{

      srcName: 'src 1',
      srcId: 1,

      emissionFactors: [{
        name: '',
        value: 2,
        unit: 1,
        unitStr: 'liters'
      }],


      activityData: [
        {
          metricsName: 'act data 1',
          values: [0, 12, 2, 232, 1, 23, 2, 3, 2, 3, 2, 2,2],
          unit: 1,
          unitStr: 'liters',
        }
      ],


      sourceOfData : '',

      emission: 2,

    }]
  }

  public companies: {
    id: number,
    name: '',
    pages: [],
    emissionCategories: [],
    allowedEmissionSources: any,
    emSources: []
  } [] = [];
  public companyId  = 0;
  noOfEmployees: any;
  pg_current: any;
  currentEmployee: any;
  DE: number=0;
  IE:number=0;
  tIndirectEmision: number =0;
  tdirectEmision: number = 0;
  TE: number =0;
  TDIE:number=0;


  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private toastSerivce: NbToastrService,
              private einfo:CadminGeneralInfoComponent,
              private cemployees:ClientEmpCommuComponent) {

    this.masterData.loadCompaniesDtos().subscribe(data => {
      this.companies = data;
    });
  }

  ngOnInit() {
    console.log("FFFFF",this.cemployees.totalCount)
    ///----------------------
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      {
        isLogoRequired: 1,
        FILTER_MODEL: {
          id: { value: UserState.getInstance().companyId , type: 1, col: 1}
        },
        PAGE_NUM: 1,
      }
    ).then(data => {

      console.log("DDDDD", data.DAT.list[0].noOfEmployees)
  
      this.noOfEmployees = data.DAT.list[0].noOfEmployees;
    });
  // this.cemployees.loadData();
  let jsonBody   = {
    PAGE_NUMBER: this.pg_current,
    FILTER_MODEL: {
      companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
    }
  }
  this.boService.sendRequestToBackend(
    RequestGroup.Emission,
    RequestType.ListEmpCommuting,
    jsonBody
  ).then(data => {
    

    this.currentEmployee = data.DAT.TOTAL_COUNT;
    console.log("HHHHH",this.currentEmployee)
  });


    this.loadData(UserState.getInstance().companyId)
    var x = this.loadData(this.companyId)

    this.einfo.loadCompanyInfo();
    //console.log("CBIaaaa "+ companyBasicInfo.noOfEmployees);
 // console.log(x)
    }

      //Pasindu added
    //   changetEmission($event) {
    // if (this.jsonBody.noemployees == 0) {
    //   this.jsonBody.noemployees = 0;
    //   return;
    // }
    // this.jsonBody.totalemission = this.jsonBody.noemployees / 65
    // }


  onChangeCompany(option: any) {
    // const company = this.companines.find(com => com.id == value);
    // console.log(company)

    //todo request data
    console.log("testing pasindu"+option)
    this.loadData(option.value)

  }

  convertActDataOrderForFy(values:number[]) : number[] {
    //do swapping oct,sep
    const janIndex = 0;
    const aprIndex = 0;
    const decIndex = 11;
    const octIdex = 8;
    const sepIndex = 9;
    const octValue = values[octIdex];
    values[octIdex] = values[sepIndex];
    values[sepIndex]  = octValue;

    const fyValues = [...values.slice(3,12), ...values.slice(0,3), values[12]]
    return fyValues;
  }

  convertSummaryForFy(summary: any) : any{
    if (!summary) {
      return {};
    }

    if (summary.unCategorized) {
      for (const actData of summary.unCategorized) {
        if (actData.activityData) {
          for (const metric of actData.activityData) {
            metric.values = this.convertActDataOrderForFy(metric.values );
          }
        }
      }
    }
    if (summary.indirectSources) {
      for (const actData of summary.indirectSources) {
        if (actData.activityData) {
          for (const metric of actData.activityData) {
            metric.values = this.convertActDataOrderForFy(metric.values );
          }
        }
      }
    }
    if (summary.directSources) {
      for (const actData of summary.directSources) {
        if (actData.activityData) {
          for (const metric of actData.activityData) {
            metric.values = this.convertActDataOrderForFy(metric.values );
          }
        }
      }
    }
    return summary;
  }

  loadData (companyId: number) {
    this.boService.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.GetProjectSummary, {
      projectId: 0,
      companyId: companyId,
    }).then(
      data => {
        console.log("DATA ",data);


        if (data !== undefined && data.DAT !== undefined && data.DAT.summary !== undefined) {

          if (data.DAT.summary.isFy) {
            this.summaryData = this.convertSummaryForFy(data.DAT.summary)
            console.log("DATA-2 ",data.DAT.summary)

          }else {
            console.log("DATA 3 ",data.DAT.summary)
            this.summaryData = data.DAT.summary;
            // let IE:number =0;
            // let DE:number =0;
            // let TE:number =0;
            //not paid
            for(let e in this.summaryData.indirectSources){
             console.log("TTT "+this.summaryData.indirectSources[e].srcName);
               if(this.summaryData.indirectSources[e].srcName.includes("Employee Commuting, Not paid by the company")){
                 this.IE = this.summaryData.indirectSources[e].emission;
                  console.log("EC Not paid Indirect Emission111 ",this.IE)

               }
               else{
                console.log("NO")
               }

          
            }

            //paid
            for(let e in this.summaryData.directSources){
               console.log("YYY "+this.summaryData.directSources[e].srcName);
               if(this.summaryData.directSources[e].srcName.includes("Employee Commuting, Paid by the company")){
                this. DE = this.summaryData.directSources[e].emission;
                //  console.log("EC Not paid Indirect Emission "+IE)

               }
               else{
                console.log("NO")
               }

          
            }
            this.TE = this.DE+this.IE

           this.tIndirectEmision = (this.IE/405)*this.noOfEmployees;
           this.tdirectEmision=  (this.DE/405)*this.noOfEmployees;
           this.TDIE = this.tIndirectEmision+this.tdirectEmision;

           console.log("EC Not paid Indirect Emission "+this.tIndirectEmision)
            console.log("EC paid direct Emission "+this.tdirectEmision)
            console.log("EC Paid Direct Emission "+this.DE)

            console.log("RRRRRR "+this.noOfEmployees)

           
            


            
          }




        }
      }
    )
  }

  updateSuggestionsAndExcludedReasons() {
    this.boService.sendRequestToBackend(RequestGroup.Institution, RequestType.UpdateCompanySuggestionsAndExcludedReasons, {
      companyId: this.companyId,
      excludedReasons: this.summaryData.excludedReasons,
      suggestions: this.summaryData.suggestions,
    }).then(
      data => {


        if (data !== undefined && data.DAT !== undefined ) {


          this.toastSerivce.show('', 'Updated Successfully!', {
            status: "success",
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });


        }
      }
    )
  }

  onAddExcludedReason($event: MouseEvent) {
    if ( this.summaryData.excludedReasons == undefined ) {
      this.summaryData.excludedReasons = [];
    }else {

      if (!Boolean(this.inputExcludedReason.srcName) || !Boolean(this.inputExcludedReason.reason) ) {
        this.toastSerivce.show('', 'Source Name and Reason cannot be empty!', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }


      if (this.summaryData.excludedReasons.find(reas => reas.srcName == this.inputExcludedReason.srcName)) {
        this.toastSerivce.show('', 'Emission Source already exists!', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;



      }

      this.summaryData.excludedReasons.push({
        reason: this.inputExcludedReason.reason,
        srcName: this.inputExcludedReason.srcName
      });
      this.inputExcludedReason.srcName = '';
      this.inputExcludedReason.reason = '';

      this.updateSuggestionsAndExcludedReasons();


    }

  }



  onDeleteExcludedSrc($event: MouseEvent, delSrc: { reason: string; srcName: string }) {
    let delIndex =  this.summaryData.excludedReasons.findIndex(src  => delSrc.srcName == src.srcName);
    if (delIndex >=0 ) {
      this.summaryData.excludedReasons.splice(delIndex ,1);


      this.updateSuggestionsAndExcludedReasons();
    }
  }

  onAddSuggestion($event: MouseEvent) {
    if (this.summaryData.suggestions == undefined) {
      this.summaryData.suggestions = [];
    } else {

      if (!Boolean(this.inputSuggestion.title) || !Boolean(this.inputSuggestion.description)) {
        this.toastSerivce.show('', 'Title and Description cannot be empty!', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }


      if (this.summaryData.suggestions.find(sugg => sugg.title == this.inputSuggestion.title)) {
        this.toastSerivce.show('', 'Suggestion already exists!', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;


      }

      this.summaryData.suggestions.push({
        title: this.inputSuggestion.title,
        description: this.inputSuggestion.description,
      });
      this.inputSuggestion.title = '';
      this.inputSuggestion.description = ''

      this.updateSuggestionsAndExcludedReasons();


    }

  }

  onDeleteSuggestion($event: MouseEvent, delSugg: {description: string; title: string}) {
    let delIndex =  this.summaryData.suggestions.findIndex(suggestion  => delSugg.title === suggestion.title);
    if (delIndex >=0 ) {
      this.summaryData.suggestions.splice(delIndex ,1);


      this.updateSuggestionsAndExcludedReasons();
    }
  }


  exportSummary($event) {

    let workbook = new Workbook();
    let summaryWS = workbook.addWorksheet("Summary");
    this.addWorkSheetByEmissionSource(workbook);

    this.writeSummaryWS(summaryWS);

    let wsIndex = 2;
    //direct emission sources
    for (const emissionSrc of this.summaryData.directSources) {
      const wsName = this.getWorkBookNameBySrc("direct", emissionSrc.srcName);
      console.log("jhjhjhjh")
      const emSrcWS  = workbook.getWorksheet(wsIndex);

      console.log(emSrcWS)
      console.log(wsName)
      this.writeEmissionSourceWS(emSrcWS, emissionSrc, this.summaryData.isFy)

      wsIndex++;
    }

    //direct emission sources
    for (const emissionSrc of this.summaryData.indirectSources) {
      const wsName = this.getWorkBookNameBySrc("indirect", emissionSrc.srcName);
      const emSrcWS  = workbook.getWorksheet(wsIndex);
      this.writeEmissionSourceWS(emSrcWS, emissionSrc, this.summaryData.isFy)

      wsIndex++
    }

    //uncategorized
    for (const emissionSrc of this.summaryData.unCategorized) {
      const wsName = this.getWorkBookNameBySrc("uncat", emissionSrc.srcName);
      const emSrcWS  = workbook.getWorksheet(wsIndex);
      this.writeEmissionSourceWS(emSrcWS, emissionSrc, this.summaryData.isFy)

      wsIndex++
    }

    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, "summary"+'-'+new Date().valueOf()+'.xlsx');
    });

  }

  writeSummaryWS(summaryWS : Worksheet) {
    //company
    const boundaryData = ['Boundary',  this.summaryData.companyName];
    summaryWS.addRow([]) //offset
    summaryWS.addRow([]) //offset
    summaryWS.addRow([]) //offset
    summaryWS.addRow(boundaryData) //todo styling

    //year
    const yearData = ['Year' , this.summaryData.fy]
    summaryWS.addRow([]) //offset
    summaryWS.addRow(yearData);


    //method
    const methData = ['Methodology', this.summaryData.methodology];
    summaryWS.addRow([]) //offset
    summaryWS.addRow(methData);

    //approach
    const approachData = ['Approach', this.summaryData.protocol];
    summaryWS.addRow([]) //offset
    summaryWS.addRow(approachData);

    //talbe title
    summaryWS.addRow([]) //offset
    const tableTitle = ['Information related to emission sources ']
    summaryWS.addRow(tableTitle);


    //table
    summaryWS.addRow([]) //offset
    //table header
    const tableHeaderData = ['Category', 'Emission Source', 'Emission']
    const tableHeaderRow = summaryWS.addRow(tableHeaderData);

    const directData = this.getSummaryTableDataByCategory('direct');
    const indirectData = this.getSummaryTableDataByCategory('indirect');
    const uncatData = this.getSummaryTableDataByCategory('uncat');

    let directDataFirstRow: Row = null;
    let indirectDataFirstRow: Row = null;
    let uncatDataFirstRow = null;

    for (let index = 0 ; index < directData.length; index++) {
      if (index == 0) {
        directDataFirstRow = summaryWS.addRow(directData[index]);
        continue;
      }
      summaryWS.addRow(directData[index])
    }
    for(let index = 0; index < indirectData.length; index++) {
      if (index == 0) {
        indirectDataFirstRow = summaryWS.addRow(indirectData[index]);
        continue;
      }
      summaryWS.addRow(indirectData[index])
    }
    for (let index = 0 ; index< uncatData.length; index++) {
      if (index == 0) {
        uncatDataFirstRow = summaryWS.addRow(uncatData[index]);
        continue;
      }
      summaryWS.addRow(uncatData[index])
    }

    let catColIndex = 1;

    if (directDataFirstRow != null ){
      const directDataLastCell = summaryWS.getRow(directDataFirstRow.number + directData.length).getCell( catColIndex);
      const directDataFirstCell = directDataFirstRow.getCell(catColIndex);

      summaryWS.mergeCells(directDataFirstCell.fullAddress + ":" + directDataLastCell.fullAddress);

      directDataFirstCell.value = "Direct";

    }

    if (indirectDataFirstRow != null ){
      const indirectDataLastCell = summaryWS.getRow(indirectDataFirstRow.number + indirectData.length).getCell( catColIndex);
      const indirectDataFirstCell = indirectDataFirstRow.getCell(catColIndex);

      // summaryWS.mergeCells(indirectDataFirstCell.fullAddress + ":" + indirectDataLastCell.fullAddress);

      indirectDataFirstCell.value = "Indirect";

    }




  }

  writeEmissionSourceWS(ws : Worksheet, emissionSrc, isFy:boolean) {
    //Emission src name and emission
    ws.addRow([])
    ws.addRow([])
    ws.addRow([])
    ws.addRow([emissionSrc.srcName + "(tCO2e)", emissionSrc.emission]);

    //act data header  data/month
    ws.addRow([])
    ws.addRow([])

    const actDataHeader = isFy ? ['Data/Month', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Not Specified'] :   ['Data/Month', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Not Specified'] ;
    ws.addRow(actDataHeader);
    //act data for loop
    for (let index = 0 ; index < emissionSrc.activityData.length; index++){
      // const actData = [emissionSrc.activityData[index].metricsName + "(" + emissionSrc.activityData[index].unitStr + ")"];
      ws.addRow([emissionSrc.activityData[index].metricsName + "(" +emissionSrc.activityData[index].unitStr + ")" , ...emissionSrc.activityData[index].values])
    }


    //emission factors title
    ws.addRow([])
    ws.addRow([])
    ws.addRow(['Emission Factors'])


    //emission factor table header
    ws.addRow(['Emission Factor', 'Value'])
    for(let index =0 ; index < emissionSrc.emissionFactors.length; index++) {
      ws.addRow([emissionSrc.emissionFactors[index].name + "(" + emissionSrc.emissionFactors[index].unitStr + ")", emissionSrc.emissionFactors[index].value])
    }

    //emission


  }

  getSummaryTableDataByCategory(cat: 'direct' |'indirect' | 'uncat') :(  any[])[]{

    console.log("VVVVVVVVVVVV");
    const data = [];
    if (cat === 'direct') {
      if (this.summaryData && this.summaryData.directSources) {
        this.summaryData.directSources.forEach(src => {
          console.log("TESTING "+src.srcName)
          data.push( ['', src.srcName, src.emission])
        });
      }
    }else if (cat === 'indirect') {
      if (this.summaryData && this.summaryData.indirectSources) {
        
        this.summaryData.indirectSources.forEach(src => {
          data.push( ['', src.srcName, src.emission])
          console.log("testing wwwww")

        });
      }
    }else if (cat === 'uncat') {
      if (this.summaryData && this.summaryData.unCategorized) {
        this.summaryData.unCategorized.forEach(src => {
       //   data.push( ['', src.srcName, src.emission])
        });
      }
    }
    console.log("TESTING ")

    return data;
  }




  getWorkBookNameBySrc(cat: 'direct' |'indirect' | 'uncat' , name) : string {
    if (cat === 'direct') {
      return "Direct-" + name
    }else if (cat === 'indirect') {
      return "Indirect-" + name
    }else if (cat === 'uncat') {
      return  name
    }
    return "";
  }

  addWorkSheetByEmissionSource(workbook : Workbook) {

    //direct
    if (this.summaryData && this.summaryData.directSources && this.summaryData.directSources.length > 0) {
      for (const src of this.summaryData.directSources) {
        workbook.addWorksheet(this.getWorkBookNameBySrc('direct', src.srcName));
        console.log("testing wwwww")
      }
    }

    //indirect
    if (this.summaryData && this.summaryData.indirectSources && this.summaryData.indirectSources.length > 0) {
      for (const src of this.summaryData.indirectSources) {
        workbook.addWorksheet(this.getWorkBookNameBySrc('indirect', src.srcName));
      }
    }

    //uncategorized
    if (this.summaryData && this.summaryData.unCategorized && this.summaryData.unCategorized.length > 0) {
      for (const src of this.summaryData.unCategorized) {
        workbook.addWorksheet(this.getWorkBookNameBySrc('uncat', src.srcName));
      }
    }
  }

}
