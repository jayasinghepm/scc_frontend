import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSummaryDataInputsComponent } from './client-summary-data-inputs.component';

describe('ClientSummaryDataInputsComponent', () => {
  let component: ClientSummaryDataInputsComponent;
  let fixture: ComponentFixture<ClientSummaryDataInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientSummaryDataInputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientSummaryDataInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
