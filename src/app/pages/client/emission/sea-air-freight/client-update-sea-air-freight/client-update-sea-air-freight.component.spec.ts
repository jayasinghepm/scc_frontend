import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientUpdateSeaAirFreightComponent } from './client-update-sea-air-freight.component';

describe('ClientUpdateSeaAirFreightComponent', () => {
  let component: ClientUpdateSeaAirFreightComponent;
  let fixture: ComponentFixture<ClientUpdateSeaAirFreightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientUpdateSeaAirFreightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientUpdateSeaAirFreightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
