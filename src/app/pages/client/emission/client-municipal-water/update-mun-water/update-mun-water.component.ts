import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-mun-water',
  templateUrl: './update-mun-water.component.html',
  styleUrls: ['./update-mun-water.component.scss']
})
export class ClientUpdateMunWaterComponent implements OnInit {
  private isSaved = false;

  public jsonBody = {
    MUNICIPAL_WATER_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    UNITS: undefined, // todo
    YEAR: undefined,
    MONTH: undefined,
    WATER_CONSUMPTION: undefined,
    // NUMS_OF_METERS: numOfMeters,
    METER_NO: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
  }

  public companies = [];
  public branches = [];
  public months = [];
  public years = [];
  public units = [{ id: 1, name: 'm3'}, { id: 6, name: 'LKR'}];


  constructor( public dialogRef: MatDialogRef<ClientUpdateMunWaterComponent>,
               private masterData: MassterDataService,
               private boService: BackendService,
               private toastSerivce: NbToastrService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew);
  }

  private init(data:any, isNew:boolean) {
    this.jsonBody.COMPANY_ID = UserState.getInstance().companyId;
    this.jsonBody.BRANCH_ID = UserState.getInstance().branchId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.COMPANY_ID)[0].name;
    })
    this.branches.push({id: UserState.getInstance().branchId, name: UserState.getInstance().branchName});
    this.jsonBody.branch = UserState.getInstance().branchName;
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d;
    //   this.jsonBody.branch = this.branches.filter(c => c.id === this.jsonBody.BRANCH_ID)[0].name;
    // })

    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      this.years = d;
    })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    if (!isNew) {
      this.jsonBody.MUNICIPAL_WATER_ENTRY_ID = data.id ;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.METER_NO = data.meterNo ;
      this.jsonBody.WATER_CONSUMPTION = data.consumption ;
      this.jsonBody.YEAR = data.year ;
      this.jsonBody.MONTH = data.idMon;
      this.jsonBody.UNITS = data.idUnit;
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        console.log(d);
        this.years = d;
      })
    }
  }

  private validateEntry(onEdit: boolean): boolean {
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' ){
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){
      return false;
    }
    // if (this.jsonBody.numOfMeters === undefined || this.jsonBody.numOfMeters === '' ){
    //   return false;
    // }
    if (this.jsonBody.WATER_CONSUMPTION === undefined || this.jsonBody.WATER_CONSUMPTION === '' ){
      return false;
    }
    if (this.jsonBody.UNITS === undefined || this.jsonBody.UNITS === '' ){
      return false;
    }
    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.MUNICIPAL_WATER_ENTRY_ID === undefined || this.jsonBody.MUNICIPAL_WATER_ENTRY_ID <= 0)) {
      //show snack bar
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
        if (this.validateEntry(true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageMunWaterEntry,
            { DATA: this.jsonBody}
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }

    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageMunWaterEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }


  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }


}
