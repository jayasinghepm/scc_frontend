import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSludgeTransComponent } from './client-sludge-trans.component';

describe('ClientSludgeTransComponent', () => {
  let component: ClientSludgeTransComponent;
  let fixture: ComponentFixture<ClientSludgeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientSludgeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientSludgeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
