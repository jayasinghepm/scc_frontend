import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientStaffTransComponent } from './update-client-staff-trans.component';

describe('UpdateClientStaffTransComponent', () => {
  let component: UpdateClientStaffTransComponent;
  let fixture: ComponentFixture<UpdateClientStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
