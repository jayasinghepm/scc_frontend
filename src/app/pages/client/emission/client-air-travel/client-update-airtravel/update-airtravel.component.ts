import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {UserState} from "../../../../../@core/auth/UserState";
import {MatCheckboxChange} from "@angular/material/typings/esm5/checkbox";

@Component({
  selector: 'app-update-airtravel',
  templateUrl: './update-airtravel.component.html',
  styleUrls: ['./update-airtravel.component.scss']
})
export class ClientUpdateAirtravelComponent implements OnInit {

  private isSaved = false;
  public twoWay = false;
  public jsonBody = {
    AIR_TRAVEL_ENTRY_ID: -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    EMP_ID: undefined, // todo
    TRAVEL_YEAR: undefined,
    TRAVEL_MONTH: undefined,
    DEP_AIRPORT: undefined,
    DEP_COUNTRY: undefined,
    TRANSIST_1: undefined,
    TRANSIST_2: undefined,
    TRANSIST_3: undefined,
    AIR_TRAVEL_WAY: undefined,
    SEAT_CLASS: undefined ,
    DEST_AIRPORT: undefined,
    DEST_COUNTRY: undefined,
    NO_OF_EMPLOYEES: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
    depCoun: undefined,
    destCoun: undefined,
    desPort: undefined,
    depPort: undefined,
    trans1: undefined,
    trans2: undefined,
    trans3: undefined,
    seat: undefined,
    travelWay: undefined,
  }

  public countries = [];
  public airports = [];
  public months = [];
  public years = [];
  public branches = [];
  public companies = [];
  public seatClasses = [];

  public countriesBackup = [];
  private portsAdded = [];
  private countyAdded =  [];



  constructor(
                public dialogRef: MatDialogRef<ClientUpdateAirtravelComponent>,
                private masterData: MassterDataService,
                private toastSerivce: NbToastrService,
                private boService: BackendService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,
                ) {
    console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew);
  }

  private init(data:any, isNew: boolean) {
    this.jsonBody.COMPANY_ID = UserState.getInstance().companyId;
    this.jsonBody.BRANCH_ID = UserState.getInstance().branchId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(d => {return d.id === UserState.getInstance().companyId})[0].name;
    })
    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      console.log(d);
      this.years = d;
    })
    this.masterData.getAirportsFull().subscribe(data => {
      // @ts-ignore
      this.airports.push(...data);
    });
    this.masterData.getCountriesFull().subscribe(data => {
      // @ts-ignore
      this.countries  = data;
      this.countriesBackup = data;
    });
    this.masterData.getSeatClassesFull().subscribe(data => {
     this.seatClasses = data;
    });
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.branches.push({id: UserState.getInstance().branchId, name: UserState.getInstance().branchName});
    this.jsonBody.branch = UserState.getInstance().branchName;
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d
    //   this.jsonBody.branch = this.branches.filter(b => b.id === this.jsonBody.BRANCH_ID)[0].name;
    // })


    if (!isNew) {
      this.jsonBody.AIR_TRAVEL_ENTRY_ID = data.id;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.TRAVEL_YEAR = data.year;
      this.jsonBody.TRAVEL_MONTH = data.idMon ;
      this.jsonBody.DEP_AIRPORT = data. idDepPort ;
      this.jsonBody.DEP_COUNTRY = data.idDepCou ;
      this.jsonBody.DEST_AIRPORT = data.idDesPort ;
      this.jsonBody.DEST_COUNTRY = data.idDesCou ;
      this.jsonBody.TRANSIST_1 = data.idTran1 ;
      this.jsonBody.TRANSIST_2 = data.idTrans2 ;
      this.jsonBody.TRANSIST_3 = data.idTrans3 ;
      this.jsonBody.AIR_TRAVEL_WAY = data.isTwoWay === 'No'? 1: 2 ;
      this.twoWay = this.jsonBody.AIR_TRAVEL_WAY == 2? true: false;
      this.jsonBody.SEAT_CLASS = data.idSeat ;
      this.jsonBody.NO_OF_EMPLOYEES = data.numEmps;
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        console.log(d);
        this.years = d;
      })

      this.jsonBody.trans1 = data.trans1;
      this.jsonBody.trans2 = data.trans2;
      this.jsonBody.trans3 = data.trans3;
      this.jsonBody.destCoun = data.destCoun;
      this.jsonBody.depCoun = data.depCoun;
      this.jsonBody.depPort = data.depPort;
      this.jsonBody.desPort = data.destPort;

      this.portsAdded.push(...[
        { id: this.jsonBody.DEP_AIRPORT, name: this.jsonBody.depPort},
        { id: this.jsonBody.DEST_AIRPORT, name: this.jsonBody.desPort},
      ])
      if (this.jsonBody.trans1 !== undefined) {
        this.portsAdded.push({ id: this.jsonBody.TRANSIST_1 , name: this.jsonBody.trans1})
      }
      if (this.jsonBody.trans2 !== undefined) {
        this.portsAdded.push({ id: this.jsonBody.TRANSIST_2, name: this.jsonBody.trans2})
      }
      if (this.jsonBody.trans3 !== undefined) {
        this.portsAdded.push({ id: this.jsonBody.TRANSIST_3, name: this.jsonBody.trans3})
      }
      this.airports.push(...this.portsAdded);
    }


  }

  private validateEntry(onEdit: boolean): boolean {
    if(this.twoWay) { this.jsonBody.AIR_TRAVEL_WAY = 2} else { this.jsonBody.AIR_TRAVEL_WAY = 1;}
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID == "" || this.jsonBody.BRANCH_ID === 0) {
    //   //show snack bar
    //   return false;
    // }
    // if (this.jsonBody.DEP_AIRPORT === undefined || this.jsonBody.DEP_AIRPORT == "") {
    //   //show snack bar
    //   return false;
    // }
    if (this.jsonBody.AIR_TRAVEL_WAY === undefined || this.jsonBody.AIR_TRAVEL_WAY == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.DEP_COUNTRY === undefined || this.jsonBody.DEP_COUNTRY == "") {
      //show snack bar
      return false;
    }
    // if (this.jsonBody.DEST_AIRPORT === undefined || this.jsonBody.DEST_AIRPORT == "") {
    //   //show snack bar
    //   return false;
    // }
    if (this.jsonBody.DEST_COUNTRY === undefined || this.jsonBody.DEST_COUNTRY == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.NO_OF_EMPLOYEES === undefined || this.jsonBody.NO_OF_EMPLOYEES == "") {
      //show snack bar
      return false;
    }

    if (this.jsonBody.SEAT_CLASS === undefined || this.jsonBody.SEAT_CLASS == "") {
      //show snack bar
      return false;
    }
    if(this.jsonBody.TRANSIST_1 === -1) {
      return false;
    }
    if(this.jsonBody.TRANSIST_2 === -1) {
      return false;
    }
    if(this.jsonBody.TRANSIST_3 === -1) {
      return false;
    }
    if (this.jsonBody.DEP_AIRPORT === undefined || this.jsonBody.DEP_AIRPORT == "" || this.jsonBody.DEP_AIRPORT === -1) {
      //show snack bar
      return false;
    }
    if (this.jsonBody.DEST_AIRPORT === undefined || this.jsonBody.DEST_AIRPORT == "" || this.jsonBody.DEST_AIRPORT === -1) {
      //show snack bar
      return false;
    }
    // if (this.jsonBody.transit2 === undefined || this.jsonBody.transit2 == "") {
    //   //show snack bar
    //   return false;
    // }
    // if (this.jsonBody.transit3 === undefined || this.jsonBody.transit3 == "") {
    //   //show snack bar
    //   return false;
    // }
    if (this.jsonBody.TRAVEL_YEAR === undefined || this.jsonBody.TRAVEL_YEAR == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.TRAVEL_MONTH === undefined || this.jsonBody.TRAVEL_MONTH < 0) {
      //show snack bar
      return false;
    }
    // if (this.jsonBody.transit1 === undefined || this.jsonBody.transit1 == "") {
    //   //show snack bar
    //   return false;
    // }
    if (onEdit && (this.jsonBody.AIR_TRAVEL_ENTRY_ID === undefined || this.jsonBody.AIR_TRAVEL_ENTRY_ID <=0)) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID <= 0) ){
      return false;
    }
    return true;

  }


  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })

  }

  public onClickSave(){
    // update
    if (!this.popupData.isNew) {

        if (this.validateEntry( true)) {
            this.boService.sendRequestToBackend(
              RequestGroup.Emission,
               RequestType.ManageAirtravelEntry,
              { DATA: this.jsonBody}
               ).then( data => {
                if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
                  this.isSaved = true;
                  this.toastSerivce.show('', 'Saved data successfully.', {
                    status: 'success',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  })
                } else {
                  this.toastSerivce.show('', 'Error in saving data.', {
                    status: 'danger',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  })
                }
               })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
    }
    // new
    else {
      if (this.validateEntry( false)) {

        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
           RequestType.ManageAirtravelEntry,
          { DATA: this.jsonBody}
           ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
           })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }

  onSearchAirport(value: any) {
    if (value === '') {
      return;
    }
    this.airports = []
    this.airports.push(...this.portsAdded)
    this.boService.sendRequestToBackend(
      RequestGroup.MasterData,
      RequestType.ListAirports,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          searchName: { value: value , type: 4, col: 3}
        },
      }
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          // const set = new Set(this.airports);
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.airports.unshift({id: val.id, name: val.name});
            }
          });
          if (this.airports.length === 0) {
            if (this.portsAdded.length === 0) {
              this.airports.push({
                id: -1, name: 'No Matches Found'
              })
            }else {
              this.airports.push(...JSON.parse(JSON.stringify(this.portsAdded)));
            }
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  onChangePort($event: MatSelectChange, field: number) {
    switch(field) {
      case 1: {
        // this.portsAdded .push({id: this.jsonBody.DEP_AIRPORT , name:$event.value })
        this.jsonBody.DEP_AIRPORT = this.airports.filter(a => a.name ===  $event.value)[0].id;
        this.portsAdded .push({id: this.jsonBody.DEP_AIRPORT , name:$event.value })
        // this.jsonBody.depPort = $event.value.name;
        break;
      }
      case 2: {
        this.jsonBody.DEST_AIRPORT = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.desPort  = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.DEST_AIRPORT , name:$event.value })
        break;
      }
      case 3: {
        this.jsonBody.TRANSIST_1 = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.trans1 = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.TRANSIST_1 , name:$event.value })
        break;
      }
      case 4: {
        this.jsonBody.TRANSIST_2 = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.trans2 = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.TRANSIST_2 , name:$event.value })
        break;
      }
      case 5: {
        this.jsonBody.TRANSIST_3 = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.trans3 = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.TRANSIST_3 , name:$event.value })
        break;
      }
    }
    console.log(this.jsonBody)
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  public country;
  onSearchCountry(value: any) {
    if (value === '') {
      this.countries = JSON.parse(JSON.stringify(this.countriesBackup));
    }
    this.countries = [];
    this.countries.push(... JSON.parse(JSON.stringify(this.countyAdded)))
    this.countries.push(...JSON.parse(JSON.stringify(this.countriesBackup.filter(c => c.name.toLowerCase().indexOf(value.toLowerCase()) > -1))));
    if (this.countries.length === 0) {
      this.countries = JSON.parse(JSON.stringify(this.countriesBackup));
      // this.country = ''
    }
    console.log(this.countyAdded)
  }
  onChangeCountry($event: MatSelectChange, field: number) {
    switch(field) {
      case 1: {
        this.jsonBody.DEP_COUNTRY = this.countriesBackup.filter(c => c.name === $event.value)[0].id;
        // this.jsonBody.depCoun = $event.value.name;
        this.countyAdded.push({id: this.jsonBody.DEP_COUNTRY, name: $event.value});
        break;
      }
      case 2: {
        this.jsonBody.DEST_COUNTRY = this.countriesBackup.filter(c => c.name === $event.value)[0].id;
        // this.jsonBody.destCoun = $event.value.name;
        this.countyAdded.push({id: this.jsonBody.DEST_COUNTRY, name: $event.value});
        break;
      }
    }
  }


  onChangeSeat($event: MatSelectChange) {
    this.jsonBody.seat = this.seatClasses.filter(c => c.id == $event.value)[0].name;
  }
  onChangeWay($event: MatCheckboxChange) {
    this.jsonBody.travelWay = $event.checked ? "Yes": "No";
  }

  onSearchPortClose($event: KeyboardEvent) {
    if (this.airports.length === 0) {
      this.airports.push(...this.portsAdded);
    }
  }

}
