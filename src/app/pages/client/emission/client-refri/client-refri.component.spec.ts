import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientRefriComponent } from './client-refri.component';

describe('ClientRefriComponent', () => {
  let component: ClientRefriComponent;
  let fixture: ComponentFixture<ClientRefriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientRefriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientRefriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
