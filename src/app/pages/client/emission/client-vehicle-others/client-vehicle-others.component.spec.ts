import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientVehicleOthersComponent } from './client-vehicle-others.component';

describe('ClientVehicleOthersComponent', () => {
  let component: ClientVehicleOthersComponent;
  let fixture: ComponentFixture<ClientVehicleOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientVehicleOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientVehicleOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
