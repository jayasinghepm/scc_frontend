import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpateClientVehicleOthersComponent } from './upate-client-vehicle-others.component';

describe('UpateClientVehicleOthersComponent', () => {
  let component: UpateClientVehicleOthersComponent;
  let fixture: ComponentFixture<UpateClientVehicleOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpateClientVehicleOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpateClientVehicleOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
