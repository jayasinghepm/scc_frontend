import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientFurnaceOilComponent } from './client-furnace-oil.component';

describe('ClientFurnaceOilComponent', () => {
  let component: ClientFurnaceOilComponent;
  let fixture: ComponentFixture<ClientFurnaceOilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFurnaceOilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFurnaceOilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
