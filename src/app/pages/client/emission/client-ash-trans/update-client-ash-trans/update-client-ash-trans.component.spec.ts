import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientAshTransComponent } from './update-client-ash-trans.component';

describe('UpdateClientAshTransComponent', () => {
  let component: UpdateClientAshTransComponent;
  let fixture: ComponentFixture<UpdateClientAshTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientAshTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientAshTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
