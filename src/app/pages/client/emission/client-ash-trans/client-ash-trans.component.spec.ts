import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAshTransComponent } from './client-ash-trans.component';

describe('ClientAshTransComponent', () => {
  let component: ClientAshTransComponent;
  let fixture: ComponentFixture<ClientAshTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAshTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAshTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
