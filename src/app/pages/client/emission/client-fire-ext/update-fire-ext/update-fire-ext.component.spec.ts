import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFireExtComponent } from './update-fire-ext.component';

describe('UpdateFireExtComponent', () => {
  let component: UpdateFireExtComponent;
  let fixture: ComponentFixture<UpdateFireExtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFireExtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFireExtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
