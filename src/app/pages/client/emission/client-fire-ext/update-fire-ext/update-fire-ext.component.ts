import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-fire-ext',
  templateUrl: './update-fire-ext.component.html',
  styleUrls: ['./update-fire-ext.component.scss']
})
export class ClientUpdateFireExtComponent implements OnInit {
  public isSaved = false;

  public  jsonBody = {
    FIRE_EXT_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    YEAR: undefined,
    MONTH: undefined,
    WEIGHT: undefined,
    AMOUNT_REFILLED: undefined,
    FIRE_EXT_TYPE: undefined,
    NO_OF_TANKS: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
    fireExt: undefined,
  };



  public months = [];
  public companies = []
  public years = [];
  public branches = [];
  public fireExtTypes = [];


  constructor(
    public dialogRef: MatDialogRef<ClientUpdateFireExtComponent>,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private boService: BackendService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {
    console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew);

  }

  private init(data: any, isNew:boolean) {
    this.jsonBody.COMPANY_ID = UserState.getInstance().companyId;
    this.jsonBody.BRANCH_ID = UserState.getInstance().branchId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.COMPANY_ID)[0].name;
    })
    this.branches.push({id: UserState.getInstance().branchId, name: UserState.getInstance().branchName});
    this.jsonBody.branch = UserState.getInstance().branchName;
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d;
    //   this.jsonBody.branch = this.branches.filter(b => b.id === this.jsonBody.BRANCH_ID)[0].name;
    // })

    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      this.years = d;
    })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getFireExtTypesFull().subscribe(data => {
      this.fireExtTypes = data;
    })
    if (!isNew) {
      this.jsonBody.FIRE_EXT_ENTRY_ID = data.id ;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.WEIGHT = data.weight ;
      this.jsonBody.AMOUNT_REFILLED = data.amountRefilled ;
      this.jsonBody.FIRE_EXT_TYPE = data.idFireExt ;
      this.jsonBody.YEAR = data.year ;
      this.jsonBody.MONTH = data.idMon ;
      this.jsonBody.NO_OF_TANKS = data.noOfTanks ;
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        console.log(d);
        this.years = d;
      })

    }

  }

  private validateEntry(onEdit: boolean): boolean {
    console.log(this.jsonBody)
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' ){
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){
      return false;
    }
    if (this.jsonBody.FIRE_EXT_TYPE === undefined || this.jsonBody.FIRE_EXT_TYPE === '' ){
      return false;
    }
    if (this.jsonBody.WEIGHT === undefined || this.jsonBody.WEIGHT === '' ){
      return false;
    }
    // if (this.jsonBody.amountRefilled === undefined || this.jsonBody.amountRefilled === '' ){
    //   return false;
    // }
    if (this.jsonBody.NO_OF_TANKS === undefined || this.jsonBody.NO_OF_TANKS === '' ){
      return false;
    }
    // if (this.jsonBody.units === undefined || this.jsonBody.units === '' ){
    //   return false;
    // }


    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.MONTH === undefined|| this.jsonBody.MONTH < 0) {
      console.log(this.jsonBody.MONTH)
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.FIRE_EXT_ENTRY_ID === undefined || this.jsonBody.FIRE_EXT_ENTRY_ID <=0)) {
      //show snack bar
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  public onClickSave() {
    // // update
    if (!this.popupData.isNew) {
        if (this.validateEntry( true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageFireExtEntry,
            { DATA: this.jsonBody }
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })

            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageFireExtEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }



  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  onChangeFireExt($event: MatSelectChange) {
    this.jsonBody.fireExt = this.fireExtTypes.filter(f => f.id === $event.value)[0].name;
  }
}
