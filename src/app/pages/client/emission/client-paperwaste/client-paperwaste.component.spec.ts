import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPaperwasteComponent } from './client-paperwaste.component';

describe('ClientPaperwasteComponent', () => {
  let component: ClientPaperwasteComponent;
  let fixture: ComponentFixture<ClientPaperwasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientPaperwasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientPaperwasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
