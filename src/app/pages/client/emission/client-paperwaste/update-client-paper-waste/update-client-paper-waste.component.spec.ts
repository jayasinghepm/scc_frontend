import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientPaperWasteComponent } from './update-client-paper-waste.component';

describe('UpdateClientPaperWasteComponent', () => {
  let component: UpdateClientPaperWasteComponent;
  let fixture: ComponentFixture<UpdateClientPaperWasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientPaperWasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientPaperWasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
