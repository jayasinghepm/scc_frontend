import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSawDustTransComponent } from './client-saw-dust-trans.component';

describe('ClientSawDustTransComponent', () => {
  let component: ClientSawDustTransComponent;
  let fixture: ComponentFixture<ClientSawDustTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientSawDustTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientSawDustTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
