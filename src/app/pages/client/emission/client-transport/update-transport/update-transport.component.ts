import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-transport',
  templateUrl: './update-transport.component.html',
  styleUrls: ['./update-transport.component.scss']
})
export class ClientUpdateTransportComponent implements OnInit {

  private isSaved = false;

  public jsonBody = {
    VEHICLE_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    YEAR: undefined,
    MONTH: undefined,
    UNITS: undefined,
    FUEL_TYPE: undefined,
    FUEL_CONSUMPTION: undefined,
    VEHICLE_MODEL: undefined,
    VEHICLE_CATEGORY: undefined,
    VEHICLE_NUMBER: undefined,
    IS_FUEL_PAID_BY_COMPANY: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
    model: undefined,
    category: undefined,
    fuel: undefined,
    fuelEconomy: undefined,
    distance: undefined,
  }

  public companies = [];
  public branches = [];
  public months = [];
  public years = [];
  public fuels = [];
  public vehicleModels = [];
  public vehicleCats = [];
  public units = [];

  constructor( public dialogRef: MatDialogRef<ClientUpdateTransportComponent>,
               private masterData: MassterDataService,
               private boService: BackendService,
               private toastSerivce: NbToastrService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    this.init(this.popupData.data, this.popupData.isNew);
    console.log(this.popupData)
  }

  private init(data:any, isNew:boolean) {
    this.jsonBody.COMPANY_ID = UserState.getInstance().companyId;
    this.jsonBody.BRANCH_ID = UserState.getInstance().branchId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.COMPANY_ID)[0].name;
    })
    this.branches.push({id: UserState.getInstance().branchId, name: UserState.getInstance().branchName});
    this.jsonBody.branch = UserState.getInstance().branchName;
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d;
    //   this.jsonBody.branch = this.branches.filter(b => b.id === this.jsonBody.BRANCH_ID)[0].name;
    // })

    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      this.years = d;
    })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getFuelTypesVehicleFull().subscribe(d => {
      this.fuels = d;
    })
    this.masterData.getVehicleModelsFull().subscribe(d => {
      this.vehicleModels = d;
    })
    this.masterData.getVehicleCategoriesFull().subscribe(d => {
      this.vehicleCats = d;
    })
    this.masterData.getUnitsGeneratorsFull().subscribe(d => {
      this.units =d ;
    })

    if (!isNew) {
      this.jsonBody.VEHICLE_ENTRY_ID = data.id ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.FUEL_TYPE = data.idFuel ;
      this.jsonBody.MONTH = data.idMon ;
      this.jsonBody.VEHICLE_MODEL = data.idModel ;
      this.jsonBody.VEHICLE_CATEGORY = data.idCat ;
      this.jsonBody.UNITS = data.idUnit ;
      this.jsonBody.FUEL_CONSUMPTION = data.consumption ;
      this.jsonBody.VEHICLE_NUMBER = data.vehicleNumber ;
      this.jsonBody.YEAR = data.year ;
      this.jsonBody.IS_FUEL_PAID_BY_COMPANY = data.paidByCompany === 'Yes'? true : false;
      this.jsonBody.fuelEconomy = data.fuelEconomy;
      this.jsonBody.distance = data.distance;
      console.log(this.jsonBody)
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        console.log(d);
        this.years = d;
      })
    }else {
      this.jsonBody.IS_FUEL_PAID_BY_COMPANY = false;
    }
  }

  private validateEntry(onEdit: boolean): boolean {
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' ){
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '' )){
      return false;
    }
    if (this.jsonBody.IS_FUEL_PAID_BY_COMPANY === undefined || this.jsonBody.IS_FUEL_PAID_BY_COMPANY === '' ){
      return false;
    }
    if (this.jsonBody.VEHICLE_CATEGORY === undefined || this.jsonBody.VEHICLE_CATEGORY === '' ){
      return false;
    }
    if (this.jsonBody.VEHICLE_MODEL === undefined || this.jsonBody.VEHICLE_MODEL === '' ){
      return false;
    }
    if (this.jsonBody.FUEL_TYPE === undefined || this.jsonBody.FUEL_TYPE == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.VEHICLE_NUMBER === undefined || this.jsonBody.VEHICLE_NUMBER == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      //show snack bar
      return false;
    }
    if (this.jsonBody.UNITS === undefined || this.jsonBody.UNITS == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.FUEL_CONSUMPTION === undefined || this.jsonBody.FUEL_CONSUMPTION == "") {
      //show snack bar
      return false;
    }
    if (!((this.jsonBody.fuelEconomy >0 && this.jsonBody.distance > 0 ) || (this.jsonBody.FUEL_CONSUMPTION > 0))) {
      return false;
    }

    if (onEdit && (this.jsonBody.VEHICLE_ENTRY_ID=== undefined || this.jsonBody.VEHICLE_ENTRY_ID <= 0)) {
      //show snack bar
      return false;
    }
    return true;
  }

  change($event) {
    if (this.jsonBody.fuelEconomy == 0) {
      this.jsonBody.FUEL_CONSUMPTION = 0;
    }
    this.jsonBody.FUEL_CONSUMPTION = this.jsonBody.distance /this.jsonBody.fuelEconomy;
  }

  ngOnInit() {
  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }
  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
        if (this.validateEntry(true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageVehicleEntry,
            { DATA: this.jsonBody}
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })

            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }

    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageVehicleEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }



  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  onChangeFuel($event: MatSelectChange) {
    this.jsonBody.fuel = this.fuels.filter(f => f.id === $event.value)[0].name;
  }

  onChangeCategory($event: MatSelectChange) {
    this.jsonBody.category = this.vehicleCats.filter(c => c.id === $event.value)[0].name;
  }

  onChangeModel($event: MatSelectChange) {
    this.jsonBody.model = this.vehicleModels.filter(m => m.id === $event.value)[0].name;
  }
}
