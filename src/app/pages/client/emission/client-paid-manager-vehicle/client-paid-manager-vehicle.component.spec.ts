import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPaidManagerVehicleComponent } from './client-paid-manager-vehicle.component';

describe('ClientPaidManagerVehicleComponent', () => {
  let component: ClientPaidManagerVehicleComponent;
  let fixture: ComponentFixture<ClientPaidManagerVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientPaidManagerVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientPaidManagerVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
