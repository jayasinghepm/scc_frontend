import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ClientRoutingModule} from "./client.routing.module";
import {ThemeModule} from "../../@theme/theme.module";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from "@angular/material";

import {
  NbCardModule,
  NbIconModule,
  NbInputModule, NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbTreeGridModule
} from "@nebular/theme";
import {ClientHomeComponent} from "./client-home/client-home.component";
import {ClientReportsComponent} from "./client-reports/client-reports.component";
import {ClientSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {ClientAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {ClientElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {ClientEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {ClientFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {ClientGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {ClientMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {ClientRefriComponent} from "./emission/client-refri/client-refri.component";
import {ClientTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {ClientTransportComponent} from "./emission/client-transport/client-transport.component";
import {ClientWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {ClientWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {CoreModule} from "../../@core/core.module";
import { ClinetEmployeeComponent } from './clinet-employee/clinet-employee.component';
import { AddEmployeeComponent } from './clinet-employee/add-employee/add-employee.component';
import {NgxEchartsModule} from "ngx-echarts";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {ClientUpdateElecComponent} from "./emission/client-electricity/update-elec/update-elec.component";
import {ClientUpdateGeneratorsComponent} from "./emission/client-generators/update-generators/update-generators.component";
import {ClientUpdateTransLocComponent} from "./emission/client-trans-loc-pur/update-trans-loc/update-trans-loc.component";
import {ClientUpdateWasteTransportComponent} from "./emission/client-waste-transport/update-waste-transport/update-waste-transport.component";
import {ClientUpdateFireExtComponent} from "./emission/client-fire-ext/update-fire-ext/update-fire-ext.component";
import {ClientUpdateEmpCommComponent} from "./emission/client-emp-commu/update-emp-comm/update-emp-comm.component";
import {ClientUpdateTransportComponent} from "./emission/client-transport/update-transport/update-transport.component";
import {ClientUpdateRefriComponent} from "./emission/client-refri/update-refri/update-refri.component";
import {ClientUpdateMunWaterComponent} from "./emission/client-municipal-water/update-mun-water/update-mun-water.component";
import {ClientUpdateAirtravelComponent} from "./emission/client-air-travel/client-update-airtravel/update-airtravel.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Ng2SmartTableModule} from "../../ng2-smart-table/src/lib/ng2-smart-table.module";
import {UpdateWasteDisComponent} from "./emission/client-waste-disposal/update-waste-dis/update-waste-dis.component";
import {ClientAshTransComponent} from "./emission/client-ash-trans/client-ash-trans.component";
import {UpdateClientAshTransComponent} from "./emission/client-ash-trans/update-client-ash-trans/update-client-ash-trans.component";
import {ClientForkliftsComponent} from "./emission/client-forklifts/client-forklifts.component";
import {UpdateClientForkliftsComponent} from "./emission/client-forklifts/update-client-forklifts/update-client-forklifts.component";
import {ClientFurnaceOilComponent} from "./emission/client-furnace-oil/client-furnace-oil.component";
import {ClientLorryTransComponent} from "./emission/client-lorry-trans/client-lorry-trans.component";
import {UpdateClientLorryTransComponent} from "./emission/client-lorry-trans/update-client-lorry-trans/update-client-lorry-trans.component";
import {ClientOilgasTransComponent} from "./emission/client-oilgas-trans/client-oilgas-trans.component";
import {UpdateClientOilgasComponent} from "./emission/client-oilgas-trans/update-client-oilgas/update-client-oilgas.component";
import {ClientPaidManagerVehicleComponent} from "./emission/client-paid-manager-vehicle/client-paid-manager-vehicle.component";
import {UpdateClientPaidManagerVehicleComponent} from "./emission/client-paid-manager-vehicle/update-client-paid-manager-vehicle/update-client-paid-manager-vehicle.component";
import {ClientPaperwasteComponent} from "./emission/client-paperwaste/client-paperwaste.component";
import {UpdateClientPaperWasteComponent} from "./emission/client-paperwaste/update-client-paper-waste/update-client-paper-waste.component";
import {ClientRawMaterialTransComponent} from "./emission/client-raw-material-trans/client-raw-material-trans.component";
import {UpdateClientRawMaterialTransComponent} from "./emission/client-raw-material-trans/update-client-raw-material-trans/update-client-raw-material-trans.component";
import {ClientSawDustTransComponent} from "./emission/client-saw-dust-trans/client-saw-dust-trans.component";
import {UpdateClientSawDustTransComponent} from "./emission/client-saw-dust-trans/update-client-saw-dust-trans/update-client-saw-dust-trans.component";
import {ClientSludgeTransComponent} from "./emission/client-sludge-trans/client-sludge-trans.component";
import {UpdateSludgeTransComponent} from "./emission/client-sludge-trans/update-sludge-trans/update-sludge-trans.component";
import {ClientStaffTransComponent} from "./emission/client-staff-trans/client-staff-trans.component";
import {UpdateClientStaffTransComponent} from "./emission/client-staff-trans/update-client-staff-trans/update-client-staff-trans.component";
import {BiomassComponent} from "./emission/biomass/biomass.component";
import {UpdateBiomassComponent} from "./emission/biomass/update-biomass/update-biomass.component";
import {LpgasComponent} from "./emission/lpgas/lpgas.component";
import {UpdateLpgasComponent} from "./emission/lpgas/update-lpgas/update-lpgas.component";
import { UpateClientVehicleOthersComponent } from './emission/client-vehicle-others/upate-client-vehicle-others/upate-client-vehicle-others.component';
import {ClientVehicleOthersComponent} from "./emission/client-vehicle-others/client-vehicle-others.component";
import {SeaAirFreightComponent} from "./emission/sea-air-freight/sea-air-freight.component";
import {ClientUpdateSeaAirFreightComponent} from "./emission/sea-air-freight/client-update-sea-air-freight/client-update-sea-air-freight.component";
import {UpdateClientFurnaceOilComponent} from "./emission/client-furnace-oil/update-client-furnace-oil/update-client-furnace-oil.component";



@NgModule({
  imports: [
    CommonModule,
    NbCardModule,
    MatSelectModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    ClientRoutingModule,
    NbSelectModule,
    NbRadioModule,
    NgxEchartsModule,
    NgxChartsModule,
    NbListModule,

  ],
  declarations: [
    ClientHomeComponent,
    ClientReportsComponent,
    ClientSummaryDataInputsComponent,
    ClientAirTravelComponent,
    ClientElectricityComponent,
    ClientEmpCommuComponent,
    ClientFireExtComponent,
    ClientGeneratorsComponent,
    ClientMunicipalWaterComponent,
    ClientRefriComponent,
    ClientTransLocPurComponent,
    ClientTransportComponent,
    ClientWasteTransportComponent,
    ClientWasteDisposalComponent,
    ClinetEmployeeComponent,
    ClinetEmployeeComponent,
    AddEmployeeComponent,
    ClientUpdateAirtravelComponent,
    ClientUpdateElecComponent,
    ClientUpdateEmpCommComponent,
    ClientUpdateFireExtComponent,
    ClientUpdateGeneratorsComponent,
    ClientUpdateMunWaterComponent,
    ClientUpdateRefriComponent,
    ClientUpdateTransLocComponent,
    ClientUpdateTransportComponent,
    // UpdateWasteDisComponent,
    ClientUpdateWasteTransportComponent,
    UpdateWasteDisComponent,
    ClientAshTransComponent,
    UpdateClientAshTransComponent,
    ClientForkliftsComponent,
    UpdateClientForkliftsComponent,
    ClientFurnaceOilComponent,
    UpdateClientFurnaceOilComponent,
    ClientLorryTransComponent,
    UpdateClientLorryTransComponent,
    ClientOilgasTransComponent,
    UpdateClientOilgasComponent,
    ClientPaidManagerVehicleComponent,
    UpdateClientPaidManagerVehicleComponent,
    ClientPaperwasteComponent,
    UpdateClientPaperWasteComponent,
    ClientRawMaterialTransComponent,
    UpdateClientRawMaterialTransComponent,
    ClientSawDustTransComponent,
    UpdateClientSawDustTransComponent,
    ClientSludgeTransComponent,
    UpdateSludgeTransComponent,
    ClientStaffTransComponent,
    UpdateClientStaffTransComponent,
    BiomassComponent,
    UpdateBiomassComponent,
    LpgasComponent,
    UpdateLpgasComponent,
    UpateClientVehicleOthersComponent,
    ClientVehicleOthersComponent,
    SeaAirFreightComponent,
    ClientUpdateSeaAirFreightComponent,

  ],
  entryComponents: [
    AddEmployeeComponent,
    ClientUpdateAirtravelComponent,
    ClientUpdateElecComponent,
    ClientUpdateEmpCommComponent,
    ClientUpdateFireExtComponent,
    ClientUpdateGeneratorsComponent,
    ClientUpdateMunWaterComponent,
    ClientUpdateRefriComponent,
    ClientUpdateTransLocComponent,
    ClientUpdateTransportComponent,
    // UpdateWasteDisComponent,
    ClientUpdateWasteTransportComponent,
    UpdateWasteDisComponent,

    UpdateLpgasComponent,
    UpdateBiomassComponent,
    UpdateClientAshTransComponent,
    UpdateClientForkliftsComponent,
    UpdateClientLorryTransComponent,
    UpdateClientPaperWasteComponent,
    UpdateClientPaidManagerVehicleComponent,
    UpdateClientOilgasComponent,
    UpdateClientRawMaterialTransComponent,
    UpdateClientSawDustTransComponent,
    UpdateSludgeTransComponent,
    UpdateClientStaffTransComponent,
    UpateClientVehicleOthersComponent,
    UpdateClientFurnaceOilComponent,


  ],

  providers: [
  ]
})
export class ClientModule { }
