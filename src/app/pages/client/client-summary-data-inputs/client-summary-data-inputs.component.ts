import { Component, OnInit } from '@angular/core';
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {BackendService} from "../../../@core/rest/bo_service";
import {UserState} from "../../../@core/auth/UserState";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {User} from "../../../@core/data/users";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-summary-data-inputs',
  templateUrl: './client-summary-data-inputs.component.html',
  styleUrls: ['./client-summary-data-inputs.component.scss']
})
export class ClientSummaryDataInputsComponent implements OnInit {
  public companines: {name: string, id: number}[] = [];
  public branches: {name: string, id: number} [] = [ { id: -1, name: "All Branches"}];

  public comId;
  public branchId = -1;



  public comowned = {
    jan: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    feb: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    mar: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    apr: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    may: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    jun: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    jul: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    aug: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    sep: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    oct: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    nov: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    dec: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    all: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    }

  }
  public rented =  {
    jan: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    feb: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    mar: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    apr: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    may: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    jun: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    jul: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    aug: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    sep: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    oct: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    nov: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    dec: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    all: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    }

  }
  public hired = {
    jan: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    feb: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    mar: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    apr: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    may: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    jun: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    jul: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    aug: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    sep: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    oct: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    nov: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    dec: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    },
    all: {
      l_petrol: '0.0',
      lkr_diesel: '0.0',
      na: true,
      l_diesel: '0.0',
      m3_diesel: "0.0",
      m3_petrol: '0.0',
      lkr_petrol: '0.0',
      nums: [],
    }

  }
  public fire = {
    jan: {
      na: true,
      kg: 0.0,
    },
    feb: { na: true,
      kg: 0.0,},
    mar: { na: true,
      kg: 0.0,},
    apr: { na: true,
      kg: 0.0,},
    may: { na: true,
      kg: 0.0,},
    jun: { na: true,
      kg: 0.0,},
    jul: { na: true,
      kg: 0.0,},
    aug: { na: true,
      kg: 0.0,},
    sep:{ na: true,
      kg: 0.0,},
    oct: { na: true,
      kg: 0.0,},
    nov: { na: true,
      kg: 0.0,},
    dec: { na: true,
      kg: 0.0,},
    all: { na: true,
      kg: 0.0,},
  }
  public refri = {
    jan: {
      na: true,
      kg: 0.0,
    },
    feb: { na: true,
      kg: 0.0,},
    mar: { na: true,
      kg: 0.0,},
    apr: { na: true,
      kg: 0.0,},
    may: { na: true,
      kg: 0.0,},
    jun: { na: true,
      kg: 0.0,},
    jul: { na: true,
      kg: 0.0,},
    aug: { na: true,
      kg: 0.0,},
    sep:{ na: true,
      kg: 0.0,},
    oct: { na: true,
      kg: 0.0,},
    nov: { na: true,
      kg: 0.0,},
    dec: { na: true,
      kg: 0.0,},
    all: { na: true,
      kg: 0.0,},
  };
  public wastTrans = {
    jan: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    feb:{
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    mar: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    apr: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    may: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    jun: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    jul: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    aug: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    sep: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    oct: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    nov:{
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    dec: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },
    all: {
      km_diesel: 0.0,
      na: true,
      km_petrol: 0.0,
      subcontractors: [],
    },

  }
  public transLoc = {
    jan: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    feb: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    mar: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    apr: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    may: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    jun: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    jul: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    aug: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    sep: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    oct: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    nov: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    dec: { km_diesel: 0.0, na: true, km_petrol: 0.0},
    all:{ km_diesel: 0.0, na: true, km_petrol: 0.0},
  }
  public elec = {
    jan: { na: false, kwh: 0.0, nums: []},
    feb: {na: false, kwh: 0.0, nums: []},
    mar: {na: false, kwh: 0.0, nums: []},
    apr: {na: false, kwh: 0.0, nums: []},
    may: {na: false, kwh: 0.0, nums: []},
    jun: {na: false, kwh: 0.0, nums: []},
    jul: {na: false, kwh: 0.0, nums: []},
    aug: {na: false, kwh: 0.0, nums: []},
    sep: {na: false, kwh: 0.0, nums: []},
    oct: {na: false, kwh: 0.0, nums: []},
    nov: {na: false, kwh: 0.0, nums: []},
    dec: {na: false, kwh: 0.0, nums: []},
    all: {na: false, kwh: 0.0, nums: []},
  }
  public mw = {
    jan: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    feb: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    mar: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    apr: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    may: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    jun: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    jul: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    aug: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    sep: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    oct: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    nov: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    dec: { lkr: 0.0, na: true, m3: 0.0, nums: []},
    all:{ lkr: 0.0, na: true, m3: 0.0, nums: []},
  }
  public airtravel = {
    jan: {
      emps: 0,

      na: true,
    },
    feb: { emps: 0,

      na: true,},
    mar: { emps: 0,

      na: true,},
    apr: { emps: 0,

      na: true,},
    may: { emps: 0,

      na: true,},
    jun: { emps: 0,

      na: true,},
    jul: { emps: 0,

      na: true,},
    aug: { emps: 0,

      na: true,},
    sep: { emps: 0,

      na: true,},
    oct: { emps: 0,

      na: true,},
    nov: { emps: 0,

      na: true,},
    dec: { emps: 0,

      na: true,},
    all:{ emps: 0,

      na: true,},
  }
  public emp_comm = {
    jan: {
      emps: 0,
      days: 0,
      na: true,
    },
    feb: { emps: 0,
      days: 0,
      na: true,},
    mar: { emps: 0,
      days: 0,
      na: true,},
    apr: { emps: 0,
      days: 0,
      na: true,},
    may: { emps: 0,
      days: 0,
      na: true,},
    jun: { emps: 0,
      days: 0,
      na: true,},
    jul: { emps: 0,
      days: 0,
      na: true,},
    aug: { emps: 0,
      days: 0,
      na: true,},
    sep: { emps: 0,
      days: 0,
      na: true,},
    oct: { emps: 0,
      days: 0,
      na: true,},
    nov: { emps: 0,
      days: 0,
      na: true,},
    dec: { emps: 0,
      days: 0,
      na: true,},
    all:{ emps: 0,
      days: 0,
      na: true,},
  }
  public gen = {
    jan: { lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    feb: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    mar: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    apr: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    may: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    jun: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    jul: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    aug: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    sep: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    oct: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    nov: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    dec: {lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
    all:{lkr: 0.0, na: true,  m3: 0.0, l: 0.0, nums: []},
  }
  public freight = {
    jan: { na: false, air_quantity: 0.0, sea_quantity: 0.0},
    feb: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    mar: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    apr: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    may: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    jun: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    jul: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    aug: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    sep: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    oct: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    nov: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    dec: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
    all: {na: false, air_quantity: 0.0, sea_quantity: 0.0},
  }


  private fy = '';

  public wastDis = {
    jan: {
      reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0
    },
    feb: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    mar: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    apr: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    may: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    jun: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    jul: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    aug: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    sep: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    oct: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    nov: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    dec: { reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
    all:{ reuse: 0,
      oloop: 0,
      cloop: 0,
      combu: 0,
      compo: 0,
      land: 0,
      anaer:0,
      piggery: 0,
      incin:0},
  }

  public dataEntryStatus = {
    elec: {
      name: 'Electricity Data Entry',
      status: 0,
    },
    mw: {
      name: 'Municipal Water Data Entry',
      status: 0,
    },
    refri: {
      name: 'Refrigerant Leakage Water Data Entry',
      status: 0,
    },
    fire: {
      name: 'Fire Extinguisher Data Entry',
      status: 0,
    },
    wasteTrans: {
      name: 'Waste Transport Data Entry',
      status: 0,
    },
    comOwned: {
      name: 'Company Owned Vehicles Data Entry',
      status: 0,
    },
    empComm: {
      name: 'Employee Commuting Data Entry',
      status: 0,
    },
    airtravel: {
      name: 'Business Air Travel Data Entry',
      status: 0,
    },
    rented: {
      name: 'Rented Vehicles Data Entry',
      status: 0,
    },
    hired: {
      name: 'Hired Vehicles Data Entry',
      status: 0,
    },
    gen: {
      name: 'Generators Data Entry',
      status: 0,
    },
    wasteDis: {
      name: 'Waste Disposal Data Entry',
      status: 0,
    },
    transLoc: {
      name: 'Transport Locally Purchased Data Entry',
      status: 0,
    },
    freight: {
      name: 'Freight Transport',
      status: 0,
    }
  }



  constructor(
    private masterData: MassterDataService,
    private boservice: BackendService,
    private router:Router
  ) {
    if ( !UserState.getInstance().existActiveProject) {
      this.router.navigate(['/pages/client/reports']);
    }else {
      this.comId = UserState.getInstance().companyId;
      this.branchId = UserState.getInstance().branchId;
      this.loadStatus();
      this.loadSummaryInputs();
    }


  }

  ngOnInit() {
  }

  private loadStatus()  {
    this.masterData.getYearsForCompany(this.comId).subscribe(d => {
      if (d !== undefined) {
        if (d.length > 0) {
          this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.DataEntryStatus,
            {
              companyId: this.comId,
              branchId: this.branchId,
              fy: d[0],
            }
          ).then(data => {
            if (data !== undefined && data.DAT !== undefined && data.DAT.list !== undefined) {
              data.DAT.list.forEach(k => {
                switch(k.emissionSrc) {
                  case 1: {
                    this.dataEntryStatus.elec.status  = k.status;
                    break;
                  }
                  case 2:{
                    this.dataEntryStatus.mw.status  = k.status;
                    break;
                  }
                  case 3:{
                    this.dataEntryStatus.refri.status  = k.status;
                    break;
                  }
                  case 4:{
                    this.dataEntryStatus.fire.status  = k.status;
                    break;
                  }
                  case 5:{
                    this.dataEntryStatus.wasteTrans.status  = k.status;
                    break;
                  }
                  case 6:{
                    this.dataEntryStatus.comOwned.status  = k.status;
                    break;
                  }
                  case 7: {
                    this.dataEntryStatus.empComm.status  = k.status;
                    break;
                  }
                  case 8: {
                    this.dataEntryStatus.airtravel.status  = k.status;
                    break;
                  }
                  case 9: {
                    this.dataEntryStatus.rented.status  = k.status;
                    break;
                  }
                  case 10: {
                    this.dataEntryStatus.hired.status  = k.status;
                    break;
                  }
                  case 11:{
                    this.dataEntryStatus.gen.status  = k.status;
                    break;
                  }
                  case 12:{
                    this.dataEntryStatus.wasteDis.status  = k.status;
                    break;
                  }
                  case 13:{
                    this.dataEntryStatus.transLoc.status  = k.status;
                    break;
                  }
                  case 14: {
                    this.dataEntryStatus.freight.status = k.status;
                    break;
                  }
                }
              })
            }
          });

        }
      }
    });
  }

  private loadSummaryInputs() {
    const jsonBody = {
      branchId: this.branchId,
      companyId: this.comId,
    };
    //freight
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.SeaAirFreightSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {

          this.freight = data.DAT.DTO;

        } else {
          //todo: show error
        }
      });
    //  elec
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.ElecSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {

          this.elec = data.DAT.DTO;


          let nums;
          // @ts-ignore
          nums = this.elec.jan.nums.split(",");
          this.elec.jan.nums = nums;
          // @ts-ignore
          nums = this.elec.feb.nums.split(",");
          this.elec.feb.nums = nums;
          // @ts-ignore
          nums = this.elec.mar.nums.split(",");
          this.elec.mar.nums = nums;
          // @ts-ignore
          nums = this.elec.apr.nums.split(",");
          this.elec.apr.nums = nums;
          // @ts-ignore
          nums = this.elec.may.nums.split(",");
          this.elec.may.nums = nums;
          // @ts-ignore
          nums = this.elec.jun.nums.split(",");
          this.elec.jun.nums = nums;
          // @ts-ignore
          nums = this.elec.jul.nums.split(",");
          this.elec.jul.nums = nums;
          // @ts-ignore
          nums = this.elec.aug.nums.split(",");
          this.elec.aug.nums = nums;
          // @ts-ignore
          nums = this.elec.sep.nums.split(",");
          this.elec.sep.nums = nums;
          // @ts-ignore
          nums = this.elec.oct.nums.split(",");
          this.elec.oct.nums = nums;
          // @ts-ignore
          nums = this.elec.nov.nums.split(",");
          this.elec.nov.nums = nums;
          // @ts-ignore
          nums = this.elec.dec.nums.split(",");
          this.elec.dec.nums = nums;
          // @ts-ignore
          nums = this.elec.all.nums.split(",");
          this.elec.all.nums = nums;


        } else {
          //todo: show error
        }
      });
    //mw
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.MunWaterSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.mw = data.DAT.DTO;

          let nums;
          // @ts-ignore
          nums = this.mw.jan.nums.split(",");
          this.mw.jan.nums = nums;
          // @ts-ignore
          nums = this.mw.feb.nums.split(",");
          this.mw.feb.nums = nums;
          // @ts-ignore
          nums = this.mw.mar.nums.split(",");
          this.mw.mar.nums = nums;
          // @ts-ignore
          nums = this.mw.apr.nums.split(",");
          this.mw.apr.nums = nums;
          // @ts-ignore
          nums = this.mw.may.nums.split(",");
          this.mw.may.nums = nums;
          // @ts-ignore
          nums = this.mw.jun.nums.split(",");
          this.mw.jun.nums = nums;
          // @ts-ignore
          nums = this.mw.jul.nums.split(",");
          this.mw.jul.nums = nums;
          // @ts-ignore
          nums = this.mw.aug.nums.split(",");
          this.mw.aug.nums = nums;
          // @ts-ignore
          nums = this.mw.sep.nums.split(",");
          this.mw.sep.nums = nums;
          // @ts-ignore
          nums = this.mw.oct.nums.split(",");
          this.mw.oct.nums = nums;
          // @ts-ignore
          nums = this.mw.nov.nums.split(",");
          this.mw.nov.nums = nums;
          // @ts-ignore
          nums = this.mw.dec.nums.split(",");
          this.mw.dec.nums = nums;
          // @ts-ignore
          nums = this.mw.all.nums.split(",");
          this.mw.all.nums = nums;

          console.log(this.mw)
        } else {
          //todo: show error
        }
      })
    //Refri
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.MunRefriSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.refri = data.DAT.DTO;
        } else {
          //todo: show error
        }
      })
    //fire
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.FireExtSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.fire = data.DAT.DTO;
        } else {
          //todo: show error
        }
      })
    //waste trans
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.WasteTransSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.wastTrans = data.DAT.DTO;
          let subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.jan.subcontractors.split(",");
          this.wastTrans.jan.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.feb.subcontractors.split(",");
          this.wastTrans.feb.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.mar.subcontractors.split(",");
          this.wastTrans.mar.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.apr.subcontractors.split(",");
          this.wastTrans.apr.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.may.subcontractors.split(",");
          this.wastTrans.may.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.jun.subcontractors.split(",");
          this.wastTrans.jun.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.jul.subcontractors.split(",");
          this.wastTrans.jul.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.aug.subcontractors.split(",");
          this.wastTrans.aug.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.sep.subcontractors.split(",");
          this.wastTrans.sep.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.oct.subcontractors.split(",");
          this.wastTrans.oct.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.nov.subcontractors.split(",");
          this.wastTrans.nov.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.dec.subcontractors.split(",");
          this.wastTrans.dec.subcontractors = subcontractors;
          // @ts-ignore
          subcontractors = this.wastTrans.all.subcontractors.split(",");
          this.wastTrans.all.subcontractors = subcontractors;

        } else {
          //todo: show error
        }
      })
    //com owned
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.ComOwnedSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.comowned = data.DAT.DTO;
          let nums;
          // @ts-ignore
          nums = this.comowned.jan.nums.split(",");
          this.comowned.jan.nums = nums;
          // @ts-ignore
          nums = this.comowned.feb.nums.split(",");
          this.comowned.feb.nums = nums;
          // @ts-ignore
          nums = this.comowned.mar.nums.split(",");
          this.comowned.mar.nums = nums;
          // @ts-ignore
          nums = this.comowned.apr.nums.split(",");
          this.comowned.apr.nums = nums;
          // @ts-ignore
          nums = this.comowned.may.nums.split(",");
          this.comowned.may.nums = nums;
          // @ts-ignore
          nums = this.comowned.jun.nums.split(",");
          this.comowned.jun.nums = nums;
          // @ts-ignore
          nums = this.comowned.jul.nums.split(",");
          this.comowned.jul.nums = nums;
          // @ts-ignore
          nums = this.comowned.aug.nums.split(",");
          this.comowned.aug.nums = nums;
          // @ts-ignore
          nums = this.comowned.sep.nums.split(",");
          this.comowned.sep.nums = nums;
          // @ts-ignore
          nums = this.comowned.oct.nums.split(",");
          this.comowned.oct.nums = nums;
          // @ts-ignore
          nums = this.comowned.nov.nums.split(",");
          this.comowned.nov.nums = nums;
          // @ts-ignore
          nums = this.comowned.dec.nums.split(",");
          this.comowned.dec.nums = nums;
          // @ts-ignore
          nums = this.comowned.all.nums.split(",");
          this.comowned.all.nums = nums;


        } else {
          //todo: show error
        }
      })
    //emp comm
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.EmpCommSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          // this.emp_comm = data.DAT.DTO;
          for (let k1 of Object.keys(data.DAT.DTO)) {
            if (data.DAT.DTO[k1] !== undefined) {
              for (let k2 of Object.keys(data.DAT.DTO[k1])) {
                this.emp_comm[k1][k2] = JSON.parse(data.DAT.DTO[k1][k2]);
              }
            }
          }
        } else {
          //todo: show error
        }
      })
    //air travel
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.AirtravelSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          for (let k1 of Object.keys(data.DAT.DTO)) {
            if (data.DAT.DTO[k1] !== undefined) {
              for (let k2 of Object.keys(data.DAT.DTO[k1])) {
                this.airtravel[k1][k2] = JSON.parse(data.DAT.DTO[k1][k2]);
              }
            }
          }
        }

      })
    //rented
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.RentedSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.rented = data.DAT.DTO;

          let nums;
          // @ts-ignore
          nums = this.rented.jan.nums.split(",");
          this.rented.jan.nums = nums;
          // @ts-ignore
          nums = this.rented.feb.nums.split(",");
          this.rented.feb.nums = nums;
          // @ts-ignore
          nums = this.rented.mar.nums.split(",");
          this.rented.mar.nums = nums;
          // @ts-ignore
          nums = this.rented.apr.nums.split(",");
          this.rented.apr.nums = nums;
          // @ts-ignore
          nums = this.rented.may.nums.split(",");
          this.rented.may.nums = nums;
          // @ts-ignore
          nums = this.rented.jun.nums.split(",");
          this.rented.jun.nums = nums;
          // @ts-ignore
          nums = this.rented.jul.nums.split(",");
          this.rented.jul.nums = nums;
          // @ts-ignore
          nums = this.rented.aug.nums.split(",");
          this.rented.aug.nums = nums;
          // @ts-ignore
          nums = this.rented.sep.nums.split(",");
          this.rented.sep.nums = nums;
          // @ts-ignore
          nums = this.rented.oct.nums.split(",");
          this.rented.oct.nums = nums;
          // @ts-ignore
          nums = this.rented.nov.nums.split(",");
          this.rented.nov.nums = nums;
          // @ts-ignore
          nums = this.rented.dec.nums.split(",");
          this.rented.dec.nums = nums;
          // @ts-ignore
          nums = this.rented.all.nums.split(",");
          this.rented.all.nums = nums;


        } else {
          //todo: show error
        }
      })
    //Hired
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.HiredSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.hired = data.DAT.DTO;

          let nums;
          // @ts-ignore
          nums = this.hired.jan.nums.split(",");
          this.hired.jan.nums = nums;
          // @ts-ignore
          nums = this.hired.feb.nums.split(",");
          this.hired.feb.nums = nums;
          // @ts-ignore
          nums = this.hired.mar.nums.split(",");
          this.hired.mar.nums = nums;
          // @ts-ignore
          nums = this.hired.apr.nums.split(",");
          this.hired.apr.nums = nums;
          // @ts-ignore
          nums = this.hired.may.nums.split(",");
          this.hired.may.nums = nums;
          // @ts-ignore
          nums = this.hired.jun.nums.split(",");
          this.hired.jun.nums = nums;
          // @ts-ignore
          nums = this.hired.jul.nums.split(",");
          this.hired.jul.nums = nums;
          // @ts-ignore
          nums = this.hired.aug.nums.split(",");
          this.hired.aug.nums = nums;
          // @ts-ignore
          nums = this.hired.sep.nums.split(",");
          this.hired.sep.nums = nums;
          // @ts-ignore
          nums = this.hired.oct.nums.split(",");
          this.hired.oct.nums = nums;
          // @ts-ignore
          nums = this.hired.nov.nums.split(",");
          this.hired.nov.nums = nums;
          // @ts-ignore
          nums = this.hired.dec.nums.split(",");
          this.hired.dec.nums = nums;
          // @ts-ignore
          nums = this.hired.all.nums.split(",");
          this.hired.all.nums = nums;


        } else {
          //todo: show error
        }
      })
    //Genera
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.GeneratorSummInpust, jsonBody)
      .then(data => {

        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.gen = data.DAT.DTO;
          let nums;
          // @ts-ignore
          nums = this.gen.jan.nums.split(",");
          this.gen.jan.nums = nums;
          // @ts-ignore
          nums = this.gen.feb.nums.split(",");
          this.gen.feb.nums = nums;
          // @ts-ignore
          nums = this.gen.mar.nums.split(",");
          this.gen.mar.nums = nums;
          // @ts-ignore
          nums = this.gen.apr.nums.split(",");
          this.gen.apr.nums = nums;
          // @ts-ignore
          nums = this.gen.may.nums.split(",");
          this.gen.may.nums = nums;
          // @ts-ignore
          nums = this.gen.jun.nums.split(",");
          this.gen.jun.nums = nums;
          // @ts-ignore
          nums = this.gen.jul.nums.split(",");
          this.gen.jul.nums = nums;
          // @ts-ignore
          nums = this.gen.aug.nums.split(",");
          this.gen.aug.nums = nums;
          // @ts-ignore
          nums = this.gen.sep.nums.split(",");
          this.gen.sep.nums = nums;
          // @ts-ignore
          nums = this.gen.oct.nums.split(",");
          this.gen.oct.nums = nums;
          // @ts-ignore
          nums = this.gen.nov.nums.split(",");
          this.gen.nov.nums = nums;
          // @ts-ignore
          nums = this.gen.dec.nums.split(",");
          this.gen.dec.nums = nums;
          // @ts-ignore
          nums = this.gen.all.nums.split(",");
          this.gen.all.nums = nums;


        } else {
          //todo: show error
        }
      })
    //waste dis
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.WasteDisSummInputs, jsonBody)
      .then(data => {
        console.log(data)
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          for (let k1 of Object.keys(data.DAT.DTO)) {
            if (data.DAT.DTO[k1] !== undefined) {
              for (let k2 of Object.keys(data.DAT.DTO[k1])) {
                this.wastDis[k1][k2] = JSON.parse(data.DAT.DTO[k1][k2]);
              }
            }
          }
        }
      })
    //transloc pur
    this.boservice.sendRequestToBackend(RequestGroup.SummaryInputs, RequestType.TransLocPurSummInputs, jsonBody)
      .then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.DTO !== undefined) {
          this.transLoc = data.DAT.DTO;
          for (let k1 of Object.keys(data.DAT.DTO)) {
            if (data.DAT.DTO[k1] !== undefined) {
              for (let k2 of Object.keys(data.DAT.DTO[k1])) {
                this.transLoc[k1][k2] = JSON.parse(data.DAT.DTO[k1][k2]);
              }
            }
          }

        } else {
          //todo: show error
        }
      })
  }


}
