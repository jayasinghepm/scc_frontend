import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, Routes, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {UserState} from "../../../@core/auth/UserState";

@Injectable()
export class ClientRouteGuard implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (UserState.getInstance().isAuthenticated) {
      return true;
    }
    console.log("Client guard")
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;

    // return true;
  }
  constructor(private router: Router) {}

}
