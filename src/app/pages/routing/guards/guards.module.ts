import {ModuleWithProviders, NgModule} from "@angular/core";
import {AdminRouteGuard} from "./admin-route.guard";
import {CadminRouteGuard} from "./cadmin-route.guard";
import {ClientRouteGuard} from "./client-route.guard";
import {CommonModule} from "@angular/common";

@NgModule( {
  imports: [
    CommonModule
  ]
})
export class GuardsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: GuardsModule,
      providers: [
        AdminRouteGuard,
        CadminRouteGuard,
        ClientRouteGuard
      ]
    };
  }
}
