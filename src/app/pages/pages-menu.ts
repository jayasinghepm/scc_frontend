import { NbMenuItem } from '@nebular/theme';
import {UserState} from "../@core/auth/UserState";
import {PageIdEnums} from "../@core/enums/PageIdEnums";
export const getComAdminMenus  = ():NbMenuItem[] => {
  const menus = [
    {
      title: 'Home',
      icon: 'home-outline',
      link: '/pages/com_admin/home',
      home: true,
    },
  ];


  const pages = UserState.getInstance().pages;

  if (pages == undefined) {
    return  [];
  }

  if (pages[PageIdEnums.Branch -1]) {
    menus.push( {
      title: 'Branches',
      icon: 'globe-2-outline',
      link: '/pages/com_admin/branches',
      home: false,
    })
  }
  if (pages[PageIdEnums.Users -1]) {
    menus.push({
      icon: 'person-outline',
      title: 'Company Users',
      link: '/pages/com_admin/data_entry_users',
      home: false,
    })
  }
  if (pages[PageIdEnums.SummInputs -1]) {
    // menus.push( {
    //   title: 'Summary Data Inputs',
    //   icon: 'archive-outline',
    //   link: '/pages/com_admin/summary_data_inputs',
    //   home: false,
    // })
  }
  if (pages[PageIdEnums.AccessControl -1]) {
    menus.push( {
      title: 'Access Control',
      icon: 'shield-outline',
      link: '/pages/com_admin/access',
      home: false,
    })
  }
  const enterDataMenu = {
    title: 'Enter Data',
    icon: 'edit-outline',
    children: [],
    link: '',
    home: false,
  }
  if (pages[PageIdEnums.AirTravel -1]) {
    enterDataMenu.children.push( {
      title: 'Business Air travel',
      link: '/pages/com_admin/air_travel'
    })
  }
  if (pages[PageIdEnums.FireExt -1]) {
    enterDataMenu.children.push({
      title: 'Fire Extinguisher',
      link: '/pages/com_admin/fire_ext'
    })
  }
  if (pages[PageIdEnums.Refri - 1]) {
    enterDataMenu.children.push( {
      title: 'Refrigerants',
      link: '/pages/com_admin/refri'
    })
  }
  if (pages[PageIdEnums.Generator - 1]) {
    enterDataMenu.children.push({
      title: 'Generators',
      link: '/pages/com_admin/generators'
    })
  }
  if(pages[PageIdEnums.Elec -1]) {
    enterDataMenu.children.push( {
      title: 'Electricity',
      link: '/pages/com_admin/electricity',
    })
  }
  if (pages[PageIdEnums.WasteDis -1]) {
    enterDataMenu.children.push(
      {
        title: 'Waste Disposal',
        link: '/pages/com_admin/waste_disposal',
      }
    )
  }
  if(pages[PageIdEnums.MunWater -1]) {
    enterDataMenu.children.push(
      {
        title: 'Municipal water',
        link: '/pages/com_admin/municipal_water',
      }
    )
  }
  if (pages[PageIdEnums.EmpComm -1]) {
    enterDataMenu.children.push( {
      title: 'Employee Commuting ',
      link: '/pages/com_admin/emp_comm',
    })
  }
  if (pages[PageIdEnums.Transport - 1]) {
    enterDataMenu.children.push( {
      title: 'Transport',
      link: '/pages/com_admin/transport',
    })
  }
  if (pages[PageIdEnums.WasteTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Waste transport',
      link: '/pages/com_admin/waste_transport',
    })
  }
  if (pages[PageIdEnums.TransLocPur -1]) {
    enterDataMenu.children.push( {
      title: 'Transport Locally purchased',
      link: '/pages/com_admin/trans_loc_purch',
    })
  }
  if (pages[PageIdEnums.Freight -1]) {
    enterDataMenu.children.push( {
      title: 'Freight Transport',
      link: '/pages/com_admin/freight_transport',
    })
  }
  if (pages[PageIdEnums.BioMass -1]) {
    enterDataMenu.children.push( {
      title: 'Biomass',
      link: '/pages/com_admin/biomass',
    })
  }
  if (pages[PageIdEnums.LPG -1]) {
    enterDataMenu.children.push( {
      title: 'LP Gas',
      link: '/pages/com_admin/lpgas',
    })
  }
  if (pages[PageIdEnums.Bgas -1]) {
    enterDataMenu.children.push( {
      title: 'Bio Gas',
      link: '/pages/com_admin/bgas',
    })
  }
  if (pages[PageIdEnums.AshTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Ash Transportation',
      link: '/pages/com_admin/ash_transport',
    })
  }
  if (pages[PageIdEnums.Forklifts -1]) {
    enterDataMenu.children.push( {
      title: 'Forklifts',
      link: '/pages/com_admin/forklifts',
    })
  }

  if (pages[PageIdEnums.FurnaceOil -1]) {
    enterDataMenu.children.push( {
      title: 'Furnace Oil',
      link: '/pages/com_admin/furnace-oil',
    })
  }
  if (pages[PageIdEnums.LorryTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Lorry Transportation',
      link: '/pages/com_admin/lorry-transport',
    })
  }
  if (pages[PageIdEnums.OilGasTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Oil and Gas Transportation',
      link: '/pages/com_admin/oilgas-transport',
    })
  }
  if (pages[PageIdEnums.PaidManagerVehicle -1]) {
    enterDataMenu.children.push( {
      title: 'Paid Manager Vehicle',
      link: '/pages/com_admin/paid-manager-vehicle',
    })
  }
  if (pages[PageIdEnums.PaperWaste -1]) {
    enterDataMenu.children.push( {
      title: 'Paper Waste',
      link: '/pages/com_admin/paper-waste',
    })
  }
  if (pages[PageIdEnums.RawMatTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Raw Material Transportation Locally',
      link: '/pages/com_admin/raw-material-transport',
    })
  }
  if (pages[PageIdEnums.SawDustTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Saw Dust Transportation',
      link: '/pages/com_admin/saw-dust-transport',
    })
  }
  if (pages[PageIdEnums.StaffTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Staff Transportation',
      link: '/pages/com_admin/staff-transport',
    })
  }
  if (pages[PageIdEnums.SludgeTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Sludge Transportation',
      link: '/pages/com_admin/sludge-transport',
    })
  }
  if (pages[PageIdEnums.VehicleOthers -1]) {
    enterDataMenu.children.push( {
      title: 'Vehicle Others',
      link: '/pages/com_admin/vehicle_others',
    })
  }


  menus.push({
    title: 'Project Summary',
    icon: 'archive-outline',
    link: '/pages/com_admin/project_summary',
    home: false,
  })










  menus.push(enterDataMenu);

  //todo add bio mass and LPG

  //todo add menus
  // const efMenu = {
  //   title: 'Emission Factors',
  //   icon: 'clipboard-outline',
  //   home: false,
  //   link: '',
  //   children: [
  //     {
  //       icon: '',
  //       title: 'Common',
  //       link: '/pages/com_admin/factors'
  //
  //     },
  //     {
  //       icon: '',
  //       title: 'Waste Disposal',
  //       link: '/pages/com_admin/waste_factors'
  //
  //     },
  //     {
  //       icon: '',
  //       title: 'Public Transport',
  //       link: '/pages/com_admin/pub_factors'
  //
  //     }
  //   ],
  // };

  // menus.push( efMenu)
  menus.push( {
    title: 'Info',
    icon: 'info-outline',
    link: '/pages/com_admin/info',
    home: false,
  })
  menus.push( {
    title: 'Reports',
    icon: 'book-outline',
    link: '/pages/com_admin/reports',
    home: false,
  })

  if (pages[PageIdEnums.RenewableSolar -1]) {
    const mitigation = {
      title: 'Mitigation',
      icon: 'pantone-outline',
      link: '/pages/com_admin/solar',
      home: false,

    }
    // mitigation.children.push({
    //   title: 'Solar',
    //   link: '/pages/com_admin/solar',
    // })
    menus.push(mitigation)
  }

  return menus;
}


export const getDataEntryUserMenus  = ():NbMenuItem[] => {
  const menus = [
    {
      title: 'Home',
      icon: 'home-outline',
      link: '/pages/data_entry_user/home',
      home: true,
    }];
  const pages = UserState.getInstance().pages;

  if (pages == undefined) {
    return  [];
  }

  if (pages[PageIdEnums.Branch -1]) {
    menus.push( {
      title: 'Branches',
      icon: 'globe-2-outline',
      link: '/pages/data_entry_user/branches',
      home: false,
    })
  }
  // if (pages[PageIdEnums.Users -1]) {
  //   menus.push({
  //     icon: 'person-outline',
  //     title: 'Company Users',
  //     link: '/pages/com_admin/data_entry_users',
  //     home: false,
  //   })
  // }
  if (pages[PageIdEnums.SummInputs -1]) {
    // menus.push( {
    //   title: 'Summary Data Inputs',
    //   icon: 'archive-outline',
    //   link: '/pages/data_entry_user/summary_data_inputs',
    //   home: false,
    // })
  }

  const enterDataMenu = {
    title: 'Enter Data',
    icon: 'edit-outline',
    children: [],
    link: '',
    home: false,
  }
  if (pages[PageIdEnums.AirTravel -1]) {
    enterDataMenu.children.push( {
      title: 'Business Air travel',
      link: '/pages/data_entry_user/air_travel'
    })
  }
  if (pages[PageIdEnums.FireExt -1]) {
    enterDataMenu.children.push({
      title: 'Fire Extinguisher',
      link: '/pages/data_entry_user/fire_ext'
    })
  }
  if (pages[PageIdEnums.Refri - 1]) {
    enterDataMenu.children.push( {
      title: 'Refrigerants',
      link: '/pages/data_entry_user/refri'
    })
  }
  if (pages[PageIdEnums.Generator - 1]) {
    enterDataMenu.children.push({
      title: 'Generators',
      link: '/pages/data_entry_user/generators'
    })
  }
  if(pages[PageIdEnums.Elec -1]) {
    enterDataMenu.children.push( {
      title: 'Electricity',
      link: '/pages/data_entry_user/electricity',
    })
  }
  if (pages[PageIdEnums.WasteDis -1]) {
    enterDataMenu.children.push(
      {
        title: 'Waste Disposal',
        link: '/pages/data_entry_user/waste_disposal',
      }
    )
  }
  if(pages[PageIdEnums.MunWater -1]) {
    enterDataMenu.children.push(
      {
        title: 'Municipal water',
        link: '/pages/data_entry_user/municipal_water',
      }
    )
  }
  if (pages[PageIdEnums.EmpComm -1]) {
    enterDataMenu.children.push( {
      title: 'Employee Commuting ',
      link: '/pages/data_entry_user/emp_comm',
    })
  }
  if (pages[PageIdEnums.Transport - 1]) {
    enterDataMenu.children.push( {
      title: 'Transport',
      link: '/pages/data_entry_user/transport',
    })
  }
  if (pages[PageIdEnums.WasteTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Waste transport',
      link: '/pages/data_entry_user/waste_transport',
    })
  }
  if (pages[PageIdEnums.TransLocPur -1]) {
    enterDataMenu.children.push( {
      title: 'Transport Locally purchased',
      link: '/pages/data_entry_user/trans_loc_purch',
    })
  }
  if (pages[PageIdEnums.Freight -1]) {
    enterDataMenu.children.push( {
      title: 'Freight Transport',
      link: '/pages/data_entry_user/freight_transport',
    })
  }
  if (pages[PageIdEnums.BioMass -1]) {
    enterDataMenu.children.push( {
      title: 'Biomass',
      link: '/pages/data_entry_user/biomass',
    })
  }
  if (pages[PageIdEnums.LPG -1]) {
    enterDataMenu.children.push( {
      title: 'LP Gas',
      link: '/pages/data_entry_user/lpgas',
    })
  }
  if (pages[PageIdEnums.Bgas -1]) {
    enterDataMenu.children.push( {
      title: 'Bio Gas',
      link: '/pages/data_entry_user/bgas',
    })
  }


  if (pages[PageIdEnums.AshTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Ash Transportation',
      link: '/pages/data_entry_user/ash_transport',
    })
  }
  if (pages[PageIdEnums.Forklifts -1]) {
    enterDataMenu.children.push( {
      title: 'Forklifts',
      link: '/pages/data_entry_user/forklifts',
    })
  }

  if (pages[PageIdEnums.FurnaceOil -1]) {
    enterDataMenu.children.push( {
      title: 'Furnace Oil',
      link: '/pages/data_entry_user/furnace-oil',
    })
  }
  if (pages[PageIdEnums.LorryTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Lorry Transportation',
      link: '/pages/data_entry_user/lorry-transport',
    })
  }
  if (pages[PageIdEnums.OilGasTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Oil and Gas Transportation',
      link: '/pages/data_entry_user/oilgas-transport',
    })
  }
  if (pages[PageIdEnums.PaidManagerVehicle -1]) {
    enterDataMenu.children.push( {
      title: 'Paid Manager Vehicle',
      link: '/pages/data_entry_user/paid-manager-vehicle',
    })
  }
  if (pages[PageIdEnums.PaperWaste -1]) {
    enterDataMenu.children.push( {
      title: 'Paper Waste',
      link: '/pages/data_entry_user/paper-waste',
    })
  }
  if (pages[PageIdEnums.RawMatTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Raw Material Transportation Locally',
      link: '/pages/data_entry_user/raw-material-transport',
    })
  }
  if (pages[PageIdEnums.SawDustTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Saw Dust Transportation',
      link: '/pages/data_entry_user/saw-dust-transport',
    })
  }
  if (pages[PageIdEnums.StaffTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Staff Transportation',
      link: '/pages/data_entry_user/staff-transport',
    })
  }
  if (pages[PageIdEnums.SludgeTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Sludge Transportation',
      link: '/pages/data_entry_user/sludge-transport',
    })
  }
  if (pages[PageIdEnums.VehicleOthers -1]) {
    enterDataMenu.children.push( {
      title: 'Vehicle Others',
      link: '/pages/data_entry_user/vehicle_others',
    })
  }

  menus.push(enterDataMenu);

  menus.push({
    title: 'Project Summary',
    icon: 'archive-outline',
    link: '/pages/data_entry_user/project_summary',
    home: false,
  })

  //todo add bio mass and LPG

  //todo add menus

  const efMenu = {
    title: 'Emission Factors',
    icon: 'clipboard-outline',
    home: false,
    link: '',
    children: [
      {
        icon: '',
        title: 'Common',
        link: '/pages/data_entry_user/factors'

      },
      {
        icon: '',
        title: 'Waste Disposal',
        link: '/pages/data_entry_user/waste_factors'

      },
      {
        icon: '',
        title: 'Public Transport',
        link: '/pages/data_entry_user/pub_factors'

      }
    ],
  };

  // menus.push( efMenu)

  menus.push( {
    title: 'Reports',
    icon: 'book-outline',
    link: '/pages/data_entry_user/reports',
    home: false,
  })

  if (pages[PageIdEnums.RenewableSolar -1]) {
    menus.push({
      title: 'Mitigation',
      icon: 'archive-outline',
      link: '/pages/data_entry_user/solar',
      home: false,
    })
    const mitigation = {
      title: 'Mitigation',
      icon: 'pantone-outline',
      link: '/pages/data_entry_user/solar',
      home: false,
      children: [

      ]
    }
    // mitigation.children.push({
    //   title: 'Solar',
    //   link: '/pages/data_entry_user/solar',
    // })
    menus.push(mitigation)
  }

  return menus;
}


export const getClientMenus  = ():NbMenuItem[] => {
  const menus = [{
    title: 'Home',
    icon: 'home-outline',
    link: '/pages/client/home',
    home: true,
  }];
  const pages = UserState.getInstance().pages;

  if (pages == undefined) {
    return  [];
  }
  if (pages[PageIdEnums.SummInputs -1]) {
    // menus.push(  {
    //   title: 'Summary Data Inputs',
    //   icon: 'archive-outline',
    //   link: '/pages/client/summary_data_inputs',
    //   home: false,
    // })
  }


  const enterDataMenu = {
    title: 'Enter Data',
    icon: 'edit-outline',
    children: [],
    link: '',
    home: false,
  }
  if (pages[PageIdEnums.AirTravel -1]) {
    enterDataMenu.children.push( {
      title: 'Business Air travel',
      link: '/pages/client/air_travel'
    })
  }
  if (pages[PageIdEnums.FireExt -1]) {
    enterDataMenu.children.push({
      title: 'Fire Extinguisher',
      link: '/pages/client/fire_ext'
    })
  }
  if (pages[PageIdEnums.Refri - 1]) {
    enterDataMenu.children.push( {
      title: 'Refrigerants',
      link: '/pages/client/refri'
    })
  }
  if (pages[PageIdEnums.Generator - 1]) {
    enterDataMenu.children.push({
      title: 'Generators',
      link: '/pages/client/generators'
    })
  }
  if(pages[PageIdEnums.Elec -1]) {
    enterDataMenu.children.push( {
      title: 'Electricity',
      link: '/pages/client/electricity',
    })
  }
  if (pages[PageIdEnums.WasteDis -1]) {
    enterDataMenu.children.push(
      {
        title: 'Waste Disposal',
        link: '/pages/client/waste_disposal',
      }
    )
  }
  if(pages[PageIdEnums.MunWater -1]) {
    enterDataMenu.children.push(
      {
        title: 'Municipal water',
        link: '/pages/client/municipal_water',
      }
    )
  }
  if (pages[PageIdEnums.EmpComm -1]) {
    enterDataMenu.children.push( {
      title: 'Employee Commuting ',
      link: '/pages/client/emp_comm',
    })
  }
  if (pages[PageIdEnums.Transport - 1]) {
    enterDataMenu.children.push( {
      title: 'Transport',
      link: '/pages/client/transport',
    })
  }
  if (pages[PageIdEnums.WasteTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Waste transport',
      link: '/pages/client/waste_transport',
    })
  }
  if (pages[PageIdEnums.TransLocPur -1]) {
    enterDataMenu.children.push( {
      title: 'Transport Locally purchased',
      link: '/pages/client/trans_loc_purch',
    })
  }
  if (pages[PageIdEnums.Freight -1]) {
    enterDataMenu.children.push( {
      title: 'Freight Transport',
      link: '/pages/client/freight_transport',
    })
  }
  if (pages[PageIdEnums.BioMass -1]) {
    enterDataMenu.children.push( {
      title: 'Biomass',
      link: '/pages/client/biomass',
    })
  }
  if (pages[PageIdEnums.LPG -1]) {
    enterDataMenu.children.push( {
      title: 'LP Gas',
      link: '/pages/client/lpgas',
    })
  }
  if (pages[PageIdEnums.Bgas -1]) {
    enterDataMenu.children.push( {
      title: 'Bio Gas',
      link: '/pages/client/bgas',
    })
  }
  if (pages[PageIdEnums.AshTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Ash Transportation',
      link: '/pages/client/ash_transport',
    })
  }
  if (pages[PageIdEnums.Forklifts -1]) {
    enterDataMenu.children.push( {
      title: 'Forklifts',
      link: '/pages/client/forklifts',
    })
  }

  if (pages[PageIdEnums.FurnaceOil -1]) {
    enterDataMenu.children.push( {
      title: 'Furnace Oil',
      link: '/pages/client/furnace-oil',
    })
  }
  if (pages[PageIdEnums.LorryTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Lorry Transportation',
      link: '/pages/client/lorry-transport',
    })
  }
  if (pages[PageIdEnums.OilGasTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Oil and Gas Transportation',
      link: '/pages/client/oilgas-transport',
    })
  }
  if (pages[PageIdEnums.PaidManagerVehicle -1]) {
    enterDataMenu.children.push( {
      title: 'Paid Manager Vehicle',
      link: '/pages/client/paid-manager-vehicle',
    })
  }
  if (pages[PageIdEnums.PaperWaste -1]) {
    enterDataMenu.children.push( {
      title: 'Paper Waste',
      link: '/pages/client/paper-waste',
    })
  }
  if (pages[PageIdEnums.RawMatTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Raw Material Transportation Locally',
      link: '/pages/client/raw-material-transport',
    })
  }
  if (pages[PageIdEnums.SawDustTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Saw Dust Transportation',
      link: '/pages/client/saw-dust-transport',
    })
  }
  if (pages[PageIdEnums.StaffTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Staff Transportation',
      link: '/pages/client/staff-transport',
    })
  }
  if (pages[PageIdEnums.SludgeTrans -1]) {
    enterDataMenu.children.push( {
      title: 'Sludge Transportation',
      link: '/pages/client/sludge-transport',
    })
  }
  if (pages[PageIdEnums.VehicleOthers -1]) {
    enterDataMenu.children.push( {
      title: 'Vehicle Others',
      link: '/pages/client/vehicle_others',
    })
  }

  // if (pages[PageIdEnums.RenewableSolar -1]) {
  //   const mitigation = {
  //     title: 'Mitigation',
  //     icon: 'pantone-outline',
  //     link: '/pages/client/reports',
  //     home: false,
  //     children: [

  //     ]
  //   }
  //   mitigation.children.push({
  //     title: 'Solar',
  //     link: '/pages/client/solar',
  //   });
  //   menus.push(mitigation)
  // }

  // menus.push({
  //   title: 'Mitigation',
  //   icon: 'archive-outline',
  //   link: '/pages/data_entry_user/solar',
  //   home: false,
  // })



  menus.push(enterDataMenu);


  return menus;
}


export const CLIENT_MENU_ITEMS: NbMenuItem[] = getClientMenus();

export const ADMIN_MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Home',
    icon: 'home-outline',
    link: '/pages/admin/home',
    home: true,
  },
  {
    title: 'Project',
    icon: 'square-outline',
    link: '/pages/admin/project',
  },
  {
    title: 'Access Control',
    icon: 'shield-outline',
    link: '/pages/admin/access',
  },
  {
    title: 'Companies',
    icon: 'globe-outline',
    link: '/pages/admin/companies'
  },
  {
    title: 'Emission Categories',
    icon: 'people-outline',
    link: '/pages/admin/emission_categories'
  },
  {
    title: 'Manage Menus',
    icon: 'map-outline',
    link: '/pages/admin/manage_menus'
  },
  // {
  //   title: 'Summary Data Inputs',
  //   icon: 'archive-outline',
  //   link: '/pages/admin/summary_data_inputs',
  // },
  {
    title: 'Enter Data',
    icon: 'edit-outline',
    children: [
      {
        title: 'Business Air travel',
        link: '/pages/admin/air_travel'
      },
      {
        title: 'Fire Extinguisher',
        link: '/pages/admin/fire_ext'
      },
      {
        title: 'Refrigerants',
        link: '/pages/admin/refri'
      },
      {
        title: 'Generators',
        link: '/pages/admin/generators'
      },
      {
        title: 'Electricity',
        link: '/pages/admin/electricity',
      },
      {
        title: 'Waste Disposal',
        link: '/pages/admin/waste_disposal',
      },
      {
        title: 'Municipal water',
        link: '/pages/admin/municipal_water',
      },
      {
        title: 'Employee Commuting',
        link: '/pages/admin/emp_comm',
      },
      {
        title: 'Transport',
        link: '/pages/admin/transport',
      },
      {
        title: 'Waste transport',
        link: '/pages/admin/waste_transport',
      },
      {
        title: 'Transport Locally purchased',
        link: '/pages/admin/trans_loc_purch',
      },
      {
        title: 'Freight Transport',
        link: '/pages/admin/freight_transport',
      },
      {
        title: 'Biomass',
        link: '/pages/admin/biomass',
      },
      {
        title: 'LP Gas',
        link: '/pages/admin/lpgas',
      },
      {
        title: 'Bio Gas',
        link: '/pages/admin/bgas',
      },
      {
        title: 'Ash Transportation',
        link: '/pages/admin/ash_transport'
      },
      {
        title : 'Forklifts',
        link: '/pages/admin/forklifts',
      },
      {
        title: 'Furnace Oil',
        link: '/pages/admin/furnace-oil'
      },
      {
        title: 'Lorry Transportation',
        link: '/pages/admin/lorry-transport',
      },
      {
        title: 'Paid Manager Vehicles',
        link: '/pages/admin/paid-manger-vehicle',
      },
      {
        title: 'Paper Waste',
        link: '/pages/admin/paper-waste',
      },
      {
        title: 'Oil and Gas Transportation',
        link: '/pages/admin/oilgas-transport',
      },
      {
        title: 'Raw Material Transportation',
        link: '/pages/admin/raw-material-transport',
      },
      {
        title: 'Saw Dust Transportation',
        link: '/pages/admin/saw-dust-transport',
      },
      {
        title: 'Staff Transportation',
        link: '/pages/admin/staff-transport',
      },
      {
        title: 'Sludge Transportation',
        link: '/pages/admin/sludge-transport',
      },
      {
        title: 'Vehicle Others',
        link: '/pages/admin/vehicle_others',
      }
    ]
  },
  {
    title: 'Project Summary',
    icon: 'archive-outline',
    link: '/pages/admin/project_summary',
    home: false,
  },
  {
    title: 'GHG History',
    icon: 'book-open-outline',
    link: '/pages/admin/emission_history'
  },
  {
    title: 'Emission Factors',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'Common',
        link: '/pages/admin/factors'
      },
      {
        title: 'Waste Disposal',
        link: '/pages/admin/waste_factors'
      },
      {
        title: 'Public Transport',
        link: '/pages/admin/pub_factors'
      }
    ]

  },
  {
    title: 'Users',
    icon: 'person-outline',
    children: [
      {
        title: 'Company Admins',
        link: '/pages/admin/com_admins'
      },
      {
        title: 'Clients',
        link: '/pages/admin/admin_clients'
      },
      {
        title: 'Data Entry Users',
        link: '/pages/admin/data_entry_users'
      }

    ]

  },

  {
    title: 'Reports',
    icon: 'book-outline',
    link: '/pages/admin/reports'
  }
]

export const MASTER_ADMIN_ITEMS: NbMenuItem[] = [
  {
    title: 'Home',
    icon: 'home-outline',
    link: '/pages/admin/home',
    home: true,
  },
  {
    title: 'Project',
    icon: 'square-outline',
    link: '/pages/admin/project',
  },
  {
    title: 'Access Control',
    icon: 'shield-outline',
    link: '/pages/admin/access',
  },
  {
    title: 'Companies',
    icon: 'globe-outline',
    link: '/pages/admin/companies'
  },
  {
    title: 'Emission Categories',
    icon: 'people-outline',
    link: '/pages/admin/emission_categories'
  },
  {
    title: 'Manage Menus',
    icon: 'map-outline',
    link: '/pages/admin/manage_menus'
  },
  // {
  //   title: 'Summary Data Inputs',
  //   icon: 'archive-outline',
  //   link: '/pages/admin/summary_data_inputs',
  // },
  {
    title: 'Enter Data',
    icon: 'edit-outline',
    children: [
      {
        title: 'Business Air travel',
        link: '/pages/admin/air_travel'
      },
      {
        title: 'Fire Extinguisher',
        link: '/pages/admin/fire_ext'
      },
      {
        title: 'Refrigerants',
        link: '/pages/admin/refri'
      },
      {
        title: 'Generators',
        link: '/pages/admin/generators'
      },
      {
        title: 'Electricity',
        link: '/pages/admin/electricity',
      },
      {
        title: 'Waste Disposal',
        link: '/pages/admin/waste_disposal',
      },
      {
        title: 'Municipal water',
        link: '/pages/admin/municipal_water',
      },
      {
        title: 'Employee Commuting ',
        link: '/pages/admin/emp_comm',
      },
      {
        title: 'Transport',
        link: '/pages/admin/transport',
      },
      {
        title: 'Waste transport',
        link: '/pages/admin/waste_transport',
      },
      {
        title: 'Transport Locally purchased',
        link: '/pages/admin/trans_loc_purch',
      },
      {
        title: 'Freight Transport',
        link: '/pages/admin/freight_transport',
      },
      {
        title: 'Biomass',
        link: '/pages/admin/biomass',
      },
      {
        title: 'LP Gas',
        link: '/pages/admin/lpgas',
      },
      {
        title: 'Bio Gas',
        link: '/pages/admin/bgas',
      },
      {
        title: 'Ash Transportation',
        link: '/pages/admin/ash_transport'
      },
      {
        title : 'Forklifts',
        link: '/pages/admin/forklifts',
      },
      {
        title: 'Furnace Oil',
        link: '/pages/admin/furnace-oil'
      },
      {
        title: 'Lorry Transportation',
        link: '/pages/admin/lorry-transport',
      },
      {
        title: 'Paid Manager Vehicles',
        link: '/pages/admin/paid-manger-vehicle',
      },
      {
        title: 'Paper Waste',
        link: '/pages/admin/paper-waste',
      },
      {
        title: 'Oil and Gas Transportation',
        link: '/pages/admin/oilgas-transport',
      },
      {
        title: 'Raw Material Transportation',
        link: '/pages/admin/raw-material-transport',
      },
      {
        title: 'Saw Dust Transportation',
        link: '/pages/admin/saw-dust-transport',
      },
      {
        title: 'Staff Transportation',
        link: '/pages/admin/staff-transport',
      },
      {
        title: 'Sludge Transportation',
        link: '/pages/admin/sludge-transport',
      },
      // {
      //   title: 'Staff Transportation',
      //   link: '/pages/admin/staff-transport',
      // },
      {
        title: 'Vehicle Others',
        link: '/pages/admin/vehicle_others',
      }
    ]
  },
  {
    title: 'Project Summary',
    icon: 'archive-outline',
    link: '/pages/admin/project_summary',
    home: false,
  },
  {
    title: 'GHG History',
    icon: 'book-open-outline',
    link: '/pages/admin/emission_history'
  },
  {
    title: 'Emission Factors',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'Common',
        link: '/pages/admin/factors'
      },
      {
        title: 'Waste Disposal',
        link: '/pages/admin/waste_factors'
      },
      {
        title: 'Public Transport',
        link: '/pages/admin/pub_factors'
      }
    ]

  },
  {
    title: 'Users',
    icon: 'person-outline',
    children: [
      {
        title: 'Company Admins',
        link: '/pages/admin/com_admins'
      },
      {
        title: 'Clients',
        link: '/pages/admin/admin_clients'
      },
      {
        title: 'ClimateSI Admins',
        link: '/pages/admin/admins'
      },
      {
        title: 'Data Entry Users',
        link: '/pages/admin/data_entry_users'
      }
    ]

  },

  {
    title: 'Reports',
    icon: 'book-outline',
    link: '/pages/admin/reports'
  }
]

export const COM_ADMIN_MENU_ITEMS: NbMenuItem[] = getComAdminMenus();

export const DATA_ENTRY_USER_MENU_ITEMS: NbMenuItem[] = getDataEntryUserMenus();







