import { Component, OnInit } from '@angular/core';
import {BackendService} from "../../@core/rest/bo_service";
import {UserState} from "../../@core/auth/UserState";
import {MassterDataService} from "../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-project-summary',
  templateUrl: './project-summary.component.html',
  styleUrls: ['./project-summary.component.scss']
})
export class ProjectSummaryComponent implements OnInit {

  public userType = UserState.getInstance().userType;

  public inputExcludedReason  = {
    reason: '',
    srcName: '',
  }

  public inputSuggestion = {
    title: '',
    description: '',
  }

  public  summaryData = {

     companyId: 1,

     projectId : 2,

     fy: '',

     isFy: false,

     companyName: "test company",

     methodology: "ghg protocoal",

     protocol: "ghp protocol",

    totalEmission: 20.0,

    totalDirectEmission: 12.23,

    totalIndirectEmission: 8.00,
    excludedReasons: [{
     srcName: 'src 1',
     reason: 'reason 1',
    }],

    suggestions: [{
     title: 'title 1',
     description: 'desc 1',
    }],

    directSources: [
      {

        srcName: 'src 1',
        srcId: 1,

        emissionFactors: [{
          name: '',
          value: 2,
          unit: 1,
          unitStr: 'liters'
        }],


        activityData: [
          {
            metricsName: 'act data 1',
            values: [0, 12, 2, 232, 1, 23, 2, 3, 2, 3, 2, 2,2],
            unit: 1,
            unitStr: 'liters',
          }
        ],


        sourceOfData : '',

        emission: 2,

      }
    ],

    indirectSources: [
      {

        srcName: 'src 1',
        srcId: 1,

        emissionFactors: [{
          name: '',
          value: 2,
          unit: 1,
          unitStr: 'liters'
        }],


        activityData: [
          {
            metricsName: 'act data 1',
            values: [0, 12, 2, 232, 1, 23, 2, 3, 2, 3, 2, 2,2],
            unit: 1,
            unitStr: 'liters',
          }
        ],


        sourceOfData : '',

        emission: 2,

      }
    ],

    unCategorized : [{

      srcName: 'src 1',
       srcId: 1,

      emissionFactors: [{
        name: '',
        value: 2,
        unit: 1,
       unitStr: 'liters'
      }],


       activityData: [
         {
           metricsName: 'act data 1',
           values: [0, 12, 2, 232, 1, 23, 2, 3, 2, 3, 2, 2,2],
           unit: 1,
           unitStr: 'liters',
         }
       ],


      sourceOfData : '',

      emission: 2,

    }]
  }

  public companies: {
    id: number,
    name: '',
    pages: [],
    emissionCategories: [],
    allowedEmissionSources: any,
    emSources: []
  } [] = [];
  public companyId  = 0;


  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private toastSerivce: NbToastrService) {

    this.masterData.loadCompaniesDtos().subscribe(data => {
      this.companies = data;
    });
  }

  ngOnInit() {
  }


  onChangeCompany(value: any) {
    // const company = this.companines.find(com => com.id == value);
    // console.log(company)

    //todo request data
    this.loadData(value)

  }

  covertActDataOrderForFy(values:number[]) : number[] {
      const janIndex = 0;
      const aprIndex = 0;
      const decIndex = 11;
      const valuesJanToMar = values.splice(janIndex, aprIndex);
      values.splice(janIndex, 3);
      values.splice(11, 0, ...valuesJanToMar)
    console.log(values);
      return values;
  }

  loadData (companyId: number) {

  }

  onAddExcludedReason($event: MouseEvent) {
   if ( this.summaryData.excludedReasons == undefined ) {
     this.summaryData.excludedReasons = [];
   }else {

     if (!Boolean(this.inputExcludedReason.srcName) || !Boolean(this.inputExcludedReason.reason) ) {
       this.toastSerivce.show('', 'Source Name and Reason cannot be empty!', {
         status: 'warning',
         destroyByClick: true,
         duration: 2000,
         hasIcon: false,
         position: NbGlobalPhysicalPosition.TOP_RIGHT,
         preventDuplicates: true,
       });
       return;
     }


     if (this.summaryData.excludedReasons.find(reas => reas.srcName == this.inputExcludedReason.srcName)) {
       this.toastSerivce.show('', 'Emission Source already exists!', {
         status: 'warning',
         destroyByClick: true,
         duration: 2000,
         hasIcon: false,
         position: NbGlobalPhysicalPosition.TOP_RIGHT,
         preventDuplicates: true,
       });
       return;



     }

     this.summaryData.excludedReasons.push({
       reason: this.inputExcludedReason.reason,
       srcName: this.inputExcludedReason.srcName
     });
     this.inputExcludedReason.srcName = '';
     this.inputExcludedReason.reason = '';

     //TODO : add to backend and load data again


   }

  }

  onDeleteExcludedSrc($event: MouseEvent, delSrc: { reason: string; srcName: string }) {
    let delIndex =  this.summaryData.excludedReasons.findIndex(src  => delSrc.srcName == src.srcName);
    if (delIndex >=0 ) {
      this.summaryData.excludedReasons.splice(delIndex ,1);


      //todo bakckend delete and load data again
    }
  }

  onAddSuggestion($event: MouseEvent) {
    if (this.summaryData.suggestions == undefined) {
      this.summaryData.suggestions = [];
    } else {

      if (!Boolean(this.inputSuggestion.title) || !Boolean(this.inputSuggestion.description)) {
        this.toastSerivce.show('', 'Title and Description cannot be empty!', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }


      if (this.summaryData.suggestions.find(sugg => sugg.title == this.inputSuggestion.title)) {
        this.toastSerivce.show('', 'Suggestion already exists!', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;


      }

      this.summaryData.suggestions.push({
        title: this.inputSuggestion.title,
        description: this.inputSuggestion.description,
      });
      this.inputSuggestion.title = '';
      this.inputSuggestion.description = ''

      //TODO : add to backend and load data again


    }

  }

  onDeleteSuggestion($event: MouseEvent, delSugg: {description: string; title: string}) {
      let delIndex =  this.summaryData.suggestions.findIndex(suggestion  => delSugg.title === suggestion.title);
      if (delIndex >=0 ) {
        this.summaryData.suggestions.splice(delIndex ,1);


        //todo bakckend delete and load data again
      }
  }
}
