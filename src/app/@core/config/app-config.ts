
// {
//   method: 'POST',
//     url: 'https://calculator.climatesi.com/ghg_bo_rest/services/ghg_services',
//
//   ws: 'wss://calculator.climatesi.com/ghg_bo_rest/ws/',
//   secure: false,
// },

// {
//   method: 'POST',
//     url: 'http://localhost:3000/ghg_bo_rest/services/ghg_services',
//
//   ws: 'wss://calculator.climatesi.com/ghg_bo_rest/ws/',
//   secure: false,
// },

export const config = {

  bo: {
    method: 'POST',
    url:'https://calculator.climatesi.com/ghg_bo_rest/services/ghg_services' +
      '' +
      '',

     ws: 'wss://calculator.climatesi.com/ghg_bo_rest/ws/',
    secure: false,
  },
  client: {
    userType: 1,
    entitlements: {
      routes: [
        'pages/client/',
        ''
      ]
    },
    menus: [

    ]
  },
  admin: {
    userType: 2,
    routes: [
      '',
      '',
      ''
    ]
  },
  cadmin: {
    userType: 2,
    routes: [
      '',
      '',
      ''
    ]
  },

}
