export enum CadminStatus {
  NotSubmitted = 1,
  Pending = 2,
  Approved = 3,
  NeedInfo = 4,

};
