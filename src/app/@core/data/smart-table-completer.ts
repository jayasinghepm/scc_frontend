import {Observable} from "rxjs";

export abstract class SmartTableData {
  abstract getBranches(): Observable<any[]>;
  abstract getCountries(): Observable<any[]>;
  abstract getAirportCodes(): Observable<any[]>;
  abstract getEmployees(): Observable<any[]>;
  abstract getSeatClasses(): Observable<any[]>;
  abstract getMonths(): Observable<any[]>;
  abstract getYears(): Observable<any[]>;
  abstract getFireExtTypes(): Observable<any[]>;
  abstract getRefrigerantTypes(): Observable<any[]>;
  abstract getFueltypes(): Observable<any[]>;
  abstract getUnits(): Observable<any[]>;
  abstract getWasteTypes(): Observable<any[]>;
  abstract getDisposalMethods(): Observable<any[]>;
  abstract getTransportModes(): Observable<any[]>;
  abstract getVehicleCategories(): Observable<any[]>;
  abstract getVehicleModels(): Observable<any[]>;
  abstract getMatrialtypes(): Observable<any[]>;

}
