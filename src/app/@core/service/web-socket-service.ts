import {Injectable} from "@angular/core";
import {config} from '../../@core/config/app-config'
import {UserState} from "../auth/UserState";

@Injectable({
  providedIn: "root",
})
export class WebSocketService {

  public url = config.bo.ws;

  public wsNotification:WebSocket;
  public wsAirports:WebSocket;

  public init(){
    // this.wsAirports = new WebSocket(this.url + UserState.getInstance().userId + "/3")
    // this.wsNotification= new WebSocket(this.url + UserState.getInstance().userId + "/2")
  }

}
