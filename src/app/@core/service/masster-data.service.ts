import {Injectable} from "@angular/core";
import {of as observableOf,Observable} from "rxjs";
import {BackendService} from "../rest/bo_service";
import {RequestGroup} from "../enums/request-group-enum";
import {RequestType} from "../enums/request-type-enums";
import {UserData} from "../data/users";
import {UserState} from "../auth/UserState";
import {from} from 'rxjs';


@Injectable({
  providedIn: "root",
})
export class MassterDataService {
  //cost years updating
  currenty: number = new Date().getFullYear();
  py:number = this.currenty-1;
  ppy:number = this.currenty-2;
  pppy:number = this.currenty-3;
  ppppy:number = this.currenty-4;

  private countries: {id: number, name: string}[] = [];
  private airports: {id:number, name: string}[] = [];
  private branches: {id:number, name: string,  companyId: number}[] = [];
  private companies: {id: number, name: string, fy: boolean, pages: number[], emSources: number[], allowedEmissionSources : any}[] = [];
  private companiesDataEntry: {id: number, name: string, fy: boolean}[] = [];
  private companiesSummaryInputs: {id: number, name: string, fy: boolean}[] = [];
  private seatClasses: {id: number, name: string}[] = [];
  private airTravelways: {id: number, name: string}[] = [];
  private months: {id: number, name: string}[] = [];
  private units: {id: number, name: string}[] = [];
  private refriTypes: {id: number, name: string}[] = [];
  private fuelTypes: {id: number, name: string}[] = [];
  private vehicleTypes: {id: number, name: string}[] = [];
  private vehicleTypesWaste: {id: number, name: string}[] = [];
  private vehcielTypesOwn : {id: number, name: string}[] = [];
  private disMethods: {id: number, name: string}[] = [];
  private wasteTypes: {method: number, id: number, name: string, msw: boolean, sludge: boolean}[] = [];
  private vehicleCategroies: {id: number, name: string}[] = [];
  private fireExtTypes: {id: number, name: string}[] = [];
  private employees: {id: number, name: string}[] = [];
  private titles: string [] = [];
  private emissionSources : {id: number,name: string}[] = [];
  private publicVehicleTypes: { id: number, name: string} [] =  [];

  private unitsElectrictity : {id: number, name: string} [] = [];
  public unitsMunWater : {id: number, name: string} [] = []
  private fuelTypesGenerators: {id: number, name: string} [] = []
  private wasteTransVehcileTypes:{id: number, name: string} [] = []
  private districts :{name: string}[]  = []
  private unitsGenerators: {id: number, name: string} [] = []
  private unitsTransport: {id: number, name: string} [] = [];
  private noEmissionOptions : {id: number, name: string} [] = [];

  private ports: {id: number, name: string} [] = [];
  private freightTypes : {id: number, name: string} [ ] = [];

  private years: {comId:number, years: string []}[] = []

  private fueltTypesVehicle: {id: number, name: string} [] = [];
  private fueltTypesVehicleY: {id: number, name: string} [] = [];



  private roles: {id: number, name: string}[] = [];

  private projects: {id: number, name: string, status: number, comId: number} [] = [];
  private companyDtos : any[] = [];
  private biomassTypes : any[] = [];
  mitigationProjects: {id: number, name: string}[] = [];

  constructor(private boservice: BackendService) {

  }
  public init() {
    // todo:
    // this.countries.push(...[{id: 1, name: 'Sri lanka'}, {id: 2, name: 'India'}])
    // this.airports.push(...[{id: 0, name: 'None'},{id: 1, name: 'CMB'}, {id: 2, name: 'NZ'}]);
    // this.branches.push(...[{id: 1, name: 'Head office'}, {id: 2, name: 'Borrella'}]);
    // this.companies.push({id: 1, name: 'Commercial Bank'});
    this.airports = []
    this.branches = []
    this.companies = []
    this.countries = []
    this.employees = []
    this.titles = [];
    this.seatClasses = []
    this.airTravelways = []
    this.months = []
    this.units = []
    this.unitsMunWater = []
    this.unitsElectrictity = []

    this.refriTypes = [];
    this.fuelTypesGenerators = []
    this.fuelTypes = []
    this.vehicleTypes = []
    this.wasteTransVehcileTypes = []
    this.vehicleCategroies = [];
    this .publicVehicleTypes = [];
    this.disMethods = [];
    this.wasteTypes = [];
    this.fireExtTypes = []
    this.emissionSources = []
    this.unitsGenerators = []
    this.districts = []
    this.unitsTransport = [];
    this.fueltTypesVehicle = [];
    this.fueltTypesVehicleY = [];


    this.vehicleTypesWaste = [];
    this.noEmissionOptions = [];
    this.vehcielTypesOwn = [];
    this.roles = [];
    this.ports = [];
    this.freightTypes = [];
    this.companyDtos = [];






    this.loadAirports()
    // this.loadBranches().subscribe(d => {
    //   this.branches = d;
    // })
    this.loadCompanies().subscribe(d => {
      this.companies = d;
    })
    this.loadCompaninesForDataEnry();
    this.loadCompaninesForSummaryInputs();
    this.loadCountries();
    this.loadEmployees().subscribe(d => {
      this.employees = d;
    })

    this.freightTypes.push(...[{id: 1, name: 'Sea Freight'}, {id: 2, name: 'Air Freight'}]);
    this.ports.push(...[
      {id: 1, name: 'SHANGHAI (CHINA)'},
      {id: 2, name: 'JAKARTA (INDONESIA)'},
      {id: 3, name: 'HOCHIMINH (VIETNAM)'},
      {id: 4, name: 'SINGAPORE'},
      {id: 5, name: 'MUNDRA (INDIA)'},
      {id: 6, name: 'TUTICORIN (INDIA)'},
      {id: 7, name: 'KAOHSIUNG (TAIWAN)'},
      {id: 8, name: 'QINGDAO (CHINA)'},
      {id: 9, name: 'KARACHCHI (PAKISTHAN)'},
      {id: 10, name: 'PIPAVAV (INDIA)'},
      {id: 11, name: 'NINGBO (CHINA)'},
      {id: 12, name: 'GERMAN'},
      {id: 13, name: 'NETHERLAND'},
      {id: 14, name: 'PORT HEDLAND (AUSTRALIA)'},
      {id: 15, name: 'VANCOUVER (CANADA)'},
      {id: 16, name: 'CARTAGENA (COLOMBIA)'},
      {id: 17, name: 'FREDERICIA (DENMARK)'},
      {id: 18, name: 'HELSINKI (FINLAND)'},
      {id: 19, name: 'MARSEILLE (FRANCE)'},
      {id: 20, name: 'ASHDOD (ISRAEL)'},
      {id: 21, name: 'NAGOYA (JAPAN)'},
      {id: 22, name: 'BUSAN (KOREA)'},
      {id: 23, name: 'MANZANILLO (MEXICO)'},
      {id: 24, name: 'ROTTERDAM (NETHERLANDS)'},
      {id: 25, name: 'TAURANGA (NEW ZEALAND)'},
      {id: 26, name: 'DURBAN (SOUTH AFRICA)'},
      {id: 27, name: 'SOUTH LOUISIANA (UNITED STATES)'},

    ])
    this.titles.push(...['Mr', 'Mrs', 'Miss', 'Ms', 'Dr', 'Prof', 'Rev','Eng'])
    this.seatClasses.push(...[{id: 1, name: 'First Class'}, {id: 2, name: 'Business Class'}, {id: 3, name: 'Economic Class'}]);
    this.airTravelways.push(...[{id: 1, name: 'No'}, { id: 2, name: 'Yes'}]);
    this.months.push(...[
      {id: 12, name: 'All Months'},
      {id: 0, name: 'January',},
      { id: 1, name: 'February'},
      {id: 2, name: 'March'},
      {id: 3, name: 'April'},
      {id: 4, name: 'May'},
      {id: 5, name: 'June'},
      {id: 6, name: 'July'},
      {id: 7, name: 'August'},
      {id: 9, name: 'September'},
      {id: 8, name: 'October'},
      {id: 10, name: 'November'},
      {id: 11, name: 'December'},
    ]);

    this.mitigationProjects.push(...[
      { id: 1, name: 'Solar'},
      { id: 2, name: 'Hydro'},
      { id: 3, name: 'Wind'},
      { id: 4, name: 'Tree planting'},
      { id: 5, name: 'Rain water harvesting'},
      { id: 6, name: 'Energy efficiency actions'},
      { id: 7, name: 'Biogas'},
      { id: 8, name: 'Recycling'},
    ])



    this.units.push(...[
      { id: 1, name: 'm3'},
      {id: 2, name: 'kg'},
      {id: 3, name: 'kWh'},
      {id: 4, name: 'liters'},
      {id: 5, name: 'Tons'},
      {id: 6, name: 'LKR'},

    ]);
    this.unitsTransport.push(...[
      { id: 1, name: 'm3'},
      {id: 4, name: 'liters'},
      {id: 6, name: 'LKR'},
    ])
    this.unitsGenerators.push(...[{ id: 1, name: 'm3'},{id: 4, name: 'liters'},{id: 6, name: 'LKR'},])
    this.unitsElectrictity.push(...[{id: 3, name: 'kWh'},]);
    this.unitsMunWater.push( { id: 1, name: 'm3'})
    this.refriTypes.push(...[
      {id: 1, name: 'R22'},
      {id: 2, name: 'R407C'},
      {id: 3, name: 'R410A'},
      {id: 4, name: 'R134a'},
    ]);
    this.fuelTypes.push(...[
      {id: 1, name: 'Petrol' },
      {id: 2, name: 'Diesel',},
      {id: 3, name: 'Kerosene'},
      {id: 4, name: 'Solar Electric'},
      {id: 5, name: 'Grid Electric'},
    ]);

    this.fueltTypesVehicle.push(...[
     
      {id: 1, name: 'Petrol'},
      {id: 2, name: 'Disel' },

      
    ]);
    
    this.fueltTypesVehicleY.push(...[
     
      {id: 1, name: 'LP95'},
      {id: 3, name: 'LP92' },

      {id: 2, name: 'LAD',},
      {id: 4, name: 'LSD'},
      
    ]);


    

    this.fuelTypesGenerators.push(...[
      {id: 2, name: 'Diesel',},])
    this.vehicleTypes.push(...[
      {id: 1, name: 'Lorry'},
      {id: 2, name: 'Tractor'},
      {id: 3, name: 'Van'},
      {id: 4, name: 'Jeep'},
      {id: 5, name: 'Car'},
      {id: 6, name: 'Prime move'},
      {id: 7, name: 'Bike'},
      {id: 8, name: 'Threewheel'},
      {id: 9, name: 'Bus'},
      {id: 10, name: 'Train'},
      {id: 11, name: 'Walking'},
      {id: 12, name: 'Cycling'},
      {id: 13, name: 'Free Ride'},
      {id: 14, name: 'Agriculture Tractors'},
      {id: 15, name: 'Chain saws'},
      {id: 16, name: 'ForkLifts'},
      {id: 17, name: 'Airport Groud Support Equipment'},
      {id: 18, name: 'Other Offroad'},


      {id: 19, name: 'Other'},
      {id: 20, name: 'None'}
    ]);
    this.vehcielTypesOwn.push(...[
      {id: 3, name: 'Van'},
      {id: 4, name: 'Jeep'},
      {id: 5, name: 'Car'},
      {id: 6, name: 'Prime move'},
      {id: 7, name: 'Bike'},
      {id: 8, name: 'Threewheel'},
      {id: 20, name: 'None'}
    ])
    this.noEmissionOptions.push(...[
      {id: 11, name: 'Walking'},
      {id: 12, name: 'Cycling'},
      {id: 13, name: 'Free Ride'},
      {id: 20, name: 'None'}
    ])
    this.wasteTransVehcileTypes.push(...[  {id: 1, name: 'Lorry'},
      {id: 2, name: 'Tractor'},{id: 8, name: 'Threewheel'}, {id: 3, name: 'Van'},])
    this.publicVehicleTypes.push(...[
      {id: 1, name: 'Van Diesel'},
      {id: 2, name: 'Medium Buse Diesel'},
      {id: 3, name: 'Bus Diesel'},
      {id: 4, name: 'Train'},
      {id: 20, name: 'None'}
    ])
    this.vehicleCategroies.push(...[
      {id: 1, name: 'Company Own'},
      {id: 2, name: 'Rented'},
      {id: 3, name: 'Hired'},
    ]);
    this.disMethods.push(...[
      {id: 1, name: 'Re-use'},
      {id: 2, name: 'Open-loop'},
      {id: 3, name: 'Closed-loop'},
      {id: 4, name: 'Combustion'},
      {id: 5, name: 'Composting'},
      {id: 6, name: 'Landfill'},
      {id: 7, name: 'Anaerobic digestion'},
      {id: 8, name: 'Piggery Feeding'},
      {id: 9, name: 'Incineration'},
    ]);
    this.wasteTypes.push(...[
      {method: 1 ,id: 1, name: 'Average construction', msw: false, sludge: false},
      {method: 1 ,id: 3, name: 'Tyres', msw: false, sludge: false},
      {method: 1 ,id: 4, name: 'Wood', msw: false, sludge: false},
      // {method: 1 ,id: 7, name: 'Average construction', msw: false, sludge: false},

      {method: 2 ,id: 1, name: 'Average construction', msw: false, sludge: false},
      {method: 2 ,id: 3, name: 'Tyres', msw: false, sludge: false},
      {method: 2 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 2 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 2 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 2 ,id: 13, name: 'WEEE', msw: false, sludge: false},
      {method: 2 ,id: 14, name: 'WEEE - fridges and freezers', msw: false, sludge: false},
      {method: 2 ,id: 15, name: 'Batteries', msw: false, sludge: false},
      {method: 2 ,id: 17, name: 'Plastics', msw: false, sludge: false},

      {method: 3 ,id: 1, name: 'Average construction', msw: false, sludge: false},
      {method: 3 ,id: 2, name: 'Soils', msw: false, sludge: false},
      {method: 3 ,id: 3, name: 'Tyres', msw: false, sludge: false},
      {method: 3 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 3 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 3 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 3 ,id: 7, name: 'Clothing', msw: false, sludge: false},
      {method: 3 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 3 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},
      {method: 3 ,id: 16, name: 'Metal', msw: false, sludge: false},
      {method: 3 ,id: 17, name: 'Plastics', msw: false, sludge: false},
      {method: 3 ,id: 18, name: 'Paper and board', msw: false, sludge: false},

      {method: 4 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 4 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 4 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 4 ,id: 7, name: 'Clothing', msw: false, sludge: false},
      {method: 4 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 4 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 4 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 4 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 4 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},
      {method: 4 ,id: 13, name: 'WEEE', msw: false, sludge: false},
      {method: 4 ,id: 16, name: 'Metal', msw: false, sludge: false},
      {method: 4 ,id: 17, name: 'Plastics', msw: false, sludge: false},
      {method: 4 ,id: 18, name: 'Wood', msw: false, sludge: false},

      {method: 5 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 5 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 5 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 5 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 5 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 5 ,id: 18, name: 'Paper and board', msw: false, sludge: false},

      {method: 6 ,id: 2, name: 'Soils', msw: false, sludge: false},
      {method: 6 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 6 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 6 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 6 ,id: 7, name: 'Clothing', msw: false, sludge: false},
      {method: 6 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 6 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 6 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 6 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 6 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},
      {method: 6 ,id: 13, name: 'WEEE ', msw: false, sludge: false},
      {method: 6 ,id: 14, name: 'WEEE - fridges and freezers', msw: false, sludge: false},
      {method: 6 ,id: 15, name: 'Batteries', msw: false, sludge: false},
      {method: 6 ,id: 16, name: 'Metal', msw: false, sludge: false},
      {method: 6 ,id: 17, name: 'Plastics', msw: false, sludge: false},
      {method: 6 ,id: 18, name: 'Paper and board', msw: false, sludge: false},

      {method: 7 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 7 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 7 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 7 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 7 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},

      {method: 8 ,id: 30, name: 'Waste', msw: false, sludge: false},



      {method: 9 ,id: 19, name: 'Paper/ Cardboard', msw: true, sludge: false},
      {method: 9 ,id: 20, name: 'Textiles', msw: true, sludge: false},
      {method: 9 ,id: 21, name: 'Food waste', msw: true, sludge: false},
      {method: 9 ,id: 22, name: 'Wood', msw: true, sludge: false},
      {method: 9 ,id: 23, name: 'Garden and Park waste', msw: true, sludge: false},
      {method: 9 ,id: 24, name: 'Nappies', msw: true, sludge: false},
      {method: 9 ,id: 25, name: 'Rubber and Leather', msw: true, sludge: false},
      {method: 9 ,id: 26, name: 'Plastics ', msw: true, sludge: false},
      {method: 9 ,id: 27, name: 'Other, inert waste', msw: true, sludge: false},


      {method: 9 ,id: 28, name: 'Domestic', msw: false, sludge: true},
      {method: 9 ,id: 29, name: 'Industrial', msw: false, sludge: true},




    ]);
    this.fireExtTypes.push(...[
      {id: 1, name: 'W/Gas'},
      {id: 2, name: 'CO2'},
      {id: 3, name: 'D/C/P'},
    ])
    this.emissionSources.push(...[
      {id: 1, name: 'Business Air travel'},
      {id: 2, name: 'Fire Extinguisher'},
      {id: 3, name: 'Refrigerants'},
      {id: 4, name: 'Generators'},
      {id: 5, name: 'Electricity'},
      {id: 6, name: 'Waste Disposal'},
      {id: 7, name: 'Municipal water'},
      {id: 8, name: 'Employee Commuting'},
      {id: 9, name: 'Transport'},
      {id: 10, name: 'Waste transport'},
      {id: 11, name: 'Transport Locally purchased'},
    ])
    this.districts.push(...[
      {name: 'Ampara'},
      {name: 'Anuradhapura'},
      {name: 'Badulla'},
      {name: 'Batticaloa'},
      {name: 'Colombo'},
      {name: 'Galle'},
      {name: 'Gampaha'},
      {name: 'Hambantota'},
      {name: 'Jaffna'},
      {name: 'Kalutara'},
      {name: 'Kandy'},
      {name: 'Kegalle'},
      {name: 'Kilinochchi'},
      {name: 'Kurunegala'},
      {name: 'Mannar'},
      {name: 'Matale'},
      {name: 'Matara'},
      {name: 'Moneragala'},
      {name: 'Mullaitivu'},
      {name: 'Nuwara Eliya'},
      {name: 'Polonnaruwa'},
      {name: 'Puttalam'},
      {name: 'Ratnapura'},
      {name: 'Trincomalee'},
      {name: 'Vavuniya'},
      {name: 'Not Available'}
    ])
    this.roles.push(...[
      {id: 1, name: 'CEO'},
      {id: 2, name: 'Finance Officer'},
      {id: 3, name: 'Sustainability Officer'},
      {id: 4, name: 'Environmental Engineer'},
      {id: 5, name: 'Head of Low Emissions Initiatives'},
      {id: 6, name: 'Head of Sustainability Strategies/ Head of Finance'},
      {id: 7, name: 'Sustainability Engineer'},

    ])

    this.biomassTypes.push(...[
      {id: 1, name: 'Wood Chips'},
      {id: 2, name: 'Saw Dust'},
      {id: 3, name: 'Other Primary Solid Biomass'}
      ])


  }

  public loadBranches(): Observable<any> {
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
         jsonBody = {
           FILTER_MODEL: {
             id: { value: UserState.getInstance().branchId , type: 1, col: 1}
           },
           PAGE_NUMBER: -1,
         }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      jsonBody
    )).pipe().map(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.branches = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
             this.branches.push({id: val.id, name: val.name,  companyId: val.companyId});
            }
          });
          // console.log(this.branches)

        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.branches;
    })
  }

  public loadCompaninesForDataEnry() :Observable<any> {
    // if (this.companiesDataEntry.length > 0) {
    //   return observableOf(this.companiesDataEntry)
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      //todo: change if required
      RequestType.ListCompany,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.companies = []
          this.years = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.companiesDataEntry = []
              this.companiesDataEntry.push({id: val.id, name: val.name, fy: val.isFinancialYear === 2 ? true: false});

                // console.log(val)
                // this.years = [];
                this.companies.push({
                  id: val.id,
                  name: val.name,
                  fy: val.isFinancialYear === 2 ? true: false,
                  pages: val.pages,
                  emSources: val.emSources,
                  allowedEmissionSources: val.allowedEmissionSources,

                });
                this.years.push({comId: val.id, years: [ val.isFinancialYear === 2 ? `${val.fyCurrentStart}/${(val.fyCurrentEnd % 100)}`: `${val.fyCurrentStart}`]})

            }
          });
          //todo; this.companines
          return this.companies;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companies;
    })
  }


  public loadCompaninesForSummaryInputs() :Observable<any> {
    // if (this.companiesSummaryInputs.length > 0) {
    //   return observableOf(this.companiesSummaryInputs);
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompanySummaryInputs,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.companies = []
          // this.years = [];
          this.companiesSummaryInputs = []
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              // console.log(val)
              this.companiesSummaryInputs.push({id: val.id, name: val.name, fy: val.isFinancialYear === 2 ? true: false});
              // this.years = [];
              // this.years.push({comId: val.id, years: [ val.isFinancialYear === 2 ? `${val.fyCurrentStart}/${(val.fyCurrentEnd % 100)}`: `${val.fyCurrentStart}`]})

            }
          });
          return this.companiesSummaryInputs;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companiesSummaryInputs;
    })
  }

  public loadCompaniesDtos():Observable<any> {
    // if (this.companies.length > 0) {
    //   return observableOf(this.companies);
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 4: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          this.companyDtos = []
          // console.log(data);
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.companyDtos.push(val);
            }
          });
          return this.companyDtos;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companyDtos;
    })
  }



  public loadCompanies():Observable<any> {
    // if (this.companies.length > 0) {
    //   return observableOf(this.companies);
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 4: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.companies = []
          this.years = [];
          // console.log(data);
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              // this.companyDtos.push(val);
              // console.log(val)
              // this.years = [];
              this.companies.push({id: val.id, name: val.name, fy: val.isFinancialYear === 2 ? true: false, pages: val.pages,
                emSources: val.emSources, allowedEmissionSources: val.allowedEmissionSources});
              this.years.push({comId: val.id, years: [ val.isFinancialYear === 2 ? `${val.fyCurrentStart}/${(val.fyCurrentEnd % 100)}`: `${val.fyCurrentStart}`]})

            }

          });
          return this.companies;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companies;
    })
  }



  private loadCountries() {
    this.boservice.sendRequestToBackend(
      RequestGroup.MasterData,
      RequestType.ListCountries,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.countries.push({id: val.id, name: val.name});
            }
          });
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private loadAirports() {
    this.boservice.sendRequestToBackend(
      RequestGroup.MasterData,
      RequestType.ListAirports,
      {
        PAGE_NUMBER: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.airports.push({id: val.id, name: val.name});
            }
          });
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  public loadEmployees(): Observable<any> {

    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            branchId: { value: UserState.getInstance().branchId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            comId: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    // todo: user type filet3er
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListEmployee,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          this.employees = []
          data.DAT.list.forEach(val => {
            if (val != undefined) {

              this.employees.push({id: val.id, name: val.name});
            }
          });
          // console.log(this.employees)
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.employees;
    })
  }

  public loadProjects():Observable<any> {

    return from( this.boservice.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          status: { value: 7, type: 2, col: 1}
        }
      }
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {

          this.projects = [];

          data.DAT.LIST.forEach(val => {
            if (val != undefined && val.status >= 5) {
              this.projects.push({id: val.id, name: val.name, status: val.status, comId: val.companyId});
            }
          });

        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
    }
      return this.projects;
    }
    );
  }



  public getBranchName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const name = this.branches.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }


  public getEmissionSrcId(name: string): Observable<number> {
    const id  = this.emissionSources.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getEmissionSrcName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const name = this.emissionSources.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getPublicVehcileId(name: string): Observable<number> {
    const id  = this.publicVehicleTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getPublicVehicleName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const name = this.publicVehicleTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  


    public getMitigationActionTypeName(id: number) :Observable<string> {
      if (id === undefined) return observableOf("")
      const name = this.mitigationProjects.filter(value=> {return value.id === id})[0].name;
      return observableOf(name);
    }
    public getMitigationActionTypeId(name: string) : Observable<number> {
      const id = this.mitigationProjects.filter(value => value.name === name)[0].id;
      return observableOf(id);
    }

  public getBranchId(name: string): Observable<number> {
    const id  = this.branches.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getEmployeeName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const name = this.employees.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getEmployeeId(name: string): Observable<number> {
    const id  = this.employees.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getCompanyName(id: number) : Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    // console.log(id);
    // console.log(this.companies)
    const name = this.companies.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getCompanyId(name: string) : Observable<number> {
    const id  = this.companies.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public isFy(id: number) : Observable<boolean> {
    return observableOf(this.companies.filter(v => v.id === id)[0].fy);
  }

  public getAirportName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    // console.log(id);
    const name = this.airports.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getAirportId(name: string) : Observable<number> {
    const id  = this.airports.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getCountryName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.countries.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getCountryId(name: string) : Observable<number> {
    const id  = this.countries.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getSeatClassName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.seatClasses.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getSeatClassId(name: string) :Observable<number> {
    const id  = this.seatClasses.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getAirTravelWay(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.airTravelways.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getAirTravelWayId(name: string) : Observable<number> {
    const id  = this.airTravelways.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getMonthName(id: number) :Observable<string> {
    if (id === undefined) return observableOf("")
    const name = this.months.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getMonthId(name: string) : Observable<number> {
    const id = this.months.filter(value => value.name === name)[0].id;
    return observableOf(id);
  }

  public getRefriTrypName(id: number): Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.refriTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getRefriTypeId(name: string) : Observable<number> {
    const id  = this.refriTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getUnitsId(name: string): Observable<number> {
    const id  = this.units.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getUnitsName(id: number): Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.units.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getVehicleTypeId(name: string) :Observable<number> {
    const id  = this.vehicleTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getVehicleTypeName(id :number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.vehicleTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getFuelTypeName(id: number) :Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.fuelTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getFuelTypeId(name: string) : Observable<number> {
    // console.log(name)
    const id  = this.fuelTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getWastTypeId(name: string) : Observable<number> {
    const id  = this.wasteTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getWastTypeName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.wasteTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getDisposalMethodId(name: string) : Observable<number> {
    const id  = this.disMethods.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getDisposalMethodName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.disMethods.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getVehicleCategory(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.vehicleCategroies.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getVehicleCatId(name: string) : Observable<number> {
    const id  = this.vehicleCategroies.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getFireExtTypeId(name: string) : Observable<number> {
    const id  = this.fireExtTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getFireExtTypeName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.fireExtTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getRoleId(name: string): Observable<number> {
    if (name === undefined || name=== '' ){
      return observableOf(0);
    }
    const id = this.roles.filter(value => value.name === name)[0].id;
    return observableOf(id);
  }

  public getRoleName(id: number): Observable<string> {
    if (id === 0 || id === undefined) return observableOf("")
    const name = this.roles.filter(value => {return value.id === id})[0].name;
    return observableOf(name);
  }

  getAirportCodes(): Observable<any[]> {
    return observableOf(this.airports.map(v => v.name));
  }

  getBranches(): Observable<any[]> {
    // return observableOf(this.branches.map(v => v.name));
    return this.loadBranches().map(v => v.map(d => d.name))
  }



  getBranchesFull(): Observable<any[]> {
    // return observableOf(this.branches)
    return this.loadBranches();
  }

  getCountries(): Observable<any[]> {
    return observableOf(this.countries.map(v => v.name));
  }

  getCountriesFull(): Observable<any[]> {
    return observableOf(this.countries);
  }

  getCompaninesFull(): Observable<any[]> {
    return this.loadCompanies();
    // return observableOf(this.companies)
  }
  getAirportsFull(): Observable<any[]> {
    return observableOf(this.airports)
  }

  getWasteDisMethodsFull(): Observable<any[]> {
    return observableOf(this.disMethods);
  }

  getWasteDisTypes(method: number): Observable<any[]> {
    return observableOf(this.wasteTypes.filter(d => { return (d.method=== method);}))
  }


  getSeatClassesFull(): Observable<any[]> {
    return observableOf(this.seatClasses);
  }
  getMonthsFull(): Observable<any[]> {
    return observableOf(this.months);
  }

  getMitigationActionFull(): Observable<any[]> {
    return observableOf(this.mitigationProjects);
  }
  getPortsFull() : Observable<any[]> {
    return observableOf(this.ports);
  }

  getBiomassTypesFull() : Observable<any[]> {
    return observableOf(this.biomassTypes);
  }

  getFreightTypesFull() : Observable<any[]> {
    return observableOf(this.freightTypes);
  }


  getRolesFull():Observable<any[]> {
    return observableOf(this.roles);
  }
  getBranchesForCompany(comId: number) : Observable<any[]> {
    return observableOf(this.branches.filter(v => v.companyId === comId));
  }
  getYearsForCompany(comId: number): Observable<any[]> {
    // console.log(this.years)
    let list = this.years.filter(v => v.comId === comId);
    // console.log(list);
    return observableOf(list.map(v => {return v.years[0]}));
  }


  getPublicVehicleTypes(): Observable<any[]> {
    return observableOf(this.publicVehicleTypes.map(v => v.name));
  }
  getPublicVehicleTypesFull(): Observable<any[]> {
    return observableOf(this.publicVehicleTypes);
  }

  getDisposalMethods(): Observable<any[]> {
    return observableOf(this.disMethods.map(v => v.name));
  }
  getDisposalMethodsFull(): Observable<any[]> {
    return observableOf(this.disMethods);
  }

  getEmployees(): Observable<any[]> {
    // return observableOf(this.employees.map(v => v.name));
    return this.loadEmployees().map(v => v.map(d => d.name));
  }

  getFireExtTypes(): Observable<any[]> {
    return observableOf(this.fireExtTypes.map(v => v.name));
  }
  getFireExtTypesFull(): Observable<any[]> {
    return observableOf(this.fireExtTypes);
  }

  getFueltypes(): Observable<any[]> {
    return observableOf(this.fuelTypes.map(v => v.name));
  }
  getFueltypesFull(): Observable<any[]> {
    return observableOf(this.fuelTypes);
  }
  public getFuelTypesGenerator(): Observable<any[]> {
    return observableOf(this.fuelTypesGenerators.map(v => v.name));
  }
  public getFuelTypesGeneratorFull(): Observable<any[]> {
    return observableOf(this.fuelTypesGenerators);
  }
  getFuelTypesVehicleFull():Observable<any[]> {
    return observableOf(this.fueltTypesVehicle);
  }
  getFuelTypesVehicleYFull():Observable<any[]> {
    return observableOf(this.fueltTypesVehicleY);
  }


  getMonths(): Observable<any[]> {
    return observableOf(this.months.map(v => v.name));
  }

  getRefrigerantTypes(): Observable<any[]> {
    return observableOf(this.refriTypes.map(v => v.name));
  }
  getRefrigerantTypesFull(): Observable<any[]> {
    return observableOf(this.refriTypes);
  }

  getSeatClasses(): Observable<any[]> {
    return observableOf(this.seatClasses.map(v => v.name));
  }

  getTransportModes(): Observable<any[]> {
    return observableOf(this.vehicleTypes.map(v => v.name));
  }

  public getWasteTransVehicleTypes():Observable<any[]> {
    return observableOf(this.wasteTransVehcileTypes.map(v => v.name));
  }
  public getWasteTransVehicleTypesFull():Observable<any[]> {
    return observableOf(this.wasteTransVehcileTypes);
  }

  getUnits(): Observable<any[]> {
    return observableOf(this.units.map(v => v.name));
  }
  public getUnitsGenerators(): Observable<any[]> {
    return observableOf(this.unitsGenerators.map(v => v.name));
  }
  public getUnitsGeneratorsFull(): Observable<any[]> {
    return observableOf(this.unitsGenerators);
  }


  getVehicleCategories(): Observable<any[]> {
    return observableOf(this.vehicleCategroies.map(v => v.name));
  }
  getVehicleCategoriesFull(): Observable<any[]> {
    return observableOf(this.vehicleCategroies);
  }

  getVehicleModels(): Observable<any[]> {
    return observableOf(this.vehicleTypes.map(v => v.name));
  }
  getVehicleModelsFull(): Observable<any[]> {
    return observableOf(this.vehicleTypes);
  }

  getWasteTypes(): Observable<any[]> {
    return observableOf(this.wasteTypes.map(v => v.name));
  }
  getWasteTypesFull(): Observable<any[]> {
    return observableOf(this.wasteTypes);
  }

  getYears(): Observable<any[]> {
    return observableOf(['2018', '2019']);
  }

  getMatrialtypes(): Observable<any[]> {
    return observableOf(this.wasteTypes.map(v => v.name));
  }
  getMatrialtypesFull(): Observable<any[]> {
    return observableOf(this.wasteTypes);
  }

  public getTitles(): Observable<string[]> {
    return observableOf(this.titles);
  }


  public getMaterialTypeId(name: string): Observable<number> {
    return observableOf(1);
  }

  public getMaterialTypeName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    return observableOf(this.wasteTypes.filter(v =>  v.id === id)[0].name);
  }

  public getCompanies(): Observable<any[]> {
    // return observableOf(this.companies.map(v => v.map(d => d.name));
    return this.loadCompanies().map(v => v.map(d => d.name));
  }

  public getEmissionSources(): Observable<string[]> {
    return observableOf(this.emissionSources.map(v => v.name));
  }

  public getUnitsElectricity(): Observable<string[]> {
    return observableOf(this.unitsElectrictity.map(v => v.name));
  }

  public getUnitsMunWater():Observable<string[]> {
    return observableOf(this.unitsMunWater.map(v => v.name));
  }

  public getDistricts():Observable<string[]> {
    return observableOf(this.districts.map(v => v.name));
  }

  public getNoEmissionOptionsFull():Observable<any[]> {
    return observableOf(this.noEmissionOptions);
  }

  public getVehiclesOwnFull(): Observable<any[]> {
    return observableOf(this.vehcielTypesOwn);
  }

}
