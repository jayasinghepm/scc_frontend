import {AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy} from '@angular/core';
import { Subscription } from "rxjs";
import { debounceTime } from "rxjs/operators";
import {LoadingScreenService} from "../../service/loading-screen-service";

@Component({
  selector: 'app-loading-component',
  templateUrl: './loading-component.component.html',
  styleUrls: ['./loading-component.component.scss']
})
export class LoadingScreenComponent implements AfterViewInit,AfterViewChecked, OnDestroy {

  debounceTime: number = 200;
  loading: boolean = false;
  loadingSubscription: Subscription;

  constructor(private loadingScreenService: LoadingScreenService,
              private _elmRef: ElementRef,
              private _changeDetectorRef: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this._elmRef.nativeElement.style.display = 'none';
    //
    this.loadingSubscription = this.loadingScreenService.loadingStatus.pipe(debounceTime(this.debounceTime)).subscribe(
      (status: boolean) => {
        this._elmRef.nativeElement.style.display = status ? 'block' : 'none';
        this._changeDetectorRef.detectChanges();
      }
    );

    this._changeDetectorRef.detectChanges();
  }
  ngAfterViewChecked(){

  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }

}
