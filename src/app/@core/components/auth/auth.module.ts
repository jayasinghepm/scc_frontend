import { NgModule } from '@angular/core'
import {
    NbLayoutModule,
    NbCardModule,
    NbIconModule,
    NbAlertModule,
    NbCheckboxModule,
    NbInputModule,
    NbActionsModule,
    NbButtonModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
} from '@nebular/theme';

import { AuthRoutingModule } from './auth.routing.module';


import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NbAuthComponent } from './auth.component';
import { NbLoginComponent } from './login/login.component';
import { NbAuthBlockComponent } from './auth-block/auth-block.component';


@NgModule({
    imports: [
        AuthRoutingModule,
        NbLayoutModule,
        NbAlertModule,
        NbCardModule,
        NbIconModule,
        CommonModule,
        NbCheckboxModule,
        FormsModule,
        NbInputModule,
        NbActionsModule,
        NbButtonModule,
        NbDatepickerModule,
        NbRadioModule,
        NbSelectModule,
        NbUserModule,
    ],
    declarations: [
        NbLoginComponent,
        NbAuthComponent,
        NbAuthBlockComponent,


    ],
})
export class AuthModule {

}